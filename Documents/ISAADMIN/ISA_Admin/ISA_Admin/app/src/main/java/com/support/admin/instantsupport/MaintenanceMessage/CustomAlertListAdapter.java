package com.support.admin.instantsupport.MaintenanceMessage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.support.admin.instantsupport.R;

import java.util.ArrayList;

/**
 * -----------------------------------------------------------------------------
 * Class Name: CustomAlertListAdapter
 * Created By: Suganya
 * Created Date:16/5/2016
 * Modified By:
 * Modified Date:
 * Purpose: This will provide customized alert view
 * -----------------------------------------------------------------------------
 */
public class CustomAlertListAdapter extends BaseAdapter {
    private static final String TAG = "CustomAlertListAdapter";
    private LayoutInflater inflater = null;
    ArrayList<String> alTechNameList = null;
    private Activity context = null;
    private Intent iNativeMap;

    public CustomAlertListAdapter(Context context, ArrayList<String> alTechNameList) {
        this.context = (Activity) context;
        this.alTechNameList = alTechNameList;
        inflater = LayoutInflater.from(this.context);

    }

    @Override
    public int getCount() {
        return alTechNameList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final AlterListHolder mAlterListHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.custom_alertlist, parent, false);
            mAlterListHolder = new AlterListHolder(convertView);
            convertView.setTag(mAlterListHolder);
        } else {
            mAlterListHolder = (AlterListHolder) convertView.getTag();
        }
        mAlterListHolder.tvAlertList.setText(alTechNameList.get(position));


        return convertView;
    }

    /**
     * -----------------------------------------------------------------------------
     * Class Name: AlterListHolder
     * Created By:
     * Created Date: 19-05-2016
     * Modified By:
     * Modified Date:
     * Purpose: For holding data
     * -----------------------------------------------------------------------------
     */
    private class AlterListHolder {
        private TextView tvAlertList;
        private Typeface TextArial;


        public AlterListHolder(View item) {
            tvAlertList = (TextView) item.findViewById(R.id.tvAlertList);

            TextArial = Typeface.createFromAsset(context.getAssets(), "fonts/arial.ttf");
            tvAlertList.setTypeface(TextArial);
        }

    }

}
