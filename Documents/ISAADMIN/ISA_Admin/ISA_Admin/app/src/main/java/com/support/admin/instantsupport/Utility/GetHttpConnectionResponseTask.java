package com.support.admin.instantsupport.Utility;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.support.admin.instantsupport.IHttpConnectionInterface;

import org.json.fh.JSONObject;


/**
 * -----------------------------------------------------------------------------
 * Class Name: GetHttpConnectionResponseTask
 * Created By: Suganya
 * Created Date: 11-08-2017
 * Modified By:
 * Modified Date:
 * Purpose: AsynTask for handling HttpConnection
 * -----------------------------------------------------------------------------
 */
public class GetHttpConnectionResponseTask extends AsyncTask {

    private Activity cntx;
    private IHttpConnectionInterface iHttpConnectionInterface;
    private String sURL,sResponce;
    private JSONObject jsonObject= null;
    private String sHttpConnectionMethod;

    public GetHttpConnectionResponseTask(Activity cntx,String sURL, JSONObject jsonObject, String sHttpConnectionMethod) {
        this.cntx = cntx;
        this.iHttpConnectionInterface = (IHttpConnectionInterface) cntx;
        this.sURL = sURL;
        this.jsonObject =jsonObject;
        this.sHttpConnectionMethod = sHttpConnectionMethod;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Object doInBackground(Object[] objects) {

        try {
           Log.e("Request =" ,sURL);
            sResponce = Utils.getHttpConnectionResponse(sURL,jsonObject,sHttpConnectionMethod);
            Log.e("sResponce", sResponce);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        iHttpConnectionInterface.processConnectionFinish(sResponce);
    }

}

