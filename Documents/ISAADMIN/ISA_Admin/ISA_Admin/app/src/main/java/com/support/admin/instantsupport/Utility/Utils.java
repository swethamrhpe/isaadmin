package com.support.admin.instantsupport.Utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.feedhenry.sdk.FH;
import com.feedhenry.sdk.FHActCallback;
import com.feedhenry.sdk.FHResponse;
import com.feedhenry.sdk.api.FHAuthRequest;
import com.support.admin.instantsupport.Application.Constants;
import com.support.admin.instantsupport.Login.ILoginInterface;
import com.support.admin.instantsupport.Login.LoginScreenModel;
import com.support.admin.instantsupport.R;

import org.json.fh.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.ContentValues.TAG;

/**
 * -----------------------------------------------------------------------------
 * Class Name: Utils
 * Created By: Suganya
 * Created Date: 31/07/2017
 * Modified By:
 * Modified Date:
 * Purpose: Put All Method That Multiple Use
 * -----------------------------------------------------------------------------
 */

public class Utils implements Constants {
    /**
     * -----------------------------------------------------------------------------
     * Method Name: isInternetOn
     * Created By : Suganya
     * Created Date:31/7/2017
     * Modified By:
     * Modified Date:
     * Purpose    : This method will check Internet Connectivity
     * and return true if it available return false
     * if its not available
     * -----------------------------------------------------------------------------
     */
    public static boolean isInternetOn(Context amcntx) {
        ConnectivityManager mConnectivityMgrdevice = (ConnectivityManager) amcntx.getSystemService(amcntx.CONNECTIVITY_SERVICE);
        NetworkInfo mNetworkinfoDevice = mConnectivityMgrdevice.getActiveNetworkInfo();
        if (mNetworkinfoDevice != null && mNetworkinfoDevice.isConnected())
            return true;

        return false;
    }


    /**
     * -----------------------------------------------------------------------------
     * Method Name: doAuthLogin
     * Created By : Suganya
     * Created Date: 31/7/2017
     * Modified By:
     * Modified Date:
     * Purpose : It will make authentication call for login with usernamre and password
     * from assets and put in /data/data...assets directory of device a
     * -----------------------------------------------------------------------------
     */
    public static LoginScreenModel doAuthLogin(final Activity cntx, final String sUserName, final String sPassword) {
        final LoginScreenModel objLoginData = new LoginScreenModel();
        final ILoginInterface iLoginInterface = (ILoginInterface) cntx;
        try {
            FH.init(cntx, new FHActCallback() {
                @Override
                public void success(FHResponse fhResponse) {
                    try {

                        FHAuthRequest authRequest = FH.buildAuthRequest(CLOUD.POLICE_ID, sUserName, sPassword);
                        authRequest.setPresentingActivity(cntx);
                        authRequest.executeAsync(new FHActCallback() {
                            @Override
                            public void success(FHResponse resp) {
                                AppLog.showLogE(TAG, "Login success" + resp.getJson());
                                JSONFormater.doJSONAuthLogin(cntx, resp.getJson());
                            }

                            @Override
                            public void fail(FHResponse resp) {
                                try {
                                    if (!TextUtils.isEmpty(resp.getJson().getString(CONST_CLOUD_CALL.MESSAGE))) {
                                        AppLog.showLogE(TAG, "Login fail" + resp.getError() + resp.getRawResponse() + resp.getJson());
                                        objLoginData.setMessage(resp.getJson().getString(CONST_CLOUD_CALL.MESSAGE));
                                        objLoginData.setStatus(CONST_CLOUD_CALL.ERROR);
                                    } else {
                                        objLoginData.setMessage(resp.getError().toString());
                                        objLoginData.setStatus(CONST_CLOUD_CALL.ERROR);
                                    }
                                } catch (Exception e) {
                                    AppLog.showLogE(TAG, "Exception>>>" + e);
                                    iLoginInterface.processFinish(null);
                                } finally {
                                    iLoginInterface.processFinish(objLoginData);
                                }

                            }
                        });

                    } catch (Exception e) {
                        AppLog.showLogE(TAG, "Exception>>>" + e);
                        iLoginInterface.processFinish(null);

                    }

                }

                @Override
                public void fail(FHResponse fhResponse) {
                    AppLog.showLogE(TAG, "FH Login fail");
                    iLoginInterface.processFinish(null);
                }
            });
            return objLoginData;
        }catch (Exception e){
            e.printStackTrace();
            return  null;
        }
    }



    /**
     * -----------------------------------------------------------------------------
     * Method Name: getHttpConnectionResponse
     * Created By : Suganya
     * Created Date: 16/08/2017
     * Modified By:
     * Modified Date:
     * Purpose    : This method will get data from FH Server using HttpURLConnection
     * -----------------------------------------------------------------------------
     */

    public static String getHttpConnectionResponse(String url, JSONObject jsonParam, String sHttpConnectionMethod) {

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        // Will contain the raw JSON response as a string.
        String forecastJsonStr = null;

        try {
            URL httpURL = new URL(url);
            urlConnection = (HttpURLConnection) httpURL.openConnection();
            urlConnection.setRequestProperty("X-FH-appkey", "a6aca00d06c0794feafe622b7b03b87ebd288ce9");
            urlConnection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            urlConnection.setRequestMethod(sHttpConnectionMethod);
            urlConnection.setReadTimeout(2500);
            urlConnection.setConnectTimeout(2500);
            // urlConnection.setDoInput(true);
            //urlConnection.setDoOutput(true);

            urlConnection.connect();

            if (jsonParam != null) {
                DataOutputStream wr = new DataOutputStream(
                        urlConnection.getOutputStream());
                wr.writeBytes(String.valueOf(jsonParam));
                wr.flush();
                wr.close();
            }

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                // But it does make debugging a *lot* easier if you print out the completed
                // buffer for debugging.
                buffer.append(line + "\n");
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return null;
            }
            forecastJsonStr = buffer.toString();
            return forecastJsonStr;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return "";
    }


    /**
     * -----------------------------------------------------------------------------
     * Method Name: ShowDalogOKCancel
     * Created By : Suganya
     * Created Date: 31/7/2017
     * Modified By:
     * Modified Date:
     * Purpose    :  This method will Appear Dialog with user friendly and
     * understandable format and on press ok it will dismiss
     * -----------------------------------------------------------------------------
     */
    @SuppressWarnings("deprecation")
    public static AlertDialog ShowDalogOKCancel(final Context amcntx, String amsTitle, String amsMessage) {
        AlertDialog adOKCancel;
        adOKCancel = new AlertDialog.Builder(amcntx).create();
        adOKCancel.setTitle(amsTitle);
        adOKCancel.setMessage(amsMessage);
        adOKCancel.setCancelable(true);
        adOKCancel.setButton(amcntx.getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                return;
            }
        });

        adOKCancel.setButton2(amcntx.getString(R.string.Cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                return;
            }
        });
        adOKCancel.show();
        adOKCancel.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
                    dialog.dismiss();
                    return true;
                }
                return false;
            }
        });
        return adOKCancel;
    }

    /**
     * -----------------------------------------------------------------------------
     * Method Name: ShowDialog
     * Created By : Suganya
     * Created Date: 08/08/2016
     * Modified By:
     * Modified Date:
     * Purpose    : This method will Appear Dialog with user friendly and
     * understandable format and on press OK, it will dismiss
     * -----------------------------------------------------------------------------
     */
    public static Dialog ShowDialog(Context amcntx, String amsTitle, String amsMessage) {

        final Dialog madFinishActivity;
        madFinishActivity = new Dialog(amcntx);
        madFinishActivity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        madFinishActivity.setContentView(R.layout.alertcustom);
        madFinishActivity.setCancelable(true);

        TextView mtxtAlrCustTitle = (TextView) madFinishActivity.findViewById(R.id.tvAlrCustTitle);
        TextView mtxtAlrCustdetail = (TextView) madFinishActivity.findViewById(R.id.tvAlrCustdetail);
        Button mbtnAlrCustOk = (Button) madFinishActivity.findViewById(R.id.btnAlrCustOk);

        mtxtAlrCustTitle.setText(amsTitle);
        mtxtAlrCustdetail.setText(amsMessage);

        mbtnAlrCustOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                madFinishActivity.dismiss();
                return;
            }
        });
        madFinishActivity.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
                    dialog.dismiss();
                    return true;
                }
                return false;
            }
        });
        madFinishActivity.show();
        return madFinishActivity;
    }


    /**
     * -----------------------------------------------------------------------------
     * Method Name: ShowDialogWithDismiss
     * Created By : Suganya
     * Created Date: 09/08/2017
     * Modified By:
     * Modified Date:
     * Purpose    : This method will Appear Dialog with user friendly and
     * understandable format and on press OK, it does not have dismiss
     * -----------------------------------------------------------------------------
     */
    public static void ShowDialogWithDismiss(Activity context, String sTitle, String sMessage, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder alDialog = new AlertDialog.Builder(context);
        alDialog.setCancelable(false);
        alDialog.setTitle(sTitle);
        alDialog.setMessage(sMessage);
        alDialog.setPositiveButton(context.getString(R.string.ok), onClickListener);
        alDialog.show();
    }
}
