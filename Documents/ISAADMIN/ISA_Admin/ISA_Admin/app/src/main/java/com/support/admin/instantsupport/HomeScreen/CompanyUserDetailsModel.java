package com.support.admin.instantsupport.HomeScreen;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * -----------------------------------------------------------------------------
 * Class Name: CompanyUserDetailsModel
 * Created By: Suganya
 * Created Date: 17-08-2017
 * Modified By:
 * Modified Date:
 * Purpose: CompanyUserDetailsModel for Storing admin company user details
 * -----------------------------------------------------------------------------
 */
public class CompanyUserDetailsModel implements Parcelable {

    private String userName;
    private String userEmail;
    private String type;
    private String domain;
    private String assignedTechnician;
    private String primaryPhoneNum;
    private String secondaryPhoneNum1;
    private String secondaryPhoneNum2;
    private String selectedLang;
    private String guid;
    private String companyId;
    private String message;


    public CompanyUserDetailsModel() {

    }

    @Override
    public String toString() {
        return "ClassPojo [userName = " + userName + ", userEmail = " + userEmail + ", type = " + type + ", domain = " + domain + ", assignedTechnician = " + assignedTechnician + ", primaryPhoneNum = " + primaryPhoneNum + ", secondaryPhoneNum1 = " + secondaryPhoneNum1 + ", secondaryPhoneNum2 = " + secondaryPhoneNum2 + ", selectedLang = " + selectedLang + ", guid = " + guid + ", companyId = " + companyId + ", message = " + message + "]";
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getAssignedTechnician() {
        return assignedTechnician;
    }

    public void setAssignedTechnician(String assignedTechnician) {
        this.assignedTechnician = assignedTechnician;
    }

    public String getPrimaryPhoneNum() {
        return primaryPhoneNum;
    }

    public void setPrimaryPhoneNum(String primaryPhoneNum) {
        this.primaryPhoneNum = primaryPhoneNum;
    }

    public String getSecondaryPhoneNum1() {
        return secondaryPhoneNum1;
    }

    public void setSecondaryPhoneNum1(String secondaryPhoneNum1) {
        this.secondaryPhoneNum1 = secondaryPhoneNum1;
    }

    public String getSecondaryPhoneNum2() {
        return secondaryPhoneNum2;
    }

    public void setSecondaryPhoneNum2(String secondaryPhoneNum2) {
        this.secondaryPhoneNum2 = secondaryPhoneNum2;
    }

    public String getSelectedLang() {
        return selectedLang;
    }

    public void setSelectedLang(String selectedLang) {
        this.selectedLang = selectedLang;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    protected CompanyUserDetailsModel(Parcel in) {
        userName = in.readString();
        userEmail = in.readString();
        type = in.readString();
        domain = in.readString();
        assignedTechnician = in.readString();
        primaryPhoneNum = in.readString();
        secondaryPhoneNum1 = in.readString();
        secondaryPhoneNum2 = in.readString();
        selectedLang = in.readString();
        guid = in.readString();
        companyId = in.readString();
        message = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userName);
        dest.writeString(userEmail);
        dest.writeString(type);
        dest.writeString(domain);
        dest.writeString(assignedTechnician);
        dest.writeString(primaryPhoneNum);
        dest.writeString(secondaryPhoneNum1);
        dest.writeString(secondaryPhoneNum2);
        dest.writeString(selectedLang);
        dest.writeString(guid);
        dest.writeString(companyId);
        dest.writeString(message);
    }

    public static final Creator<CompanyUserDetailsModel> CREATOR = new Creator<CompanyUserDetailsModel>() {
        @Override
        public CompanyUserDetailsModel createFromParcel(Parcel in) {
            return new CompanyUserDetailsModel(in);
        }

        @Override
        public CompanyUserDetailsModel[] newArray(int size) {
            return new CompanyUserDetailsModel[size];
        }
    };
}
