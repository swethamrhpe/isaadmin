package com.support.admin.instantsupport.UserScreen;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.support.admin.instantsupport.Application.Constants;
import com.support.admin.instantsupport.Application.InstApplication;
import com.support.admin.instantsupport.HomeScreen.CompanyDetailsModel;
import com.support.admin.instantsupport.HomeScreen.CompanyUserDetailsModel;
import com.support.admin.instantsupport.IHttpConnectionInterface;
import com.support.admin.instantsupport.MaintenanceMessage.CustomAlertListAdapter;
import com.support.admin.instantsupport.R;
import com.support.admin.instantsupport.Utility.AppLog;
import com.support.admin.instantsupport.Utility.GetHttpConnectionResponseTask;
import com.support.admin.instantsupport.Utility.HttpResponse;
import com.support.admin.instantsupport.Utility.ScreenNavigator;
import com.support.admin.instantsupport.Utility.Utils;

import org.json.fh.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.support.admin.instantsupport.Application.Constants.CLOUD.REQ_CREATE_USER_DATA;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.ASSIGNED_TECH;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.COMPANY_ID;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.DOMAIN;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.LATITUDE;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.LOCATION;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.LONGITUDE;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.PRIMARY_PHONE_NUM;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.SECONDARY_PHONE_NUM1;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.SECONDARY_PHONE_NUM2;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.SELECTED_LANG;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.SUBSTITUDETID;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.TRACKING;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.USER_EMAILID;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.USER_NAME;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.USER_TYPE;
/**
 * -----------------------------------------------------------------------------
 * Class Name: UserDetailActivity
 * Created By: Swetha M.R
 * Created Date: 1-09-2017
 * Modified By:
 * Modified Date:
 * Purpose: UserDetailActivity class for all Adding / Editing the data.
 * -----------------------------------------------------------------------------
 */

public class UserDetailActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener, IHttpConnectionInterface {
    @BindView(R.id.ivLoginIcon)
    ImageView ivLoginIcon;
    private CompanyDetailsModel objCompanyDetailsModel;
    private InstApplication applicationObject;
    private ArrayList<String> alLanguageList;
    private CompanyUserDetailsModel objUSerModel;
    private TextView tvAlertListTitle;
    private ListView lvAlertList;
    private LinearLayout llAlertList;
    public static AlertDialog.Builder adBuilder;
    public static Dialog adAlertList;
    private ArrayList<CompanyUserDetailsModel> arrComapnyuserList;
    private CompanyUserDetailsModel alCompanyUserDetailsModel;
    private InstApplication ApplicationObject;
    private ProgressDialog mProgressDialog;

    @BindView(R.id.eduName)
    EditText etName;
    @BindView(R.id.eduEmail)
    EditText etEmail;
    @BindView(R.id.eduDomain)
    EditText etDomain;
    @BindView(R.id.eduphone)
    EditText etPhone;
    @BindView(R.id.edusecphon1)
    EditText etSecPhon1;
    @BindView(R.id.edusecphon2)
    EditText etSecPhon2;
    @BindView(R.id.txtuserLangSelect)
    TextView tvUseleLang;
    @BindView(R.id.educmpId)
    EditText etCompId;
    @BindView(R.id.eduServiceHour)
    EditText etUserType;

    @BindView(R.id.llviplayout)
    LinearLayout llVip;
    @BindView(R.id.eduVipassTech)
    EditText etAssignedTech;

    @BindView(R.id.eduTechLocation)
    EditText etTechLoc;
    @BindView(R.id.eduTechlatitude)
    EditText etTechLatitude;
    @BindView(R.id.eduTechlongitude)
    EditText etTechLongitude;
    @BindView(R.id.eduTechsubTid)
    EditText etTechSubstitudeTid;
    @BindView(R.id.eduTechtracking)
    EditText etTechTracking;
    @BindView(R.id.llTechLayout)
    LinearLayout llTechLayout;
    @BindView(R.id.btnadduser)
    Button btAddUser;
    @BindView(R.id.btnSubmit)
    Button btSubmit;
    private String TAG = "AddUSerDataACtivity";
    String selectedType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adduser);
        ButterKnife.bind(this);
        applicationObject = ((InstApplication) getApplicationContext());
        arrComapnyuserList = applicationObject.getAlCompanyUserDetailsModel();
        adBuilder = new AlertDialog.Builder(this);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            selectedType = bundle.getString("selectedType");
            alCompanyUserDetailsModel = bundle.getParcelable(Constants.BundleKeyConstants.USER_DETAILS);
            if (alCompanyUserDetailsModel != null) {
                etName.setText( alCompanyUserDetailsModel.getUserName());
                etUserType.setText(alCompanyUserDetailsModel.getType());
                etEmail.setText(alCompanyUserDetailsModel.getUserEmail());
                etDomain.setText(alCompanyUserDetailsModel.getDomain());
                etPhone.setText(alCompanyUserDetailsModel.getPrimaryPhoneNum());
                etSecPhon1.setText(alCompanyUserDetailsModel.getSecondaryPhoneNum1());
                etSecPhon2.setText(alCompanyUserDetailsModel.getSecondaryPhoneNum2());
                etCompId.setText(String.valueOf(alCompanyUserDetailsModel.getCompanyId()));
                tvUseleLang.setText(alCompanyUserDetailsModel.getSelectedLang());
                if(null != etTechLatitude.getText().toString()){
                   // etTechLatitude.setText(alCompanyUserDetailsModel.get);
                }
                if(null != etAssignedTech.getText().toString()){
                    etAssignedTech.setText(alCompanyUserDetailsModel.getAssignedTechnician());
                }
            }
            if (selectedType != null) {
                if (selectedType.equals("VIP")) {
                    llVip.setVisibility(View.VISIBLE);
                    llTechLayout.setVisibility(View.GONE);

                } else if (selectedType.equals("Technician")) {
                    llTechLayout.setVisibility(View.VISIBLE);
                    llVip.setVisibility(View.GONE);
                }else if(selectedType.equals("Normal")){
                    llTechLayout.setVisibility(View.GONE);
                    llVip.setVisibility(View.GONE);
                }
            }
        }

        tvUseleLang.setOnClickListener(this);
        btSubmit.setOnClickListener(this);
        btAddUser.setOnClickListener(this);
        tvUseleLang.setClickable(true);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtuserLangSelect:
                tvUseleLang.setClickable(false);
                alLanguageList = objCompanyDetailsModel.getLangSupported();
                if (alLanguageList != null && alLanguageList.size() > 0) {
                    tvAlertListTitle = new TextView(new ContextThemeWrapper(UserDetailActivity.this, R.style.TextViewAlterList));
                    lvAlertList = new ListView(UserDetailActivity.this);
                    llAlertList = new LinearLayout(UserDetailActivity.this);
                    llAlertList.setOrientation(LinearLayout.VERTICAL);
                    llAlertList.setGravity(Gravity.CENTER);
                    tvAlertListTitle.setText(getString(R.string.etMMLanguage));
                    alLanguageList.add(getResources().getString(R.string.Cancel));
                    llAlertList.addView(tvAlertListTitle);
                    adBuilder.setView(llAlertList);
                    adBuilder.setCancelable(false);
                    lvAlertList.setAdapter(new CustomAlertListAdapter(UserDetailActivity.this, alLanguageList));
                    llAlertList.addView(lvAlertList);
                    lvAlertList.setOnItemClickListener(this);
                    adAlertList = adBuilder.show();
                }

                break;

            case R.id.btnSubmit:
                btnClickOPtion();

                break;
            case R.id.btnadduser:
                if (etName.getText() != null) etName.getText().clear();
                if (etEmail.getText() != null) etEmail.getText().clear();
                if (etDomain.getText() != null) etDomain.getText().clear();
                if (etUserType.getText() != null) etUserType.getText().clear();
                if (tvUseleLang.getText() != null) tvUseleLang.getText().toString();
                if (etCompId.getText() != null) etCompId.getText().clear();

                break;
            default:


        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        adAlertList.dismiss();
        // adBuilder.setCancelable(true);
        //if (objCompanyDetailsModel.getLangSupported().size() > position) {
        tvUseleLang.setText(objCompanyDetailsModel.getLangSupported().get(position).trim().split("-")[0]);
        //}
    }

    @Override
    public void processConnectionFinish(String jsonObject) {
        AppLog.showLogE(TAG, "jsonobject= " + jsonObject);
        if (mProgressDialog != null) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        }
        Gson gson = new Gson();
        HttpResponse response = new HttpResponse();
        if(jsonObject!=null) {
            response = gson.fromJson(jsonObject, HttpResponse.class);
            if (response != null && !response.getSuccessmessage().isEmpty()) {
                Utils.ShowDialog(UserDetailActivity.this, this.getString(R.string.app_name), response.getSuccessmessage());
            }
        }
        ScreenNavigator.callUserDataActivity(UserDetailActivity.this,arrComapnyuserList);
        finish();
    }

    /*Button click event for submit the details*/
    private void btnClickOPtion() {
        if (Utils.isInternetOn(UserDetailActivity.this)) {
            mProgressDialog = ProgressDialog.show(this, null, getResources().getString(R.string.pleasewait));
            mProgressDialog.setContentView(R.layout.progressdialog_custom);
            mProgressDialog.setCancelable(false);
            String assignedTechnician = etAssignedTech.getText().toString();
            String strSecphon = etSecPhon1.getText().toString();
            String strSecphon2 = etSecPhon2.getText().toString();
            String strPrimphon = etPhone.getText().toString();
            String struserType = etUserType.getText().toString();
            String strTechLati = etTechLatitude.getText().toString();
            String strTechLong = etTechLongitude.getText().toString();
            String strTechSubTID = etTechSubstitudeTid.getText().toString();
            String strTechTracking = etTechTracking.getText().toString();
            String strTechLoc = etTechLoc.getText().toString();
            if (TextUtils.isEmpty(strSecphon)) strSecphon = "";
            if (TextUtils.isEmpty(strSecphon2)) strSecphon2 = "";
            if (TextUtils.isEmpty(strPrimphon)) strPrimphon = "";
            if (TextUtils.isEmpty(struserType)) struserType = "1";

            if (TextUtils.isEmpty(strTechSubTID)) strTechSubTID = "";
            if (TextUtils.isEmpty(strTechTracking)) strTechTracking = "";
            if (TextUtils.isEmpty(strTechLati)) strTechLati = "";

            if (TextUtils.isEmpty(strTechLong)) strTechLong = "";

            if (etName.getText().toString().trim().equalsIgnoreCase("")) {
                etName.setError("This field can not be blank");
            }
            if (etEmail.getText().toString().trim().equalsIgnoreCase("")) {
                etEmail.setError("This field can not be blank");
            }
            if (etDomain.getText().toString().trim().equalsIgnoreCase("")) {
                etDomain.setError("This field can not be blank");
            }
            if (etCompId.getText().toString().trim().equalsIgnoreCase("")) {
                etCompId.setError("This field can not be blank");
            }
            if (etAssignedTech.getText().toString().trim().equalsIgnoreCase("")) {
                etAssignedTech.setError("This field can not be blank");
            }
            if (etTechLoc.getText().toString().trim().equalsIgnoreCase("")) {
                etTechLoc.setError("This field can not be blank");
            }

            JSONObject jsonParam = new JSONObject();
            if (!TextUtils.isEmpty(selectedType)) {
                if (!TextUtils.isEmpty(etCompId.getText().toString()) &&
                        !TextUtils.isEmpty(etDomain.getText().toString()) &&
                        !TextUtils.isEmpty(etName.getText().toString()) &&
                        !TextUtils.isEmpty(etEmail.getText().toString())) {

                    jsonParam.put(SELECTED_LANG, tvUseleLang.getText().toString());
                    jsonParam.put(USER_NAME, etName.getText().toString());
                    jsonParam.put(USER_EMAILID, etEmail.getText().toString());
                    jsonParam.put(DOMAIN, etDomain.getText().toString());
                    jsonParam.put(COMPANY_ID, etCompId.getText().toString());
                    jsonParam.put(PRIMARY_PHONE_NUM, strPrimphon);
                    jsonParam.put(SECONDARY_PHONE_NUM1, strSecphon);
                    jsonParam.put(SECONDARY_PHONE_NUM2, strSecphon2);
                    jsonParam.put(USER_TYPE, struserType);

                    if (selectedType.equals("VIP")) {
                        jsonParam.put(ASSIGNED_TECH, assignedTechnician);

                    } else if (selectedType.equals("Technician")) {
                        if (strTechLoc != null) {
                            jsonParam.put(LOCATION, strTechLoc);
                            jsonParam.put(LATITUDE, strTechLati);
                            jsonParam.put(LONGITUDE, strTechLong);
                            jsonParam.put(SUBSTITUDETID, strTechSubTID);
                            jsonParam.put(TRACKING, strTechTracking);
                            AppLog.showLogE(TAG, "jsonparma=" + jsonParam);
                        } else {
                            if (mProgressDialog != null && mProgressDialog.isShowing())
                                mProgressDialog.dismiss();
                        }
                    }
                    AppLog.showLogE(TAG, "jsonparma=" + jsonParam);
                    new GetHttpConnectionResponseTask(UserDetailActivity.this,
                            applicationObject.getBaseEnv() + REQ_CREATE_USER_DATA, jsonParam,
                            Constants.Method.POST).execute();
                } else {
                    if (mProgressDialog != null && mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    Utils.ShowDialog(UserDetailActivity.this, this.getString(R.string.app_name),
                            getString(R.string.entermanddetails));
                }
            }
        } // end of internet

        else {
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismiss();
            Utils.ShowDialog(UserDetailActivity.this, this.getString(R.string.app_name), getString(R.string.checkData));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ScreenNavigator.callUserDataActivity(UserDetailActivity.this, arrComapnyuserList);
        finish();
    }


    @OnClick(R.id.ivLoginIcon)
    public void onViewClicked() {
    }
}
