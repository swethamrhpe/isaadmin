package com.support.admin.instantsupport.HomeScreen;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * -----------------------------------------------------------------------------
 * Class Name: CompanyDetailsModel
 * Created By: Suganya
 * Created Date: 10-08-2017
 * Modified By:
 * Modified Date:
 * Purpose: CompanyDetailsModel for Storing admin company details
 * -----------------------------------------------------------------------------
 */

public class CompanyDetailsModel implements Parcelable {

    private String userTableName;

    private String resetPasswordUrl;

    private String domain;

    private String companyLogo;

    private String companyId;

    private String companyName;

    private String privacyURL;

    private ArrayList<String> config_U0;

    private String techTableName;

    private ArrayList<String> langSupported;

    private ArrayList<String> config_U1;

    private String uniqueId;

    private String message;

    public CompanyDetailsModel() {

    }

    public String getUserTableName() {
        return userTableName;
    }

    public void setUserTableName(String userTableName) {
        this.userTableName = userTableName;
    }

    public String getResetPasswordUrl() {
        return resetPasswordUrl;
    }

    public void setResetPasswordUrl(String resetPasswordUrl) {
        this.resetPasswordUrl = resetPasswordUrl;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPrivacyURL() {
        return privacyURL;
    }

    public void setPrivacyURL(String privacyURL) {
        this.privacyURL = privacyURL;
    }

    public ArrayList<String> getConfig_U0() {
        return config_U0;
    }

    public void setConfig_U0(ArrayList<String> config_U0) {
        this.config_U0 = config_U0;
    }

    public String getTechTableName() {
        return techTableName;
    }

    public void setTechTableName(String techTableName) {
        this.techTableName = techTableName;
    }

    public ArrayList<String> getLangSupported() {
        return langSupported;
    }

    public void setLangSupported(ArrayList<String> langSupported) {
        this.langSupported = langSupported;
    }

    public ArrayList<String> getConfig_U1() {
        return config_U1;
    }

    public void setConfig_U1(ArrayList<String> config_U1) {
        this.config_U1 = config_U1;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ClassPojo [userTableName = " + userTableName + ", resetPasswordUrl = " + resetPasswordUrl + ", domain = " + domain + ", companyLogo = " + companyLogo + ", companyId = " + companyId + ", companyName = " + companyName + ", privacyURL = " + privacyURL + ", config_U0 = " + config_U0 + ", techTableName = " + techTableName + ", langSupported = " + langSupported + ", config_U1 = " + config_U1 + ", uniqueId = " + uniqueId + ", message = " + message + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    protected CompanyDetailsModel(Parcel in) {
        userTableName = in.readString();
        resetPasswordUrl = in.readString();
        domain = in.readString();
        companyLogo = in.readString();
        companyId = in.readString();
        companyName = in.readString();
        privacyURL = in.readString();
        config_U0 = in.createStringArrayList();
        techTableName = in.readString();
        langSupported = in.createStringArrayList();
        config_U1 = in.createStringArrayList();
        uniqueId = in.readString();
        message = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userTableName);
        dest.writeString(resetPasswordUrl);
        dest.writeString(domain);
        dest.writeString(companyLogo);
        dest.writeString(companyId);
        dest.writeString(companyName);
        dest.writeString(privacyURL);
        dest.writeStringList(config_U0);
        dest.writeString(techTableName);
        dest.writeStringList(langSupported);
        dest.writeStringList(config_U1);
        dest.writeString(uniqueId);
        dest.writeString(message);
    }

    public static final Creator<CompanyDetailsModel> CREATOR = new Creator<CompanyDetailsModel>() {
        @Override
        public CompanyDetailsModel createFromParcel(Parcel in) {
            return new CompanyDetailsModel(in);
        }

        @Override
        public CompanyDetailsModel[] newArray(int size) {
            return new CompanyDetailsModel[size];
        }
    };
}
