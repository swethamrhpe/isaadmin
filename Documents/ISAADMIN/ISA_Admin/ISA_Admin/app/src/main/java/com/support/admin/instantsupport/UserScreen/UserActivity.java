package com.support.admin.instantsupport.UserScreen;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.Spinner;
import android.widget.TextView;

import com.support.admin.instantsupport.Application.Constants;
import com.support.admin.instantsupport.Application.InstApplication;
import com.support.admin.instantsupport.HomeScreen.CompanyUserDetailsModel;
import com.support.admin.instantsupport.HomeScreen.SpinnerAdapter;
import com.support.admin.instantsupport.Login.UserDetailsModel;
import com.support.admin.instantsupport.R;

import com.support.admin.instantsupport.Utility.AppLog;
import com.support.admin.instantsupport.Utility.ScreenNavigator;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by mrswe on 12/7/2017.
 */



public class UserActivity extends AppCompatActivity  implements  Filterable {

    private static final String TAG = "UserDataActivity";
    private InstApplication ApplicationObject;
    CompanyUserDetailsModel userDataModels;
    ArrayList<CompanyUserDetailsModel> arrUserDataModel;
    private RecyclerView rvUserDetail;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private static final int REQUEST_PICK_FILE = 1;
    private TextView filePath;
    CompanyUserDetailsModel userName ;
    int spinPosition;
    private File selectedFile;
    UserDetailsModel arrUserDetailsModel;
    String userTypeString ="";
    ArrayList<String> userModelArry = new ArrayList<>();
    private Spinner spinnerEnvironment;
    Set<String> filterlist      = new LinkedHashSet<>();
    List<String> envTitleList;
    List<String> userType = new ArrayList<>();
    SpinnerAdapter adapter;
    ArrayAdapter arrayAdapter;
    ArrayList<CompanyUserDetailsModel>              filteredContacts = new ArrayList<CompanyUserDetailsModel>();
    LinkedHashSet<CompanyUserDetailsModel> filterResultsData= new LinkedHashSet<CompanyUserDetailsModel>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);
        ApplicationObject = ((InstApplication) getApplicationContext());
        Bundle bunlde = getIntent().getExtras();
        if(bunlde!=null){
            arrUserDataModel = bunlde.getParcelableArrayList(Constants.BundleKeyConstants.USER_DETAILS);
        }
        spinnerEnvironment = (Spinner) findViewById(R.id.spinnerUsers);
        arrUserDetailsModel = ApplicationObject.getUserDetailsModel();
        rvUserDetail = (RecyclerView) findViewById(R.id.rvUserDetail);
        if(arrUserDataModel.size()==0){arrUserDataModel = ApplicationObject.getAlCompanyUserDetailsModel();}
        ButterKnife.bind(this);
        mLayoutManager = new LinearLayoutManager(this);
        rvUserDetail.setLayoutManager(mLayoutManager);
        rvUserDetail.setHasFixedSize(true);
        if(arrUserDataModel.size()>0) {
            for (int i = 0; i < arrUserDataModel.size(); i++) {
                userDataModels = arrUserDataModel.get(i);
                userTypeString = userDataModels.getType();
                userType.add(userTypeString);
            }
        }

        Resources res = getResources();
        String[] mySpinnerItem = res.getStringArray(R.array.arruserSelectedType);
        envTitleList = new ArrayList<>();
        for(String s:mySpinnerItem){
            envTitleList.add(s);
        }

        spinnerEnvironment.setSelection(0);
        arrayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,envTitleList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerEnvironment.setAdapter(arrayAdapter);
        arrayAdapter.notifyDataSetChanged();
        getFilter().filter("Normal");
        spinnerEnvironment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AppLog.showLogE(TAG,"pos env=" +envTitleList.get(position));
                spinPosition = position;
                if (filteredContacts != null && filteredContacts.size() > 0) {
                    if (mAdapter != null) {
                        mAdapter.notifyDataSetChanged();
                    }
                }
                getFilter().filter(envTitleList.get(position));
                mAdapter = new UserAdapter(UserActivity.this, filteredContacts,spinnerEnvironment.getSelectedItem().toString());
                try {
                    rvUserDetail.setAdapter(mAdapter);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        mAdapter = new UserAdapter(UserActivity.this, filteredContacts,spinnerEnvironment.getSelectedItem().toString());
        rvUserDetail.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    FilterObjects mContactsFilter;
    @Override
    public Filter getFilter() {
        if (mContactsFilter == null) {
            mContactsFilter = new FilterObjects();
        }
        return mContactsFilter;
    }

    public void cleanUpAdapter() {
        //rvUserDetail.getRecycledViewPool().clear();
        if(filteredContacts !=null){
            filteredContacts.clear();
        }
        mAdapter.notifyDataSetChanged(); /* Important */
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.fabUserDetailAddNew)
    public void addNewMaintenanceMessage(View view){
        cleanUpAdapter();
        ShowDialogforUsers();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        cleanUpAdapter();
        ScreenNavigator.callSubAdminHomeActivity(UserActivity.this,arrUserDetailsModel);
        finish();
    }


    public  Dialog ShowDialogforUsers() {

        final Dialog myDialog;
        myDialog = new Dialog(UserActivity.this);
        myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        myDialog.setContentView(R.layout.user_select_type_alert);
        myDialog.setCancelable(true);
        Button mbtnAlrFile = (Button) myDialog.findViewById(R.id.btnuserfile);
        TextView txtHeading = (TextView)myDialog.findViewById(R.id.txtheading);
        Button mbtnAlrSIngle = (Button) myDialog.findViewById(R.id.btnuserSIngle);
        if(spinnerEnvironment.getSelectedItem() !=null){
            txtHeading.setText("Would you like to add the "+spinnerEnvironment.getSelectedItem() +" users now?");
        }

        mbtnAlrFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserActivity.this, FilePicker.class);
                startActivityForResult(intent, REQUEST_PICK_FILE);
                if(myDialog!=null)
                    myDialog.dismiss();

            }
        });
        mbtnAlrSIngle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScreenNavigator.callAddUserActivity(UserActivity.this,userDataModels,spinnerEnvironment.getSelectedItem().toString());
                finish();
                if(myDialog!=null)
                    myDialog.dismiss();

            }
        });
        Button mbtnAlrNo = (Button) myDialog.findViewById(R.id.btnuserNo);

        mbtnAlrNo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                myDialog.dismiss();
                return;
            }
        });

        myDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
                    dialog.dismiss();
                    return true;
                }
                return false;
            }
        });
        myDialog.show();
        return myDialog;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {

            switch(requestCode) {

                case REQUEST_PICK_FILE:

                    if(data.hasExtra(FilePicker.EXTRA_FILE_PATH)) {

                        selectedFile = new File
                                (data.getStringExtra(FilePicker.EXTRA_FILE_PATH));
                        AppLog.showLogE(TAG,"selectfile =" +selectedFile.getPath());
                    }
                    break;
            }
        }
    }

    public class FilterObjects extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {                    // Create a FilterResults object
            FilterResults results = new FilterResults();
            if (filterResultsData.size() > 0) {
                filterResultsData.clear();
            }
            if (arrUserDataModel != null && arrUserDataModel.size() > 0)
                for (CompanyUserDetailsModel c : arrUserDataModel) {
                    if (("1").equals(c.getType()) && constraint.toString().equals("Normal")) {
                        if (userName != null) {
                            userName = null;
                        }
                        userName = c;
                    } else if (c.getType().equals("2") && constraint.toString().equals("VIP")) {
                        if (userName != null) {
                            userName = null;
                        }
                        userName = c;
                    } else if (c.getType().equals("3") && constraint.toString().equals("Technician")) {
                        if (userName != null) {
                            userName = null;
                        }
                        userName = c;
                    }
                    filterResultsData.add(userName);
                }
            if (filteredContacts.size() > 0) {
                filteredContacts.clear();
            }
            for (CompanyUserDetailsModel c : filterResultsData) {
                filteredContacts.add(c);
            }
            results.values = filteredContacts;
            results.count = filteredContacts.size();
            return results;


        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredContacts = (ArrayList<CompanyUserDetailsModel>) results.values;
            if (filteredContacts != null && filteredContacts.size() > 0) {
                mAdapter.notifyDataSetChanged();
            }
        }
    }
}
