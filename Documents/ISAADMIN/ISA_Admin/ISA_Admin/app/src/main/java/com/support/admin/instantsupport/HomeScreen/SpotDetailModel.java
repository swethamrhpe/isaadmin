package com.support.admin.instantsupport.HomeScreen;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * -----------------------------------------------------------------------------
 * Class Name: SpotDetailModel
 * Created By: Suganya
 * Created Date: 29-08-2016
 * Modified By:
 * Modified Date:
 * Purpose: This will provide  all sport details
 * -----------------------------------------------------------------------------
 */
public class SpotDetailModel implements Parcelable {

    private String addressField2;

    private String addressField1;

    private String centerTitle;

    private String isMeridianExist;

    private String serviceDetails;

    private String serviceHour;

    private String zipCode;

    private String state;

    private String countryCode;

    private String longitude;

    private String supportSpotType;

    private String latitude;

    private String companyId;

    private String message;

    private String uniqueId;

    public SpotDetailModel(){}

    public String getAddressField2 ()
    {
        return addressField2;
    }

    public void setAddressField2 (String addressField2)
    {
        this.addressField2 = addressField2;
    }

    public String getUniqueId ()
    {
        return uniqueId;
    }

    public void setUniqueId (String uniqueId)
    {
        this.uniqueId = uniqueId;
    }

    public String getIsMeridianExist ()
    {
        return isMeridianExist;
    }

    public void setIsMeridianExist (String isMeridianExist)
    {
        this.isMeridianExist = isMeridianExist;
    }

    public String getCenterTitle ()
    {
        return centerTitle;
    }

    public void setCenterTitle (String centerTitle)
    {
        this.centerTitle = centerTitle;
    }
    public String getServiceDetails ()
    {
        return serviceDetails;
    }

    public void setServiceDetails (String serviceDetails)
    {
        this.serviceDetails = serviceDetails;
    }
    public String getServiceHour ()
    {
        return serviceHour;
    }

    public void setServiceHour (String serviceHour)
    {
        this.serviceHour = serviceHour;
    }


    public String getAddressField1 ()
    {
        return addressField1;
    }

    public void setAddressField1 (String addressField1)
    {
        this.addressField1 = addressField1;
    }

    public String getZipCode ()
    {
        return zipCode;
    }

    public void setZipCode (String zipCode)
    {
        this.zipCode = zipCode;
    }

    public String getState ()
    {
        return state;
    }

    public void setState (String state)
    {
        this.state = state;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getCountryCode ()
    {
        return countryCode;
    }

    public void setCountryCode (String countryCode)
    {
        this.countryCode = countryCode;
    }

    public String getLongitude ()
    {
        return longitude;
    }

    public void setLongitude (String longitude)
    {
        this.longitude = longitude;
    }

    public String getSupportSpotType ()
    {
        return supportSpotType;
    }

    public void setSupportSpotType (String supportSpotType)
    {
        this.supportSpotType = supportSpotType;
    }

    public String getLatitude ()
    {
        return latitude;
    }

    public void setLatitude (String latitude)
    {
        this.latitude = latitude;
    }

    public String getCompanyId ()
    {
        return companyId;
    }

    public void setCompanyId (String companyId)
    {
        this.companyId = companyId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [addressField2 = "+addressField2+",uniqueId = "+uniqueId+", addressField1 = "+addressField1+", serviceDetails = "+serviceDetails+", countryCode = "+countryCode+", state = "+state+", supportSpotType = "+supportSpotType+", isMeridianExist = "+isMeridianExist+", zipCode = "+zipCode+", longitude = "+longitude+", companyId = "+companyId+", latitude = "+latitude+", serviceHour = "+serviceHour+", centerTitle = "+centerTitle+"]";    }

    protected SpotDetailModel(Parcel in) {
        addressField2 = in.readString();
        addressField1 = in.readString();
        zipCode = in.readString();
        state = in.readString();
        countryCode = in.readString();
        longitude = in.readString();
        supportSpotType = in.readString();
        latitude = in.readString();
        companyId = in.readString();
        message=in.readString();
        centerTitle=in.readString();
        serviceHour=in.readString();
        serviceDetails=in.readString();
        isMeridianExist = in.readString();
        uniqueId=in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(addressField2);
        dest.writeString(addressField1);
        dest.writeString(zipCode);
        dest.writeString(state);
        dest.writeString(countryCode);
        dest.writeString(longitude);
        dest.writeString(supportSpotType);
        dest.writeString(latitude);
        dest.writeString(companyId);
        dest.writeString(message);
        dest.writeString(centerTitle);
        dest.writeString(serviceHour);
        dest.writeString(serviceDetails);
        dest.writeString(isMeridianExist);
        dest.writeString(uniqueId);
    }

    @SuppressWarnings("unused")
    public static final Creator<SpotDetailModel> CREATOR = new Creator<SpotDetailModel>() {
        @Override
        public SpotDetailModel createFromParcel(Parcel in) {
            return new SpotDetailModel(in);
        }

        @Override
        public SpotDetailModel[] newArray(int size) {
            return new SpotDetailModel[size];
        }
    };
}