package com.support.admin.instantsupport.HomeScreen;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.support.admin.instantsupport.Application.Constants;
import com.support.admin.instantsupport.Application.InstApplication;
import com.support.admin.instantsupport.IHttpConnectionInterface;
import com.support.admin.instantsupport.Login.UserDetailsModel;
import com.support.admin.instantsupport.R;
import com.support.admin.instantsupport.Utility.AppLog;
import com.support.admin.instantsupport.Utility.GetHttpConnectionResponseTask;
import com.support.admin.instantsupport.Utility.JSONFormater;
import com.support.admin.instantsupport.Utility.Utils;

import org.json.fh.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainAdminHomeActivity extends AppCompatActivity implements Constants, IHttpConnectionInterface, IMainAdminHomeInterface {

    private static final String TAG = "MainAdminHomeActivity";
    private Spinner spinnerEnvironment;
    private InstApplication ApplicationObject;
    private UserDetailsModel objUserDetailsModel;
    private ProgressDialog mProgressDialog;
    private Integer iDevFlag, iTestFlag, iLiveFlag, iEnvFlag;
    private List<String> envTitleList;
    private ArrayList<Integer> envIconList;
    private TextView tvActionbarTitle;
    private int iGetAllDetailsFlag;
    private RecyclerView rvMACompanyName;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_admin_home);
        ApplicationObject = ((InstApplication) getApplicationContext());
        objUserDetailsModel = new UserDetailsModel();
        objUserDetailsModel = getIntent().getExtras().getParcelable(Constants.BundleKeyConstants.ADMIN_DETAIL);
        spinnerEnvironment = (Spinner) findViewById(R.id.spinnerEnvironment);
        tvActionbarTitle = (TextView) findViewById(R.id.tvActionbarTitle);
        rvMACompanyName = (RecyclerView) findViewById(R.id.rvMACompanyName);
        envIconList = new ArrayList<>();
        envTitleList = new ArrayList<>();
        iEnvFlag = 0;
        if (objUserDetailsModel.getEnvironment().contains(DEV_ENV)) {
            envTitleList.add(getString(R.string.development));
            envIconList.add(R.drawable.icon_dev);
            iDevFlag = iEnvFlag;
        }
        if (objUserDetailsModel.getEnvironment().contains(TEST_ENV)) {
            envTitleList.add(getString(R.string.test));
            envIconList.add(R.drawable.icon_test);
            iTestFlag = ++iEnvFlag;
        }
        if (objUserDetailsModel.getEnvironment().contains(LIVE_ENV)) {
            envTitleList.add(getString(R.string.live));
            envIconList.add(R.drawable.icon_live);
            iLiveFlag = ++iEnvFlag;
        }
        SpinnerAdapter adapter = new SpinnerAdapter(this, R.layout.env_spinner_text, envTitleList, envIconList);
        spinnerEnvironment.setAdapter(adapter);
        if (ApplicationObject.getCompanyDetailsModel() != null) {
            tvActionbarTitle.setText(getString(R.string.appbarTitle));
        }
        if (ApplicationObject.getBaseEnv().equals(ENVIRONMENT.DEV)) {
            spinnerEnvironment.setSelection(iDevFlag);
        } else if (ApplicationObject.getBaseEnv().equals(ENVIRONMENT.TEST)) {
            spinnerEnvironment.setSelection(iTestFlag);
        } else {
            spinnerEnvironment.setSelection(iLiveFlag);
        }
        spinnerEnvironment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int positionEnv, long l) {
                if (positionEnv == 0) {
                    ApplicationObject.setBaseEnv(ENVIRONMENT.DEV);
                } else if (positionEnv == 1) {
                    ApplicationObject.setBaseEnv(ENVIRONMENT.TEST);
                } else {
                    ApplicationObject.setBaseEnv(ENVIRONMENT.LIVE);
                }
                getAllCompanyDetails();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void getAllCompanyDetails() {
        if (Utils.isInternetOn(MainAdminHomeActivity.this)) {
            mProgressDialog = ProgressDialog.show(this, null, getResources().getString(R.string.pleasewait));
            mProgressDialog.setContentView(R.layout.progressdialog_custom);
            mProgressDialog.setCancelable(false);
            try {
                iGetAllDetailsFlag = 0;
                new GetHttpConnectionResponseTask(MainAdminHomeActivity.this, ENVIRONMENT.DEV + CLOUD.REQ_GET_ALL_COMPANY_DETAILS, null, Method.GET).execute();
            } catch (Exception e) {
                AppLog.showLogE(TAG, "Exception>>>" + e);
            }
        } else {

            Utils.ShowDialog(MainAdminHomeActivity.this, this.getString(R.string.app_name), getString(R.string.ErrorInternetConn));
        }
    }

    @Override
    public void processConnectionFinish(String jsonObject) {
        try {
            JSONObject jsonResponse = new JSONObject(jsonObject);
            if(jsonResponse.toString().startsWith("{")) {
                AppLog.showLogE(TAG, " processConnectionFinish response=" + jsonResponse);
                if (iGetAllDetailsFlag == 0) {
                    JSONFormater.getAllCompanyDetails(MainAdminHomeActivity.this, jsonResponse);
                }
            }
        } catch (Exception e) {
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismiss();
            e.printStackTrace();
        }

    }

    @Override
    public void processAllCompanyDetailsFinish(ArrayList<CompanyDetailsModel> alCompanyDetailsModel) {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
        if (alCompanyDetailsModel != null && alCompanyDetailsModel.get(0).getMessage().contains(Constants.CONST_CLOUD_CALL.SUCCESS)) {
            rvMACompanyName.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(this);
            rvMACompanyName.setLayoutManager(mLayoutManager);
            mAdapter = new MainAdminHomeCompanyAdapter(MainAdminHomeActivity.this, alCompanyDetailsModel, objUserDetailsModel);
            rvMACompanyName.setAdapter(mAdapter);

        } else if (alCompanyDetailsModel != null && alCompanyDetailsModel.get(0).getMessage().contains(Constants.ERROR_CODE.NO_COMPANY)) {
            Utils.ShowDialogWithDismiss(MainAdminHomeActivity.this, getString(R.string.app_name), getString(R.string.Error_NoUsersCompany), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

        } else {
            Utils.ShowDalogOKCancel(MainAdminHomeActivity.this, getString(R.string.app_name), "\n" + getString(R.string.ServerError));
        }
    }

}
