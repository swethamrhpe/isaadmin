package com.support.admin.instantsupport.Utility;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.support.admin.instantsupport.Application.Constants;
import com.support.admin.instantsupport.HomeScreen.CompanyUserDetailsModel;
import com.support.admin.instantsupport.HomeScreen.MainAdminHomeActivity;
import com.support.admin.instantsupport.HomeScreen.MaintenanceMessageModel;
import com.support.admin.instantsupport.HomeScreen.SpotDetailModel;
import com.support.admin.instantsupport.HomeScreen.SubAdminHomeActivity;
import com.support.admin.instantsupport.Login.LoginActivity;
import com.support.admin.instantsupport.Login.UserDetailsModel;
import com.support.admin.instantsupport.MaintenanceMessage.MaintenanceMessageActivity;
import com.support.admin.instantsupport.MaintenanceMessage.MaintenanceMessageDetailsActivity;
import com.support.admin.instantsupport.SuportSpot.SupportSpotDetailActivity;
import com.support.admin.instantsupport.SuportSpot.SupportSpotActivity;

import com.support.admin.instantsupport.UserScreen.UserActivity;
import com.support.admin.instantsupport.UserScreen.UserDetailActivity;

import java.util.ArrayList;

/**
 * -----------------------------------------------------------------------------
 * Class Name: ScreenNavigator
 * Created By: Suganya
 * Created Date: 09/08/2017
 * Modified By:
 * Modified Date:
 * Purpose: Class is define for call from one activity
 * to second activity with data also.
 * -----------------------------------------------------------------------------
 */

public class ScreenNavigator implements Constants {
    /**
     * -----------------------------------------------------------------------------
     * Method Name: callLanguageSelectActivity
     * Created By: Suganya
     * Created Date: 09/8/2017
     * Modified By:
     * Modified Date:
     * Purpose:  Method will be use for call activity with bundle , which can be pass in parameter
     * -----------------------------------------------------------------------------
     */
    public static void callSubAdminHomeActivity(Activity cntx, UserDetailsModel objUserDetailsModel) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(BundleKeyConstants.ADMIN_DETAIL, objUserDetailsModel);
        callIntent(cntx, bundle, SubAdminHomeActivity.class);
    }
    /**
     * -----------------------------------------------------------------------------
     * Method Name: callMainAdminHomeActivity
     * Created By: Suganya
     * Created Date: 09/8/2017
     * Modified By:
     * Modified Date:
     * Purpose:  Method will be use for call activity with bundle , which can be pass in parameter
     * -----------------------------------------------------------------------------
     */
    public static void callMainAdminHomeActivity(Activity cntx, UserDetailsModel objUserDetailsModel) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(BundleKeyConstants.ADMIN_DETAIL, objUserDetailsModel);
        callIntent(cntx, bundle, MainAdminHomeActivity.class);
    }

    /**
     * -----------------------------------------------------------------------------
     * Method Name: callMaintenanceMessageActivity
     * Created By: Suganya
     * Created Date:31/8/2016
     * Modified By:
     * Modified Date:
     * Purpose: call from one activity to MaintenanceMessageActivity activity
     * -----------------------------------------------------------------------------
     */
    public static void callMaintenanceMessageActivity(Activity cntx, ArrayList<MaintenanceMessageModel> alMaintenanceMessageModel) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(BundleKeyConstants.MAINTENANCE_MESSAGE, alMaintenanceMessageModel);
        callIntent(cntx, bundle,MaintenanceMessageActivity.class);
    }

    public static void callUserDataActivity(Activity cntx, ArrayList<CompanyUserDetailsModel> userDataModel) {
        final Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(BundleKeyConstants.USER_DETAILS,userDataModel);
        callIntent(cntx, bundle,UserActivity.class);
    }

    public static void callAddUserActivity(Activity cntx,CompanyUserDetailsModel companyUserDetailsModel,String selectedType) {
        Bundle bundle = new Bundle();
        //bundle.putParcelableArrayList(BundleKeyConstants.USER_DETAILS,fields);
        bundle.putParcelable(BundleKeyConstants.USER_DETAILS,companyUserDetailsModel);
        bundle.putString("selectedType",selectedType);
        callIntent(cntx, bundle,UserDetailActivity.class);
    }

    public static void callSupportDetailActivity(Activity cntx,SpotDetailModel objSpotDetailModel) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(BundleKeyConstants.SUPPORT_SPOT,objSpotDetailModel);
        callIntent(cntx, bundle,SupportSpotDetailActivity.class);
    }

    /**
     * -----------------------------------------------------------------------------
     * Method Name: callITSupportActivity
     * Created By: Swetha M.R
     * Created Date:11/13/2017
     * Modified By:
     * Modified Date:
     * Purpose: call from one activity to callITSupport activity
     * -----------------------------------------------------------------------------
     */
    public static void callITSupportActivity(Activity cntx, ArrayList<MaintenanceMessageModel> alMaintenanceMessageModel) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(BundleKeyConstants.MAINTENANCE_MESSAGE, alMaintenanceMessageModel);
        callIntent(cntx, bundle,MaintenanceMessageActivity.class);
    }
    public static void callSupportSpotActivity(Activity cntx, ArrayList<SpotDetailModel> alSupportspotLists) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(BundleKeyConstants.SUPPORT_SPOT, alSupportspotLists);
        callIntent(cntx, bundle,SupportSpotActivity.class);
    }
    public static void callSupportSpotDetailActivity(Activity cntx,SpotDetailModel objSpotDetailModel) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(BundleKeyConstants.SUPPORT_SPOT,objSpotDetailModel);
        callIntent(cntx, bundle,SupportSpotDetailActivity.class);
    }



    /**
     * -----------------------------------------------------------------------------
     * Method Name: callMaintenanceMessageDetailsActivity
     * Created By: Suganya
     * Created Date:31/8/2016
     * Modified By:
     * Modified Date:
     * Purpose: call from one activity to MaintenanceMessageDetailsActivity activity
     * -----------------------------------------------------------------------------
     */
    public static void callMaintenanceMessageDetailsActivity(Activity cntx, MaintenanceMessageModel objMaintenanceMessageModel) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(BundleKeyConstants.MAINTENANCE_MESSAGE_DETAILS, objMaintenanceMessageModel);
        callIntent(cntx, bundle, MaintenanceMessageDetailsActivity.class);
    }
    /**
     * -----------------------------------------------------------------------------
     * Method Name: callLoginActivity
     * Created By: Suganya
     * Created Date:19/9/2017
     * Modified By:
     * Modified Date:
     * Purpose: call from one activity to LoginActivity activity
     * -----------------------------------------------------------------------------
     */
    public static void callLoginActivity(Activity cntx) {
        callIntent(cntx, LoginActivity.class);
    }
    /**
     * -----------------------------------------------------------------------------
     * Method Name: callIntent
     * Created By: Suganya
     * Created Date:09/08/2017
     * Modified By:
     * Modified Date:
     * Purpose:  Method will be use for call activity with bundle , which can be pass in parameter
     * -----------------------------------------------------------------------------
     */
    private static void callIntent(Activity context, Bundle bundle, Class<?> cls) {
        Intent intent = new Intent(context, cls);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }
    /**
     * -----------------------------------------------------------------------------
     * Method Name: callIntent
     * Created By: Suganya
     * Created Date:19/9/2017
     * Modified By:
     * Modified Date:
     * Purpose:  Method will be use for call activity with bundle , which can be pass in parameter
     * -----------------------------------------------------------------------------
     */
    public static void callIntent(Activity context, Class<?> cls) {
        Intent intent = new Intent(context, cls);
        context.startActivity(intent);
    }

}
