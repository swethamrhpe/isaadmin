package com.support.admin.instantsupport.MaintenanceMessage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.support.admin.instantsupport.Application.Constants;
import com.support.admin.instantsupport.Application.InstApplication;
import com.support.admin.instantsupport.HomeScreen.MaintenanceMessageModel;
import com.support.admin.instantsupport.Login.UserDetailsModel;
import com.support.admin.instantsupport.R;
import com.support.admin.instantsupport.Utility.ScreenNavigator;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MaintenanceMessageActivity extends AppCompatActivity {

    private InstApplication ApplicationObject;
    private ArrayList<MaintenanceMessageModel> alMaintenanceMessageModel;
    private RecyclerView rvMaintenaceMesasage;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private UserDetailsModel objUserDetailsModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintenance_message);

        ApplicationObject = ((InstApplication) getApplicationContext());
        alMaintenanceMessageModel = getIntent().getExtras().getParcelableArrayList(Constants.BundleKeyConstants.MAINTENANCE_MESSAGE);
        rvMaintenaceMesasage = (RecyclerView) findViewById(R.id.rvMaintenaceMesasage);
        objUserDetailsModel = ApplicationObject.getUserDetailsModel();

        ButterKnife.bind(this);

        rvMaintenaceMesasage.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        rvMaintenaceMesasage.setLayoutManager(mLayoutManager);
        mAdapter = new MaintenanceMessageAdapter(MaintenanceMessageActivity.this,alMaintenanceMessageModel);
        rvMaintenaceMesasage.setAdapter(mAdapter);

    }
    @SuppressWarnings("unused")
    @OnClick(R.id.fabMaintenaceMesasageAddNew)
    public void addNewMaintenanceMessage(View view){
        ScreenNavigator.callMaintenanceMessageDetailsActivity(MaintenanceMessageActivity.this,null);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ScreenNavigator.callSubAdminHomeActivity(MaintenanceMessageActivity.this,objUserDetailsModel);
        finish();
    }
}
