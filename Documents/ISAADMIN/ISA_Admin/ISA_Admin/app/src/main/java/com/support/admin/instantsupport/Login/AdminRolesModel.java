package com.support.admin.instantsupport.Login;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
/**
 * -----------------------------------------------------------------------------
 * Class Name: AdminRolesModel
 * Created By: Suganya
 * Created Date: 07-08-2017
 * Modified By:
 * Modified Date:
 * Purpose: AdminRolesModel for Storing admin role details
 * -----------------------------------------------------------------------------
 */

public class AdminRolesModel  implements Parcelable {

    private ArrayList<String> createUser;

    private ArrayList<String> ITSupportSpot;

    private ArrayList<String> addSubAdmin;

    private ArrayList<String> maintenanceMessage;

    private ArrayList<String> callITSupport;

    private ArrayList<String> selfSupport;

    private ArrayList<String> createCompany;

    private ArrayList<String> chatITSupport;

    public AdminRolesModel() {

    }

    public ArrayList<String> getCreateUser() {
        return createUser;
    }

    public void setCreateUser(ArrayList<String> createUser) {
        this.createUser = createUser;
    }

    public ArrayList<String> getITSupportSpot() {
        return ITSupportSpot;
    }

    public void setITSupportSpot(ArrayList<String> ITSupportSpot) {
        this.ITSupportSpot = ITSupportSpot;
    }

    public ArrayList<String> getAddSubAdmin() {
        return addSubAdmin;
    }

    public void setAddSubAdmin(ArrayList<String> addSubAdmin) {
        this.addSubAdmin = addSubAdmin;
    }

    public ArrayList<String> getMaintenanceMessage() {
        return maintenanceMessage;
    }

    public void setMaintenanceMessage(ArrayList<String> maintenanceMessage) {
        this.maintenanceMessage = maintenanceMessage;
    }

    public ArrayList<String> getCallITSupport() {
        return callITSupport;
    }

    public void setCallITSupport(ArrayList<String> callITSupport) {
        this.callITSupport = callITSupport;
    }

    public ArrayList<String> getSelfSupport() {
        return selfSupport;
    }

    public void setSelfSupport(ArrayList<String> selfSupport) {
        this.selfSupport = selfSupport;
    }

    public ArrayList<String> getCreateCompany() {
        return createCompany;
    }

    public void setCreateCompany(ArrayList<String> createCompany) {
        this.createCompany = createCompany;
    }

    public ArrayList<String> getChatITSupport() {
        return chatITSupport;
    }

    public void setChatITSupport(ArrayList<String> chatITSupport) {
        this.chatITSupport = chatITSupport;
    }


    @Override
    public String toString() {
        return "ClassPojo [createUser = " + createUser + ", ITSupportSpot = " + ITSupportSpot + ", addSubAdmin = " + addSubAdmin + ", maintenanceMessage = " + maintenanceMessage + ", callITSupport = " + callITSupport + ", selfSupport = " + selfSupport + ", createCompany = " + createCompany + ", chatITSupport = " + chatITSupport + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }


    protected AdminRolesModel(Parcel in) {
        createUser = in.createStringArrayList();
        ITSupportSpot = in.createStringArrayList();
        addSubAdmin = in.createStringArrayList();
        maintenanceMessage = in.createStringArrayList();
        callITSupport = in.createStringArrayList();
        selfSupport = in.createStringArrayList();
        createCompany = in.createStringArrayList();
        chatITSupport = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(createUser);
        dest.writeStringList(ITSupportSpot);
        dest.writeStringList(addSubAdmin);
        dest.writeStringList(maintenanceMessage);
        dest.writeStringList(callITSupport);
        dest.writeStringList(selfSupport);
        dest.writeStringList(createCompany);
        dest.writeStringList(chatITSupport);
    }

    public static final Creator<AdminRolesModel> CREATOR = new Creator<AdminRolesModel>() {
        @Override
        public AdminRolesModel createFromParcel(Parcel in) {
            return new AdminRolesModel(in);
        }

        @Override
        public AdminRolesModel[] newArray(int size) {
            return new AdminRolesModel[size];
        }
    };
}