package com.support.admin.instantsupport.MaintenanceMessage;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.support.admin.instantsupport.Application.Constants;
import com.support.admin.instantsupport.Application.InstApplication;
import com.support.admin.instantsupport.HomeScreen.CompanyDetailsModel;
import com.support.admin.instantsupport.HomeScreen.MaintenanceMessageModel;
import com.support.admin.instantsupport.IHttpConnectionInterface;
import com.support.admin.instantsupport.Login.UserDetailsModel;
import com.support.admin.instantsupport.R;
import com.support.admin.instantsupport.Utility.GetHttpConnectionResponseTask;
import com.support.admin.instantsupport.Utility.ScreenNavigator;
import com.support.admin.instantsupport.Utility.Utils;

import org.json.fh.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.support.admin.instantsupport.Application.Constants.CLOUD.REQ_CREATE_MAINTENANCE_MESSAGE;
import static com.support.admin.instantsupport.Application.Constants.CLOUD.REQ_DELETE_MAINTENANCE_MESSAGE;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.COMPANY_ID;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.CREATED_DATE;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.DESCRIPTION;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.EXPIRE_TYPE;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.LANGUAGE;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.LOCATION;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.OUTAGETYPE;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.REGION;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.TITLE;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.USER_TABLE_NAME;

public class MaintenanceMessageDetailsActivity extends AppCompatActivity implements Constants,View.OnClickListener, AdapterView.OnItemClickListener, IHttpConnectionInterface {

    private EditText etMMTitle, etMMDescription, etMMRegion, etMMLocation;
    private LinearLayout llMMExpireTime,llMMStatus;
    private TextView tvMMExpireTime, tvMMLanguageSelect;
    private Spinner spMMOutageType, spMMStatus;
    private MaintenanceMessageModel objMaintenanceMessageModel;
    private int iExpireType, iMaintenanceMessage;
    private InstApplication applicationObject;
    private CompanyDetailsModel objCompanyDetailsModel;
    private AlertDialog dShowDialog;
    private TextView tvAlertListTitle;
    private ListView lvAlertList;
    private LinearLayout llAlertList;
    public static AlertDialog.Builder adBuilder;
    public static Dialog adAlertList;
    private ArrayList<String> alLanguageList;
    private ProgressDialog mProgressDialog;
    private UserDetailsModel objUserDetailsModel;
    ArrayList<MaintenanceMessageModel> alMaintenanceMessageModel;

    @BindView(R.id.btMaintenanceMessageSubmit)
    Button btMaintenanceMessageSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maintenance_message_details);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        objMaintenanceMessageModel = getIntent().getExtras().getParcelable(Constants.BundleKeyConstants.MAINTENANCE_MESSAGE_DETAILS);
        applicationObject = ((InstApplication) getApplicationContext());
        objCompanyDetailsModel = applicationObject.getCompanyDetailsModel();

        objUserDetailsModel = applicationObject.getUserDetailsModel();
        alMaintenanceMessageModel =applicationObject.getAlMaintenanceMessageModel();
        etMMTitle = (EditText) findViewById(R.id.etMMTitle);
        adBuilder = new AlertDialog.Builder(this);
        etMMDescription = (EditText) findViewById(R.id.etMMDescription);
        tvMMLanguageSelect = (TextView) findViewById(R.id.tvMMLanguageSelect);
        etMMLocation = (EditText) findViewById(R.id.etMMLocation);
        etMMRegion = (EditText) findViewById(R.id.etMMRegion);
        tvMMExpireTime = (TextView) findViewById(R.id.tvMMExpireTimeSelect);
        spMMOutageType = (Spinner) findViewById(R.id.spMMOutageType);
        spMMStatus = (Spinner) findViewById(R.id.spMMStatus);
        llMMExpireTime = (LinearLayout) findViewById(R.id.llMMExpireTime);
        llMMStatus = (LinearLayout) findViewById(R.id.llMMStatus);

        iExpireType = 1;
        llMMExpireTime.setVisibility(View.GONE);
        ButterKnife.bind(this);

        if (objMaintenanceMessageModel != null) {
            iMaintenanceMessage = 1;
            llMMStatus.setVisibility(View.VISIBLE);
            btMaintenanceMessageSubmit.setBackgroundColor(getResources().getColor(R.color.colorRed));
            btMaintenanceMessageSubmit.setTextColor(getResources().getColor(R.color.colorWhite));
            btMaintenanceMessageSubmit.setText("Delete");
            etMMTitle.setText(objMaintenanceMessageModel.getTitle());
            etMMDescription.setText(objMaintenanceMessageModel.getDescription());
            if (objMaintenanceMessageModel.getStatus().equals(Constants.MAINTENANCE_STATUS_OPEN)) {
                spMMStatus.setSelection(0);
            } else {
                spMMStatus.setSelection(1);
            }
            tvMMLanguageSelect.setText(objMaintenanceMessageModel.getLang());
            etMMRegion.setText(objMaintenanceMessageModel.getRegion());
            etMMLocation.setText(objMaintenanceMessageModel.getLocation());
            if (objMaintenanceMessageModel.getExpiryType().equals(Constants.MAINTENANCE_EXPIRY_TYPE)) {
                llMMExpireTime.setVisibility(View.VISIBLE);
                tvMMExpireTime.setText(objMaintenanceMessageModel.getExpireTime());
            } else {
                llMMExpireTime.setVisibility(View.GONE);
            }
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                    R.array.system_OutageType_arrays, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spMMOutageType.setAdapter(adapter);
            if (objMaintenanceMessageModel.getOutageType().equals(Constants.OUTAGE_TYPE_NOTIFICATION)) {
                spMMOutageType.setSelection(1);
            } else {
                spMMOutageType.setSelection(0);
            }
        } else {
            iMaintenanceMessage = 0;
            llMMStatus.setVisibility(View.GONE);
        }
        tvMMExpireTime.setOnClickListener(MaintenanceMessageDetailsActivity.this);
        tvMMLanguageSelect.setOnClickListener(MaintenanceMessageDetailsActivity.this);

    }

    @OnClick(R.id.btMaintenanceMessageSubmit)
    public void submitMaintenanceMessage(View view) {
        if (Utils.isInternetOn(MaintenanceMessageDetailsActivity.this)) {
            mProgressDialog = ProgressDialog.show(this, null, getResources().getString(R.string.pleasewait));
            mProgressDialog.setContentView(R.layout.progressdialog_custom);
            mProgressDialog.setCancelable(false);
            JSONObject jsonParam = new JSONObject();
            if (iMaintenanceMessage == 0) {
                if (objCompanyDetailsModel.getCompanyId() != null && tvMMLanguageSelect.getText() != null && etMMRegion.getText() != null && etMMLocation.getText() != null && String.valueOf(System.currentTimeMillis()) != null &&
                        etMMTitle.getText() != null && etMMDescription.getText() != null) {
                    jsonParam.put(COMPANY_ID, objCompanyDetailsModel.getCompanyId());
                    jsonParam.put(LANGUAGE, tvMMLanguageSelect.getText());
                    jsonParam.put(EXPIRE_TYPE, iExpireType);
                    jsonParam.put(REGION, etMMRegion.getText());
                    jsonParam.put(LOCATION, etMMLocation.getText());
                    jsonParam.put(CREATED_DATE, String.valueOf(System.currentTimeMillis()));
                    jsonParam.put(TITLE, etMMTitle.getText());
                    jsonParam.put(DESCRIPTION, etMMDescription.getText());
                    jsonParam.put(OUTAGETYPE, spMMOutageType.getSelectedItem());
                    jsonParam.put(USER_TABLE_NAME, objCompanyDetailsModel.getUserTableName());
                    if (iExpireType == 2) {
                        if (tvMMExpireTime.getText() != null) {
                            jsonParam.put(CONST_CLOUD_CALL.EXPIRE_TIME, tvMMExpireTime.getText());
                            new GetHttpConnectionResponseTask(MaintenanceMessageDetailsActivity.this, applicationObject.getBaseEnv() + REQ_CREATE_MAINTENANCE_MESSAGE, jsonParam, Constants.Method.POST).execute();
                        } else {
                            if (mProgressDialog != null && mProgressDialog.isShowing())
                                mProgressDialog.dismiss();
                            Utils.ShowDialog(MaintenanceMessageDetailsActivity.this, this.getString(R.string.app_name), getString(R.string.ErrorInternetConn));
                        }
                    } else {
                        new GetHttpConnectionResponseTask(MaintenanceMessageDetailsActivity.this, applicationObject.getBaseEnv() + REQ_CREATE_MAINTENANCE_MESSAGE, jsonParam, Constants.Method.POST).execute();
                    }

                } else {
                    if (mProgressDialog != null && mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    Utils.ShowDialog(MaintenanceMessageDetailsActivity.this, this.getString(R.string.app_name), getString(R.string.errorExpireTime));
                }
            }else{
                ArrayList<String> serviceIds = new ArrayList<>();
                serviceIds.add(objMaintenanceMessageModel.getUniqueId());
                jsonParam.put(CONST_CLOUD_CALL.SERVICE_ID, serviceIds);
                new GetHttpConnectionResponseTask(MaintenanceMessageDetailsActivity.this, applicationObject.getBaseEnv() + REQ_DELETE_MAINTENANCE_MESSAGE , jsonParam, Constants.Method.POST).execute();
            }
        } else {
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismiss();
            Utils.ShowDialog(MaintenanceMessageDetailsActivity.this, this.getString(R.string.app_name), getString(R.string.checkData));
        }

    }

    /**
     * -----------------------------------------------------------------------------
     * Method Name: setExpireTime
     * Created By : Suganya
     * Created Date: 07/09/2017
     * Modified By:
     * Modified Date:
     * Purpose    : This method will Appear Dialog with user friendly and
     * understandable format and on press OK, it will dismiss
     * -----------------------------------------------------------------------------
     */
    public void setExpireTime() {
        final Dialog madFinishActivity;
        madFinishActivity = new Dialog(MaintenanceMessageDetailsActivity.this);
        madFinishActivity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        madFinishActivity.setContentView(R.layout.maintenance_time_picker);
        madFinishActivity.setCancelable(true);

        final NumberPicker timePickerHour = (NumberPicker) madFinishActivity.findViewById(R.id.timePickerHour);
        Button mbtnAlrCustOk = (Button) madFinishActivity.findViewById(R.id.btnAlrCustOk);

        timePickerHour.setMinValue(0);
        timePickerHour.setMaxValue(23);

        mbtnAlrCustOk.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                madFinishActivity.dismiss();
                return;
            }
        });
        madFinishActivity.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode,
                                 KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
                    dialog.dismiss();
                    tvMMExpireTime.setText(timePickerHour.getValue());

                    return true;
                }

                return false;
            }
        });
        madFinishActivity.show();
    }

    /**
     * -----------------------------------------------------------------------------
     * Method Name: onExpireTypeRadioButtonClicked
     * Created By : Suganya
     * Created Date: 07/09/2017
     * Modified By:
     * Modified Date:
     * Purpose    : This method will provide on change of radio button
     * -----------------------------------------------------------------------------
     */

    public void onExpireTypeRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch (view.getId()) {
            case R.id.rbMMExpireTypeManual:
                if (checked)
                    iExpireType = 1;
                llMMExpireTime.setVisibility(View.GONE);
                break;
            case R.id.rbMMExpireTypeTimer:
                if (checked)
                    iExpireType = 2;
                llMMExpireTime.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvMMExpireTimeSelect:

                setExpireTime();
                break;
            case R.id.tvMMLanguageSelect:
                alLanguageList = objCompanyDetailsModel.getLangSupported();

                if (alLanguageList != null && alLanguageList.size() > 0) {
                    tvAlertListTitle = new TextView(new ContextThemeWrapper(MaintenanceMessageDetailsActivity.this, R.style.TextViewAlterList));
                    lvAlertList = new ListView(MaintenanceMessageDetailsActivity.this);
                    llAlertList = new LinearLayout(MaintenanceMessageDetailsActivity.this);
                    llAlertList.setOrientation(LinearLayout.VERTICAL);
                    llAlertList.setGravity(Gravity.CENTER);
                    tvAlertListTitle.setText(getString(R.string.etMMLanguage));
                    alLanguageList.add(getResources().getString(R.string.Cancel));
                    llAlertList.addView(tvAlertListTitle);
                    llAlertList.addView(lvAlertList);
                    adBuilder.setView(llAlertList);
                    adBuilder.setCancelable(false);
                    lvAlertList.setAdapter(new CustomAlertListAdapter(MaintenanceMessageDetailsActivity.this, alLanguageList));
                    lvAlertList.setOnItemClickListener(this);
                    adAlertList = adBuilder.show();
                }
                break;
        }

    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        adAlertList.dismiss();
        if (objCompanyDetailsModel.getLangSupported().size() > i) {
            tvMMLanguageSelect.setText(objCompanyDetailsModel.getLangSupported().get(i).trim().split("-")[0]);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ScreenNavigator.callMaintenanceMessageActivity(MaintenanceMessageDetailsActivity.this,alMaintenanceMessageModel);
        finish();

    }

    @Override
    public void processConnectionFinish(String jsonObject) {
        ScreenNavigator.callSubAdminHomeActivity(MaintenanceMessageDetailsActivity.this,objUserDetailsModel);
        finish();
    }
}
