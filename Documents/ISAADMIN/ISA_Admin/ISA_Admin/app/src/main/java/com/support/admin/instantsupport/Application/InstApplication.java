package com.support.admin.instantsupport.Application;

import android.support.multidex.MultiDexApplication;

import com.support.admin.instantsupport.HomeScreen.CallITSupportModel;
import com.support.admin.instantsupport.HomeScreen.ChatITSupportModel;
import com.support.admin.instantsupport.HomeScreen.CompanyDetailsModel;
import com.support.admin.instantsupport.HomeScreen.CompanyUserDetailsModel;
import com.support.admin.instantsupport.HomeScreen.MaintenanceMessageModel;
import com.support.admin.instantsupport.HomeScreen.SelfSupportModel;
import com.support.admin.instantsupport.HomeScreen.SpotDetailModel;
import com.support.admin.instantsupport.Login.UserDetailsModel;


import java.util.ArrayList;

/**
 * -----------------------------------------------------------------------------
 * Class Name: InstApplication
 * Created By: Suganya
 * Created Date: 31-07-2017
 * Modified By:
 * Modified Date:
 * Purpose: Application class that hold all sheared-prefetance and application contender
 * -----------------------------------------------------------------------------
 */

public class InstApplication extends MultiDexApplication {

    private String baseEnv;

    public String getBaseEnv() {
        return baseEnv;
    }

    public void setBaseEnv(String baseEnv) {
        this.baseEnv = baseEnv;
    }

    private UserDetailsModel objUserDetailsModel;

    public void setUserDetailsModel(UserDetailsModel objUserDetailsModel) {
        this.objUserDetailsModel = objUserDetailsModel;
    }

    public UserDetailsModel getUserDetailsModel() {
        return this.objUserDetailsModel;
    }

    private ArrayList<MaintenanceMessageModel> alMaintenanceMessageModel;

    public void setAlMaintenanceMessageModel(ArrayList<MaintenanceMessageModel> alMaintenanceMessageModel) {
        this.alMaintenanceMessageModel = alMaintenanceMessageModel;
    }

    public ArrayList<MaintenanceMessageModel> getAlMaintenanceMessageModel() {
        return this.alMaintenanceMessageModel;
    }

    private CompanyDetailsModel objCompanyDetailsModel;

    public void setCompanyDetailsModel(CompanyDetailsModel objCompanyDetailsModel) {
        this.objCompanyDetailsModel = objCompanyDetailsModel;
    }

    public CompanyDetailsModel getCompanyDetailsModel() {
        return this.objCompanyDetailsModel;
    }

    private ArrayList<CompanyUserDetailsModel> alCompanyUserDetailsModel;

    public ArrayList<CompanyUserDetailsModel> getAlCompanyUserDetailsModel() {
        return alCompanyUserDetailsModel;
    }

    public void setAlCompanyUserDetailsModel(ArrayList<CompanyUserDetailsModel> alCompanyUserDetailsModel) {
        this.alCompanyUserDetailsModel = alCompanyUserDetailsModel;
    }

    private ArrayList<CallITSupportModel> alCallITSupportModel;

    public ArrayList<CallITSupportModel> getAlCallITSupportModel() {
        return alCallITSupportModel;
    }

    public void setAlCallITSupportModel(ArrayList<CallITSupportModel> alCallITSupportModel) {
        this.alCallITSupportModel = alCallITSupportModel;
    }

    private ArrayList<ChatITSupportModel> alChatITSupportModel;

    public ArrayList<ChatITSupportModel> getAlChatITSupportModel() {
        return alChatITSupportModel;
    }

    public void setAlChatITSupportModel(ArrayList<ChatITSupportModel> alChatITSupportModel) {
        this.alChatITSupportModel = alChatITSupportModel;
    }

    private ArrayList<SpotDetailModel> alSportDetailModel;

    public ArrayList<SpotDetailModel> getAlSportDetailModel() {
        return alSportDetailModel;
    }

    public void setAlSportDetailModel(ArrayList<SpotDetailModel> alSportDetailModel) {
        this.alSportDetailModel = alSportDetailModel;
    }

    private ArrayList<SelfSupportModel> alSelfSupportModel;

    public ArrayList<SelfSupportModel> getAlSelfSupportModel() {
        return alSelfSupportModel;
    }

    public void setAlSelfSupportModel(ArrayList<SelfSupportModel> alSelfSupportModel) {
        this.alSelfSupportModel = alSelfSupportModel;
    }



}
