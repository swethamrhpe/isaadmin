package com.support.admin.instantsupport.Application;

/**-----------------------------------------------------------------------------
 Procedure Name: Constants
 Created By: Suganya
 Created Date:31/07/2017
 Modified By:
 Modified Date:
 Purpose: Put All constant here
 -----------------------------------------------------------------------------*/

public interface Constants {
    public static final String FONT_ARIAL="fonts/arial.ttf";
    public static final String EMAIL_PATTERN="[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    public static final String LOGIN_FAILED="User login failed";
    public static final String HEADER_TYPE = "contentType";
    public static final String HEADER_VALUE = "application/json";
    public static final int CONNECTION_TIMEOUT = 25000;
    public static final String NORMAL_USER_TYPE = "1";
    public static final String VIP_USER_TYPE = "2";
    public static final String TYPE_USER_TYPE = "3";
    public static final String MAINTENANCE_EXPIRY_TYPE = "2";
    public static final String OUTAGE_TYPE_NOTIFICATION = "notification";
    public static final String MAINTENANCE_STATUS_OPEN = "1";
    public static final String DEV_ENV = "D";
    public static final String TEST_ENV = "T";
    public static final String LIVE_ENV = "L";



    public class BundleKeyConstants {
        public static final String ADMIN_DETAIL = "admindetail";
        public static final String USER_DETAILS = "userdataModel";
        public static final String MAINTENANCE_MESSAGE = "maintenancemessage";
        public static final String MAINTENANCE_MESSAGE_DETAILS = "maintenancemessagedetails";
        public static final String SUPPORT_SPOT = "supportspot";
    }
    public class ERROR_CODE
    {
        public static final String NO_ADMIN_USER = "100078";
        public static final String NO_ADMIN_COMPANY= "100056";
        public static final String NO_ADMIN_COMPANY_USER= "100048";
        public static final String NO_COMPANY= "100048";
    }

    public class ADMIN_TYPE {
        public  static  final String MAIN_ADMIN = "1";
        public  static  final String SUB_ADMIN = "2";
    }


    public static class CONST_CLOUD_CALL {
        public static String MESSAGE = "message";
        public static String SESSIONTOKEN = "sessionToken";
        public static String STATUS = "status";
        public static String USERID = "userId";
        public static String ERROR = "error";
        public static String ADMIN_DETAILS = "adminDetails";
        public static String EMAIL_ID = "emailId";
        public static String ADMIN_TYPE = "adminType";
        public static String DOMAIN = "domain";
        public static String ENVIRONMENT = "environment";
        public static String ADMIN_ROLE = "role";
        public static String CREATE_COMPANY = "createCompany";
        public static String ADD_SUBADMIN = "addSubAdmin";
        public static String CALL_SUPPORT = "callITSupport";
        public static String CHAT_SUPPORT = "chatITSupport";
        public static String SPOT_SUPPORT = "ITSupportSpot";
        public static String SELF_SUPPORT = "selfSupport";
        public static String MAINTENANCE_MSG = "maintenanceMessage";
        public static String CREATE_USER = "createUser";
        public static String ERROR_RESPONSE ="errorResponse";
        public static String ERRORCODE = "errorCode";
        public static final String SUCCESS="success";
        public static String ADMIN_COMPANY_DETAILS = "companyDetails";
        public static String COMPANY_ID = "companyId";
        public static String COMPANY_LOGO = "companyLogo";
        public static String COMPANY_NAME = "companyName";
        public static String CONFIG_NOR = "config_U0";
        public static String CONFIG_VIP = "config_U1";
        public static String LANG_SUPPORT = "langSupported";
        public static String PRIVACY_URL = "privacyURL";
        public static String RESET_PASSWORD_URL = "resetPasswordUrl";
        public static String TECH_TABLE_NAME = "techTableName";
        public static String USER_TABLE_NAME = "userTableName";
        public static String UNIQUE_ID = "uniqueId";
        public static String USER_DATA = "UserData";

        public static String USER_LIST = "list";
        public static String FIELDS = "fields";
        public static String USER_EMAILID = "userEmail";
        public static String USER_NAME = "userName";
        public static String ADD_USER_NAME = "username";

        public static String USER_TYPE = "type";
        public static String ASSIGNED_TECH = "assignedTechnician";
        public static String PRIMARY_PHONE_NUM = "primaryPhoneNum";
        public static String SECONDARY_PHONE_NUM1 = "secondaryPhoneNum1";
        public static String SECONDARY_PHONE_NUM2 = "secondaryPhoneNum2";
        public static String SELECTED_LANG = "selectedLang";
        public static String GUID = "guid";
        public static String CALL_IT_SUPPORT_DETAILS = "callITSupportDetails";
        public static String NO_OF_QUEUES = "numberOfQueues";
        public static String LANG = "lang";
        public static String LANGUAGE = "language";
        public static String START_TIME_UTC = "startTimeInUTC";
        public static String END_TIME_UTC = "endTimeInUTC";
        public static String SERVICE_DAY_PER_WEEK = "serviceDaysPerWeek";
        public static String CONFIG = "config";
        public static String NUMBER = "number";
        public static String PHONE_NUMBERS = "phoneNumbers";
        public static String PROMPT = "prompt";
        public static String QUEUE_ID = "queueId";
        public static String QUEUE_NAME = "queueName";
        public static String PRIMARY_ADDRESS = "primaryAddress";
        public static String SECONDARY_ADDRESS = "secondaryAddress";
        public static String QUESTION_ID = "questionId";
        public static String VDN_NUM = "vdnNum";
        public static String PHONE_TYPE = "phoneType";
        public static String REQUEST_ID = "requestId";
        public static String CHAT_IT_SUPPORT_DETAILS = "chatITSupportDetails";
        public static String CHAT_BASE_URL = "chatBaseUrl";
        public static String CHAT_OPTION = "chatOptions";
        public static String ESC_QUESTION = "escQuestion";
        public static String CHAT_CATEGORY = "chatCategory";
        public static String CHAT_CONFIG = "chatConfig";
        public static String AIC = "AIC";
        public static String CHAT_TYPE = "chatType";

        public static String SPOTDETAILS = "spotDetails";
        public static String ADDRESSFIELD1 = "addressField1";
        public static String ADDRESSFIELD2 = "addressField2";
        public static String ZIP_PIN = "zipCode";
        public static String SUPPORTSPOTTYPE = "supportSpotType";
        public static String STATE = "state";
        public static String CENTERTITLE = "centerTitle";
        public static String SERVICEDETAILS = "serviceDetails";
        public static String SERVICEHOUR = "serviceHour";
        public static String ISMERIDIANEXIST = "isMeridianExist";
        public static String COUNTRYCODE = "countryCode";
        public static String LATITUDE = "latitude";
        public static String LONGITUDE = "longitude";
        public static String SUBSTITUDETID = "substituteTId";
        public static String TRACKING = "tracking";

        public static String MAINTENANCE_MESSAGE="maintenanceMessage";
        public static final String DESCRIPTION= "Description";
        public static final String EXPIRE_TIME= "expireTime";
        public static final String LOCATION = "location";
        public static final String MESSAGEID = "messageId";
        public static final String OUTAGETYPE = "outageType";
        public static final String REGION = "region";
        public static final String STAUTS = "status";
        public static final String TITLE = "title";
        public static final String CREATED_DATE = "createdDate";
        public static final String EXPIRE_TYPE = "expiryType";
        public static final String SELF_SUPPORT_DETAILS = "selfSupportDetails";
        public static final String COMPANY_DESCRIPTION = "companyDescription";
        public static final String SELF_SUP_OPTIONS = "selfSupportOptions";
        public static final String SERVICE_TYPE = "supportSpotType";
        public static final String SERVICE_URL = "serviceURL";
        public static final String PASSWORD = "password";
        public static final String OPTION_NAME = "optionName";
        public static final String COOKIE = "cookie";
        public static final String TENANT_ID = "tenantId";
        public static final String PATH = "path";
        public static final String SERVICE_ID = "serviceId";
        public static final String COMPANIES = "companies";





    }
    public class Method
    {
        public static String GET="GET";
        public static String POST="POST";

    }
    public class ENVIRONMENT
    {
        public static final String DEV = "https://hpe-isa-j3hwv6nnq6rc7x7hfoeltxvo-hpe-dev.mbaas1.hpe.feedhenry.com";
        public static final String TEST = "https://hpe-isa-j3hwv6nnq6rc7x7hfoeltxvo-hpe-test.mbaas1.hpe.feedhenry.com";
        public static final String LIVE = "https://hpe-isa-j3hwv6nnq6rc7x7hfoeltxvo-hpe-live.mbaas2.hpe.feedhenry.com";
    }
    public class CLOUD {
        public static final String POLICE_ID = "@$$$InstantSupportAuth@@@$";
        public static final String REQ_METHOD_USER_DETAIL = "/getAdminDetails";
        public static final String REQ_METHOD_SUB_ADMIN_COMPANY_DETAIL = "/getSubAdminCompanyDetails";
        public static final String REQ_GET_USER_DETAILS_COMPANY = "/getUserDetailsForCompany";
        public static final String REQ_GET_CALL_IT_SUPPORT_FOR_COMPANY = "/getCallITSupportForCompany";
        public static final String REQ_GET_CHAT_IT_SUPPORT_FOR_COMPANY = "/getChatITSupportForCompany";
        public static final String REQ_GET_SPOT_DETAILS = "/getWalkInCenters";
        public static final String REQ_GET_ALL_SELF_SUPPORT_DETAILS = "/getAllSelfSupport";
        public static final String REQ_GET_ALL_MAINTENANCE_MESSAGE = "/getAllMaintenanceMessage";
        public static final String REQ_CREATE_MAINTENANCE_MESSAGE = "/createMaintenanceMessage";
        public static final String REQ_CREATE_USER_DATA = "/createUser";
        public static final String REQ_DELETE_MAINTENANCE_MESSAGE = "/deleteMaintenanceMessage";
        public static final String REQ_GET_ALL_COMPANY_DETAILS = "/getAllCompanyDetails";
        public static final String REQ_CREATE_WALKIN_CENTER = "/createWalkInCenter";


    }
}

