package com.support.admin.instantsupport.HomeScreen;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * -----------------------------------------------------------------------------
 * Class Name: SelfSupportOptionModel
 * Created By: Suganya
 * Created Date: 14-09-2017
 * Modified By:
 * Modified Date:
 * Purpose: SelfSupportOptionModel for Storing admin self Support option details
 * -----------------------------------------------------------------------------
 */

public class SelfSupportOptionModel implements Parcelable {

    private String optionName;

    private String serviceURL;

    private String requestId;

    private String userName;

    private String serviceType;

    private String password;

    private String path;

    private String cookie;

    private String tenantId;

    public SelfSupportOptionModel(){

    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public String getServiceURL() {
        return serviceURL;
    }

    public void setServiceURL(String serviceURL) {
        this.serviceURL = serviceURL;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    @Override
    public String toString() {
        return "ClassPojo [optionName = " + optionName + ", serviceURL = " + serviceURL + ", requestId = " + requestId + ", userName = " + userName + ", serviceType = " + serviceType + ", password = " + password + ", path = " + path + ", cookie = " + cookie + ", tenantId = " + tenantId + "]";
    }

    protected SelfSupportOptionModel(Parcel in) {
        optionName = in.readString();
        serviceURL = in.readString();
        requestId = in.readString();
        userName = in.readString();
        serviceType = in.readString();
        password = in.readString();
        path = in.readString();
        cookie = in.readString();
        tenantId = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(optionName);
        parcel.writeString(serviceURL);
        parcel.writeString(requestId);
        parcel.writeString(userName);
        parcel.writeString(serviceType);
        parcel.writeString(password);
        parcel.writeString(path);
        parcel.writeString(cookie);
        parcel.writeString(tenantId);
    }

    public static final Creator<SelfSupportOptionModel> CREATOR = new Creator<SelfSupportOptionModel>() {
        @Override
        public SelfSupportOptionModel createFromParcel(Parcel in) {
            return new SelfSupportOptionModel(in);
        }

        @Override
        public SelfSupportOptionModel[] newArray(int size) {
            return new SelfSupportOptionModel[size];
        }
    };
}
