package com.support.admin.instantsupport.Utility;

import android.app.Activity;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.support.admin.instantsupport.Application.Constants;
import com.support.admin.instantsupport.HomeScreen.CallITPhoneNumberModel;
import com.support.admin.instantsupport.HomeScreen.CallITSupportModel;
import com.support.admin.instantsupport.HomeScreen.ChatITOptionModel;
import com.support.admin.instantsupport.HomeScreen.ChatITSupportModel;
import com.support.admin.instantsupport.HomeScreen.CompanyDetailsModel;
import com.support.admin.instantsupport.HomeScreen.CompanyUserDetailsModel;
import com.support.admin.instantsupport.HomeScreen.IMainAdminHomeInterface;
import com.support.admin.instantsupport.HomeScreen.ISubAdminInterface;
import com.support.admin.instantsupport.HomeScreen.MaintenanceMessageModel;
import com.support.admin.instantsupport.HomeScreen.SelfSupportModel;
import com.support.admin.instantsupport.HomeScreen.SelfSupportOptionModel;
import com.support.admin.instantsupport.HomeScreen.SpotDetailModel;
import com.support.admin.instantsupport.Login.AdminRolesModel;
import com.support.admin.instantsupport.Login.ILoginInterface;
import com.support.admin.instantsupport.Login.LoginScreenModel;
import com.support.admin.instantsupport.Login.UserDetailsModel;


import org.json.fh.JSONArray;
import org.json.fh.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.ERRORCODE;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.ERROR_RESPONSE;

/**
 * -----------------------------------------------------------------------------
 * Class Name: JSONFormater
 * Created By: Suganya
 * Created Date: 31-07-2017
 * Modified By:
 * Modified Date:
 * Purpose: Put all JSON call inside this class
 * -----------------------------------------------------------------------------
 */

public class JSONFormater implements Constants {
    private static final String TAG = "JSONFormater";

    /**
     * -----------------------------------------------------------------------------
     * Method Name: doJSONAuthLogin
     * Created By : Suganya
     * Created Date:31/07/2017
     * Modified By:
     * Modified Date:
     * Purpose    : method will used for login authentication
     * -----------------------------------------------------------------------------
     */
    public static void doJSONAuthLogin(final Activity cntx, JSONObject objJSON) {
        final LoginScreenModel objLoginData = new LoginScreenModel();
        ILoginInterface iLoginInterface = (ILoginInterface) cntx;
        objLoginData.setMessage(objJSON.getString(CONST_CLOUD_CALL.MESSAGE));
        objLoginData.setSessionToken(objJSON.getString(CONST_CLOUD_CALL.SESSIONTOKEN));
        objLoginData.setStatus(objJSON.getString(CONST_CLOUD_CALL.STATUS));
        objLoginData.setUserId(objJSON.getString(CONST_CLOUD_CALL.USERID));

        iLoginInterface.processFinish(objLoginData);
    }

    /**
     * -----------------------------------------------------------------------------
     * Method Name: getJSONUserInfoClude
     * Created By : Suganya
     * Created Date:31/07/2017
     * Modified By:
     * Modified Date:
     * Purpose    : method will used to get user details
     * -----------------------------------------------------------------------------
     */
    public static void getJSONUserInfoClude(final Activity cntx, JSONObject res) {
        final ILoginInterface iLoginInterface = (ILoginInterface) cntx;
        final UserDetailsModel objUserDetail = new UserDetailsModel();
        final AdminRolesModel objAdminRolesModel = new AdminRolesModel();
        JSONObject objUserProfile = null;


        if (res.has(CONST_CLOUD_CALL.ADMIN_DETAILS) && res.getJSONObject(CONST_CLOUD_CALL.ADMIN_DETAILS) != null) {

            objUserProfile = res.getJSONObject(CONST_CLOUD_CALL.ADMIN_DETAILS);
            objUserDetail.setEmailId(objUserProfile.getString(CONST_CLOUD_CALL.EMAIL_ID));
            objUserDetail.setAdminType(objUserProfile.getString(CONST_CLOUD_CALL.ADMIN_TYPE));
            objUserDetail.setDomain(objUserProfile.getString(CONST_CLOUD_CALL.DOMAIN));
            objUserDetail.setAdminType(objUserProfile.getString(CONST_CLOUD_CALL.ADMIN_TYPE));
            if (objUserProfile.has(CONST_CLOUD_CALL.ENVIRONMENT)) {
                JSONArray arrAdminEnv = objUserProfile.getJSONArray(CONST_CLOUD_CALL.ENVIRONMENT);
                ArrayList<String> sArrEnv = new ArrayList<String>();
                for (int iIncrEnv = 0; iIncrEnv < arrAdminEnv.length(); iIncrEnv++) {
                    sArrEnv.add(arrAdminEnv.get(iIncrEnv).toString());
                }
                objUserDetail.setEnvironment(sArrEnv);
            }
            objUserDetail.setMessage(CONST_CLOUD_CALL.SUCCESS);
            JSONObject objAdminRoles = objUserProfile.getJSONObject(CONST_CLOUD_CALL.ADMIN_ROLE);

            if (objAdminRoles.has(CONST_CLOUD_CALL.CREATE_COMPANY)) {
                JSONArray arrCreateCompany = objAdminRoles.getJSONArray(CONST_CLOUD_CALL.CREATE_COMPANY);
                ArrayList<String> sArrCreateCompany = new ArrayList<String>();
                for (int iIncrCreateCompany = 0; iIncrCreateCompany < arrCreateCompany.length(); iIncrCreateCompany++) {
                    sArrCreateCompany.add(arrCreateCompany.get(iIncrCreateCompany).toString());
                }
                objAdminRolesModel.setCreateCompany(sArrCreateCompany);
            }
            if (objAdminRoles.has(CONST_CLOUD_CALL.ADD_SUBADMIN)) {
                JSONArray arrAddSubAdmin = objAdminRoles.getJSONArray(CONST_CLOUD_CALL.ADD_SUBADMIN);
                ArrayList<String> sArrAddSubAdmin = new ArrayList<String>();
                for (int iIncrAddSubAdmin = 0; iIncrAddSubAdmin < arrAddSubAdmin.length(); iIncrAddSubAdmin++) {
                    sArrAddSubAdmin.add(arrAddSubAdmin.get(iIncrAddSubAdmin).toString());
                }
                objAdminRolesModel.setAddSubAdmin(sArrAddSubAdmin);
            }
            if (objAdminRoles.has(CONST_CLOUD_CALL.CALL_SUPPORT)) {
                JSONArray arrCallSup = objAdminRoles.getJSONArray(CONST_CLOUD_CALL.CALL_SUPPORT);
                ArrayList<String> sArrCallSup = new ArrayList<String>();
                for (int iIncrCallSup = 0; iIncrCallSup < arrCallSup.length(); iIncrCallSup++) {
                    sArrCallSup.add(arrCallSup.get(iIncrCallSup).toString());
                }
                objAdminRolesModel.setCallITSupport(sArrCallSup);

            }
            if (objAdminRoles.has(CONST_CLOUD_CALL.CHAT_SUPPORT)) {
                JSONArray arrChatSup = objAdminRoles.getJSONArray(CONST_CLOUD_CALL.CHAT_SUPPORT);
                ArrayList<String> sArrChatSup = new ArrayList<String>();
                for (int iIncrChatSup = 0; iIncrChatSup < arrChatSup.length(); iIncrChatSup++) {
                    sArrChatSup.add(arrChatSup.get(iIncrChatSup).toString());
                }
                objAdminRolesModel.setChatITSupport(sArrChatSup);
            }
            if (objAdminRoles.has(CONST_CLOUD_CALL.SPOT_SUPPORT)) {
                JSONArray arrSpotSup = objAdminRoles.getJSONArray(CONST_CLOUD_CALL.SPOT_SUPPORT);
                ArrayList<String> sArrSpotSup = new ArrayList<String>();
                for (int iIncrSpotSup = 0; iIncrSpotSup < arrSpotSup.length(); iIncrSpotSup++) {
                    sArrSpotSup.add(arrSpotSup.get(iIncrSpotSup).toString());
                }
                objAdminRolesModel.setITSupportSpot(sArrSpotSup);
            }
            if (objAdminRoles.has(CONST_CLOUD_CALL.SELF_SUPPORT)) {
                JSONArray arrSelfSup = objAdminRoles.getJSONArray(CONST_CLOUD_CALL.SELF_SUPPORT);
                ArrayList<String> sArrSelfSup = new ArrayList<String>();
                for (int iIncrSelfSup = 0; iIncrSelfSup < arrSelfSup.length(); iIncrSelfSup++) {
                    sArrSelfSup.add(arrSelfSup.get(iIncrSelfSup).toString());
                }
                objAdminRolesModel.setSelfSupport(sArrSelfSup);
            }
            if (objAdminRoles.has(CONST_CLOUD_CALL.MAINTENANCE_MSG)) {
                JSONArray arrMaintenanceMsg = objAdminRoles.getJSONArray(CONST_CLOUD_CALL.MAINTENANCE_MSG);
                ArrayList<String> sArrMaintenanceMsg = new ArrayList<String>();
                for (int iIncrMaintenanceMsg = 0; iIncrMaintenanceMsg < arrMaintenanceMsg.length(); iIncrMaintenanceMsg++) {
                    sArrMaintenanceMsg.add(arrMaintenanceMsg.get(iIncrMaintenanceMsg).toString());
                }
                objAdminRolesModel.setMaintenanceMessage(sArrMaintenanceMsg);
            }
            if (objAdminRoles.has(CONST_CLOUD_CALL.CREATE_USER)) {
                JSONArray arrCreateUser = objAdminRoles.getJSONArray(CONST_CLOUD_CALL.CREATE_USER);
                ArrayList<String> sArrCreateUser = new ArrayList<String>();
                for (int iIncrCreateUser = 0; iIncrCreateUser < arrCreateUser.length(); iIncrCreateUser++) {
                    sArrCreateUser.add(arrCreateUser.get(iIncrCreateUser).toString());
                }
                objAdminRolesModel.setCreateUser(sArrCreateUser);
            }
            objUserDetail.setRole(objAdminRolesModel);
            iLoginInterface.processUserDetailsFinish(objUserDetail);
        } else if (res.has(ERROR_RESPONSE) && res.getJSONObject(ERROR_RESPONSE) != null) {
            objUserDetail.setMessage(res.getJSONObject(ERROR_RESPONSE).getString(ERRORCODE));
            iLoginInterface.processUserDetailsFinish(objUserDetail);
        } else {
            iLoginInterface.processUserDetailsFinish(null);
        }


    }

    /**
     * -----------------------------------------------------------------------------
     * Method Name: getAdminCompanyDetails
     * Created By : Suganya
     * Created Date: 31/07/2017
     * Modified By:
     * Modified Date:
     * Purpose    : method will used to get the admin company details
     * -----------------------------------------------------------------------------
     */
    public static void getAdminCompanyDetails(final Activity cntx, JSONObject res) {
        final ISubAdminInterface iSubAdminInterface = (ISubAdminInterface) cntx;
        final CompanyDetailsModel objCompanyDetailsModel = new CompanyDetailsModel();
        JSONObject objCompanyDetail = null;

        if (res.has(CONST_CLOUD_CALL.ADMIN_COMPANY_DETAILS) && res.getJSONObject(CONST_CLOUD_CALL.ADMIN_COMPANY_DETAILS) != null) {
            objCompanyDetail = res.getJSONObject(CONST_CLOUD_CALL.ADMIN_COMPANY_DETAILS);
            objCompanyDetailsModel.setCompanyId(objCompanyDetail.getString(CONST_CLOUD_CALL.COMPANY_ID));
            objCompanyDetailsModel.setCompanyLogo(objCompanyDetail.getString(CONST_CLOUD_CALL.COMPANY_LOGO));
            objCompanyDetailsModel.setCompanyName(objCompanyDetail.getString(CONST_CLOUD_CALL.COMPANY_NAME));
            objCompanyDetailsModel.setDomain(objCompanyDetail.getString(CONST_CLOUD_CALL.DOMAIN));
            objCompanyDetailsModel.setPrivacyURL(objCompanyDetail.getString(CONST_CLOUD_CALL.PRIVACY_URL));
            objCompanyDetailsModel.setResetPasswordUrl(objCompanyDetail.getString(CONST_CLOUD_CALL.RESET_PASSWORD_URL));
            objCompanyDetailsModel.setTechTableName(objCompanyDetail.getString(CONST_CLOUD_CALL.TECH_TABLE_NAME));
            objCompanyDetailsModel.setUserTableName(objCompanyDetail.getString(CONST_CLOUD_CALL.USER_TABLE_NAME));
            objCompanyDetailsModel.setUniqueId(objCompanyDetail.getString(CONST_CLOUD_CALL.UNIQUE_ID));
            objCompanyDetailsModel.setMessage(CONST_CLOUD_CALL.SUCCESS);
            if (objCompanyDetail.has(CONST_CLOUD_CALL.CONFIG_NOR)) {
                JSONArray arrConfigNor = objCompanyDetail.getJSONArray(CONST_CLOUD_CALL.CONFIG_NOR);
                ArrayList<String> sArrConfigNor = new ArrayList<String>();
                for (int iIncrConfigNor = 0; iIncrConfigNor < arrConfigNor.length(); iIncrConfigNor++) {
                    sArrConfigNor.add(arrConfigNor.get(iIncrConfigNor).toString());
                }
                objCompanyDetailsModel.setConfig_U0(sArrConfigNor);
            }
            if (objCompanyDetail.has(CONST_CLOUD_CALL.CONFIG_VIP)) {
                JSONArray arrConfigVIP = objCompanyDetail.getJSONArray(CONST_CLOUD_CALL.CONFIG_VIP);
                ArrayList<String> sArrConfigVIP = new ArrayList<String>();
                for (int iIncrConfigVIP = 0; iIncrConfigVIP < arrConfigVIP.length(); iIncrConfigVIP++) {
                    sArrConfigVIP.add(arrConfigVIP.get(iIncrConfigVIP).toString());
                }
                objCompanyDetailsModel.setConfig_U1(sArrConfigVIP);
            }
            if (objCompanyDetail.has(CONST_CLOUD_CALL.LANG_SUPPORT)) {
                JSONArray arrLangSupport = objCompanyDetail.getJSONArray(CONST_CLOUD_CALL.LANG_SUPPORT);
                ArrayList<String> sArrLangSupport = new ArrayList<String>();
                for (int iIncrLangSupport = 0; iIncrLangSupport < arrLangSupport.length(); iIncrLangSupport++) {
                    sArrLangSupport.add(arrLangSupport.get(iIncrLangSupport).toString());
                }
                objCompanyDetailsModel.setLangSupported(sArrLangSupport);
            }
            iSubAdminInterface.processCompanyDetailsFinish(objCompanyDetailsModel);

        } else if (res.has(ERROR_RESPONSE) && res.getJSONObject(ERROR_RESPONSE) != null) {
            objCompanyDetailsModel.setMessage(res.getJSONObject(ERROR_RESPONSE).getString(ERRORCODE));
            iSubAdminInterface.processCompanyDetailsFinish(objCompanyDetailsModel);
        } else {
            iSubAdminInterface.processCompanyDetailsFinish(null);
        }

    }

    /**
     * -----------------------------------------------------------------------------
     * Method Name: getAdminCompanyUserDetails
     * Created By : Suganya
     * Created Date: 17/08/2017
     * Modified By:
     * Modified Date:
     * Purpose    : method will used to get the admin company user details
     * -----------------------------------------------------------------------------
     */
    public static void getAdminCompanyUserDetails(final Activity cntx, JSONObject res) {
        final ISubAdminInterface iSubAdminInterface = (ISubAdminInterface) cntx;
        CompanyUserDetailsModel objCompanyUserDetailsModel = new CompanyUserDetailsModel();
        JSONObject objCompanyUserDetail = new JSONObject();
        JSONArray arrayCompanyUserDetail = new JSONArray();
        JSONObject objCompanySingleUserDetail = new JSONObject();
        JSONObject objUserField = new JSONObject();
        ArrayList<CompanyUserDetailsModel> alCompanyUserDetailsModel = new ArrayList<CompanyUserDetailsModel>();

        if (res.has(CONST_CLOUD_CALL.USER_DATA) && res.getJSONObject(CONST_CLOUD_CALL.USER_DATA) != null) {
            objCompanyUserDetail = res.getJSONObject(CONST_CLOUD_CALL.USER_DATA);
            arrayCompanyUserDetail = objCompanyUserDetail.getJSONArray(CONST_CLOUD_CALL.USER_LIST);
            for (int indexUserCount = 0; indexUserCount < arrayCompanyUserDetail.length(); indexUserCount++) {
                objCompanyUserDetailsModel = new CompanyUserDetailsModel();
                objCompanySingleUserDetail = arrayCompanyUserDetail.getJSONObject(indexUserCount);
                objUserField = objCompanySingleUserDetail.getJSONObject(CONST_CLOUD_CALL.FIELDS);
                objCompanyUserDetailsModel.setCompanyId(objUserField.getString(CONST_CLOUD_CALL.COMPANY_ID));
                objCompanyUserDetailsModel.setDomain(objUserField.getString(CONST_CLOUD_CALL.DOMAIN));
                objCompanyUserDetailsModel.setType(objUserField.getString(CONST_CLOUD_CALL.USER_TYPE));
                objCompanyUserDetailsModel.setUserEmail(objUserField.getString(CONST_CLOUD_CALL.USER_EMAILID));
                objCompanyUserDetailsModel.setUserName(objUserField.getString(CONST_CLOUD_CALL.USER_NAME));
                objCompanyUserDetailsModel.setGuid(objCompanySingleUserDetail.getString(CONST_CLOUD_CALL.GUID));
                objCompanyUserDetailsModel.setMessage(CONST_CLOUD_CALL.SUCCESS);
                if (objUserField.has(CONST_CLOUD_CALL.PRIMARY_PHONE_NUM) && objUserField.getString(CONST_CLOUD_CALL.PRIMARY_PHONE_NUM) != null) {
                    objCompanyUserDetailsModel.setPrimaryPhoneNum(objUserField.getString(CONST_CLOUD_CALL.PRIMARY_PHONE_NUM));
                } else {
                    objCompanyUserDetailsModel.setPrimaryPhoneNum("");
                }
                if (objUserField.has(CONST_CLOUD_CALL.SECONDARY_PHONE_NUM1) && objUserField.getString(CONST_CLOUD_CALL.SECONDARY_PHONE_NUM1) != null) {
                    objCompanyUserDetailsModel.setSecondaryPhoneNum1(objUserField.getString(CONST_CLOUD_CALL.SECONDARY_PHONE_NUM1));
                } else {
                    objCompanyUserDetailsModel.setSecondaryPhoneNum1("");
                }
                if (objUserField.has(CONST_CLOUD_CALL.SECONDARY_PHONE_NUM2) && objUserField.getString(CONST_CLOUD_CALL.SECONDARY_PHONE_NUM2) != null) {
                    objCompanyUserDetailsModel.setSecondaryPhoneNum2(objUserField.getString(CONST_CLOUD_CALL.SECONDARY_PHONE_NUM2));
                } else {
                    objCompanyUserDetailsModel.setSecondaryPhoneNum2("");
                }
                if (objUserField.has(CONST_CLOUD_CALL.SELECTED_LANG) && objUserField.getString(CONST_CLOUD_CALL.SELECTED_LANG) != null) {
                    objCompanyUserDetailsModel.setSelectedLang(objUserField.getString(CONST_CLOUD_CALL.SELECTED_LANG));
                } else {
                    objCompanyUserDetailsModel.setSelectedLang("");
                }
                if (objUserField.has(CONST_CLOUD_CALL.ASSIGNED_TECH) && objUserField.getString(CONST_CLOUD_CALL.ASSIGNED_TECH) != null) {
                    objCompanyUserDetailsModel.setAssignedTechnician(objUserField.getString(CONST_CLOUD_CALL.ASSIGNED_TECH));
                } else {
                    objCompanyUserDetailsModel.setAssignedTechnician("");
                }
                alCompanyUserDetailsModel.add(objCompanyUserDetailsModel);
            }

            iSubAdminInterface.processCompanyUserDetailsFinish(alCompanyUserDetailsModel);

        } else if (res.has(ERROR_RESPONSE) && res.getJSONObject(ERROR_RESPONSE) != null) {
            objCompanyUserDetailsModel.setMessage(res.getJSONObject(ERROR_RESPONSE).getString(ERRORCODE));
            alCompanyUserDetailsModel.add(objCompanyUserDetailsModel);
            iSubAdminInterface.processCompanyUserDetailsFinish(alCompanyUserDetailsModel);
        } else {
            iSubAdminInterface.processCompanyUserDetailsFinish(null);
        }

    }

    /**
     * -----------------------------------------------------------------------------
     * Method Name: getAdminCallITSupportDetails
     * Created By : Suganya
     * Created Date: 21/08/2017
     * Modified By:
     * Modified Date:
     * Purpose    : method will used to get the admin company call it support details
     * -----------------------------------------------------------------------------
     */
    public static void getAdminCallITSupportDetails(final Activity cntx, JSONObject res) {
        final ISubAdminInterface iSubAdminInterface = (ISubAdminInterface) cntx;
        final ArrayList<CallITSupportModel> alCallITSupportModel = new ArrayList<>();
        CallITSupportModel objCallITSupportModel;
        ArrayList<CallITPhoneNumberModel> alCallITPhoneNumberModel;
        CallITPhoneNumberModel objCallITPhoneNumberModel;
        JSONArray arrayCallITSupportDetails;
        JSONArray arrayCallITPhoneNumberModel;


        if (res.has(CONST_CLOUD_CALL.CALL_IT_SUPPORT_DETAILS) && res.getJSONObject(CONST_CLOUD_CALL.CALL_IT_SUPPORT_DETAILS) != null) {
            arrayCallITSupportDetails = res.getJSONArray(CONST_CLOUD_CALL.CALL_IT_SUPPORT_DETAILS);
            for (int indexCallITSupport = 0; indexCallITSupport < arrayCallITSupportDetails.length(); indexCallITSupport++) {
                objCallITSupportModel = new CallITSupportModel();
                objCallITSupportModel.setCompanyId(arrayCallITSupportDetails.getJSONObject(indexCallITSupport).getString(CONST_CLOUD_CALL.COMPANY_ID));
                objCallITSupportModel.setNumberOfQueues(arrayCallITSupportDetails.getJSONObject(indexCallITSupport).getString(CONST_CLOUD_CALL.NO_OF_QUEUES));
                objCallITSupportModel.setLang(arrayCallITSupportDetails.getJSONObject(indexCallITSupport).getString(CONST_CLOUD_CALL.LANG));
                objCallITSupportModel.setStartTimeInUTC(arrayCallITSupportDetails.getJSONObject(indexCallITSupport).getString(CONST_CLOUD_CALL.START_TIME_UTC));
                objCallITSupportModel.setEndTimeInUTC(arrayCallITSupportDetails.getJSONObject(indexCallITSupport).getString(CONST_CLOUD_CALL.END_TIME_UTC));
                objCallITSupportModel.setServiceDaysPerWeek(arrayCallITSupportDetails.getJSONObject(indexCallITSupport).getString(CONST_CLOUD_CALL.SERVICE_DAY_PER_WEEK));
                if (arrayCallITSupportDetails.getJSONObject(indexCallITSupport).has(CONST_CLOUD_CALL.CONFIG) && arrayCallITSupportDetails.getJSONObject(indexCallITSupport).getString(CONST_CLOUD_CALL.CONFIG) != null) {
                    objCallITSupportModel.setConfig(arrayCallITSupportDetails.getJSONObject(indexCallITSupport).getString(CONST_CLOUD_CALL.CONFIG));
                } else {
                    objCallITSupportModel.setConfig("");
                }
                objCallITSupportModel.setUniqueId(arrayCallITSupportDetails.getJSONObject(indexCallITSupport).getString(CONST_CLOUD_CALL.UNIQUE_ID));
                alCallITPhoneNumberModel = new ArrayList<>();
                if (arrayCallITSupportDetails.getJSONObject(indexCallITSupport).has(CONST_CLOUD_CALL.PHONE_NUMBERS) && arrayCallITSupportDetails.getJSONObject(indexCallITSupport).getJSONArray(CONST_CLOUD_CALL.PHONE_NUMBERS) != null) {
                    arrayCallITPhoneNumberModel = arrayCallITSupportDetails.getJSONObject(indexCallITSupport).getJSONArray(CONST_CLOUD_CALL.PHONE_NUMBERS);
                    for (int indexPhoneNumber = 0; indexPhoneNumber < arrayCallITPhoneNumberModel.length(); indexPhoneNumber++) {
                        objCallITPhoneNumberModel = new CallITPhoneNumberModel();
                        if (arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).has(CONST_CLOUD_CALL.NUMBER)) {
                            JSONArray arrNum = arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).getJSONArray(CONST_CLOUD_CALL.NUMBER);
                            ArrayList<String> sArrConfigNor = new ArrayList<String>();
                            for (int iIncrNum = 0; iIncrNum < arrNum.length(); iIncrNum++) {
                                sArrConfigNor.add(arrNum.get(iIncrNum).toString());
                            }
                            objCallITPhoneNumberModel.setNumber(sArrConfigNor);
                        }
                        objCallITPhoneNumberModel.setPrompt(arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).getString(CONST_CLOUD_CALL.PROMPT));
                        objCallITPhoneNumberModel.setQueueName(arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).getString(CONST_CLOUD_CALL.QUEUE_NAME));
                        objCallITPhoneNumberModel.setRequestId(arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).getString(CONST_CLOUD_CALL.REQUEST_ID));
                        if (arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).has(CONST_CLOUD_CALL.QUEUE_ID) && arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).getString(CONST_CLOUD_CALL.QUEUE_ID) != null) {
                            objCallITPhoneNumberModel.setQueueId(arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).getString(CONST_CLOUD_CALL.QUEUE_ID));
                        } else {
                            objCallITPhoneNumberModel.setQueueId("");
                        }
                        if (arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).has(CONST_CLOUD_CALL.PRIMARY_ADDRESS) && arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).getString(CONST_CLOUD_CALL.PRIMARY_ADDRESS) != null) {
                            objCallITPhoneNumberModel.setPrimaryAddress(arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).getString(CONST_CLOUD_CALL.PRIMARY_ADDRESS));
                        } else {
                            objCallITPhoneNumberModel.setPrimaryAddress("");
                        }
                        if (arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).has(CONST_CLOUD_CALL.SECONDARY_ADDRESS) && arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).getString(CONST_CLOUD_CALL.SECONDARY_ADDRESS) != null) {
                            objCallITPhoneNumberModel.setSecondaryAddress(arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).getString(CONST_CLOUD_CALL.SECONDARY_ADDRESS));
                        } else {
                            objCallITPhoneNumberModel.setSecondaryAddress("");
                        }
                        if (arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).has(CONST_CLOUD_CALL.QUESTION_ID) && arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).getString(CONST_CLOUD_CALL.QUESTION_ID) != null) {
                            objCallITPhoneNumberModel.setQuestionId(arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).getString(CONST_CLOUD_CALL.QUESTION_ID));
                        } else {
                            objCallITPhoneNumberModel.setQuestionId("");
                        }
                        if (arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).has(CONST_CLOUD_CALL.VDN_NUM) && arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).getString(CONST_CLOUD_CALL.VDN_NUM) != null) {
                            objCallITPhoneNumberModel.setVdnNum(arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).getString(CONST_CLOUD_CALL.VDN_NUM));
                        } else {
                            objCallITPhoneNumberModel.setVdnNum("");
                        }
                        if (arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).has(CONST_CLOUD_CALL.PHONE_TYPE) && arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).getString(CONST_CLOUD_CALL.PHONE_TYPE) != null) {
                            objCallITPhoneNumberModel.setPhoneType(arrayCallITPhoneNumberModel.getJSONObject(indexPhoneNumber).getString(CONST_CLOUD_CALL.PHONE_TYPE));
                        } else {
                            objCallITPhoneNumberModel.setPhoneType("");
                        }

                        alCallITPhoneNumberModel.add(objCallITPhoneNumberModel);
                    }

                }
                objCallITSupportModel.setPhoneNumbers(alCallITPhoneNumberModel);
                alCallITSupportModel.add(objCallITSupportModel);
            }

            iSubAdminInterface.processCallITSupportDetailsFinish(alCallITSupportModel);

        } else {
            iSubAdminInterface.processCallITSupportDetailsFinish(null);
        }

    }

    /**
     * -----------------------------------------------------------------------------
     * Method Name: getAdminChatITSupportDetails
     * Created By : Suganya
     * Created Date: 22/08/2017
     * Modified By:
     * Modified Date:
     * Purpose    : method will used to get the admin company chat it support details
     * -----------------------------------------------------------------------------
     */
    public static void getAdminChatITSupportDetails(final Activity cntx, JSONObject res) {
        final ISubAdminInterface iSubAdminInterface = (ISubAdminInterface) cntx;
        final ArrayList<ChatITSupportModel> alChatITSupportModel = new ArrayList<>();
        ChatITSupportModel objChatITSupportModel;
        ArrayList<ChatITOptionModel> alChatITOptionModel;
        ChatITOptionModel objChatITOptionModel;
        JSONArray arrayChatITSupportDetails;
        JSONArray arrayChatITOptionModel;


        if (res.has(CONST_CLOUD_CALL.CHAT_IT_SUPPORT_DETAILS) && res.getJSONObject(CONST_CLOUD_CALL.CHAT_IT_SUPPORT_DETAILS) != null) {
            arrayChatITSupportDetails = res.getJSONArray(CONST_CLOUD_CALL.CHAT_IT_SUPPORT_DETAILS);
            for (int indexChatITSupport = 0; indexChatITSupport < arrayChatITSupportDetails.length(); indexChatITSupport++) {
                objChatITSupportModel = new ChatITSupportModel();
                objChatITSupportModel.setCompanyId(arrayChatITSupportDetails.getJSONObject(indexChatITSupport).getString(CONST_CLOUD_CALL.COMPANY_ID));
                objChatITSupportModel.setNumberOfQueues(arrayChatITSupportDetails.getJSONObject(indexChatITSupport).getString(CONST_CLOUD_CALL.NO_OF_QUEUES));
                objChatITSupportModel.setLang(arrayChatITSupportDetails.getJSONObject(indexChatITSupport).getString(CONST_CLOUD_CALL.LANG));
                objChatITSupportModel.setStartTimeInUTC(arrayChatITSupportDetails.getJSONObject(indexChatITSupport).getString(CONST_CLOUD_CALL.START_TIME_UTC));
                objChatITSupportModel.setEndTimeInUTC(arrayChatITSupportDetails.getJSONObject(indexChatITSupport).getString(CONST_CLOUD_CALL.END_TIME_UTC));
                objChatITSupportModel.setServiceDaysPerWeek(arrayChatITSupportDetails.getJSONObject(indexChatITSupport).getString(CONST_CLOUD_CALL.SERVICE_DAY_PER_WEEK));
                if (arrayChatITSupportDetails.getJSONObject(indexChatITSupport).has(CONST_CLOUD_CALL.CHAT_CONFIG) && arrayChatITSupportDetails.getJSONObject(indexChatITSupport).getString(CONST_CLOUD_CALL.CHAT_CONFIG) != null) {
                    objChatITSupportModel.setChatConfig(arrayChatITSupportDetails.getJSONObject(indexChatITSupport).getString(CONST_CLOUD_CALL.CHAT_CONFIG));
                } else {
                    objChatITSupportModel.setChatConfig("");
                }
                if (arrayChatITSupportDetails.getJSONObject(indexChatITSupport).has(CONST_CLOUD_CALL.CHAT_TYPE) && arrayChatITSupportDetails.getJSONObject(indexChatITSupport).getString(CONST_CLOUD_CALL.CHAT_TYPE) != null) {
                    objChatITSupportModel.setChatType(arrayChatITSupportDetails.getJSONObject(indexChatITSupport).getString(CONST_CLOUD_CALL.CHAT_TYPE));
                } else {
                    objChatITSupportModel.setChatType("");
                }
                if (arrayChatITSupportDetails.getJSONObject(indexChatITSupport).has(CONST_CLOUD_CALL.AIC) && arrayChatITSupportDetails.getJSONObject(indexChatITSupport).getString(CONST_CLOUD_CALL.AIC) != null) {
                    objChatITSupportModel.setAIC(arrayChatITSupportDetails.getJSONObject(indexChatITSupport).getString(CONST_CLOUD_CALL.AIC));
                } else {
                    objChatITSupportModel.setAIC("");
                }
                if (arrayChatITSupportDetails.getJSONObject(indexChatITSupport).has(CONST_CLOUD_CALL.CHAT_BASE_URL) && arrayChatITSupportDetails.getJSONObject(indexChatITSupport).getString(CONST_CLOUD_CALL.CHAT_BASE_URL) != null) {
                    objChatITSupportModel.setChatBaseUrl(arrayChatITSupportDetails.getJSONObject(indexChatITSupport).getString(CONST_CLOUD_CALL.CHAT_BASE_URL));
                } else {
                    objChatITSupportModel.setChatBaseUrl("");
                }
                objChatITSupportModel.setUniqueId(arrayChatITSupportDetails.getJSONObject(indexChatITSupport).getString(CONST_CLOUD_CALL.UNIQUE_ID));
                alChatITOptionModel = new ArrayList<>();
                if (arrayChatITSupportDetails.getJSONObject(indexChatITSupport).has(CONST_CLOUD_CALL.PHONE_NUMBERS) && arrayChatITSupportDetails.getJSONObject(indexChatITSupport).getJSONArray(CONST_CLOUD_CALL.PHONE_NUMBERS) != null) {
                    arrayChatITOptionModel = arrayChatITSupportDetails.getJSONObject(indexChatITSupport).getJSONArray(CONST_CLOUD_CALL.PHONE_NUMBERS);
                    for (int indexChatOption = 0; indexChatOption < arrayChatITOptionModel.length(); indexChatOption++) {
                        objChatITOptionModel = new ChatITOptionModel();

                        objChatITOptionModel.setEscQuestion(arrayChatITOptionModel.getJSONObject(indexChatOption).getString(CONST_CLOUD_CALL.PROMPT));

                        objChatITOptionModel.setRequestId(arrayChatITOptionModel.getJSONObject(indexChatOption).getString(CONST_CLOUD_CALL.REQUEST_ID));
                        if (arrayChatITOptionModel.getJSONObject(indexChatOption).has(CONST_CLOUD_CALL.CHAT_CATEGORY) && arrayChatITOptionModel.getJSONObject(indexChatOption).getString(CONST_CLOUD_CALL.CHAT_CATEGORY) != null) {
                            objChatITOptionModel.setChatCategory(arrayChatITOptionModel.getJSONObject(indexChatOption).getString(CONST_CLOUD_CALL.CHAT_CATEGORY));
                        } else {
                            objChatITOptionModel.setChatCategory("");
                        }

                        alChatITOptionModel.add(objChatITOptionModel);
                    }

                }
                objChatITSupportModel.setChatOptions(alChatITOptionModel);
                alChatITSupportModel.add(objChatITSupportModel);
            }

            iSubAdminInterface.processChatITSupportDetailsFinish(alChatITSupportModel);

        } else {
            iSubAdminInterface.processChatITSupportDetailsFinish(null);
        }

    }

    /**
     * -----------------------------------------------------------------------------
     * Method Name: getAdminSupportSpotDetails
     * Created By : Suganya
     * Created Date: 29/08/2017
     * Modified By:
     * Modified Date:
     * Purpose  : method will used to get the admin company support spot details
     * -----------------------------------------------------------------------------
     */
    public static void getAdminSupportSpotDetails(final Activity cntx, JSONObject res) {
        final ISubAdminInterface iSubAdminInterface = (ISubAdminInterface) cntx;
        final ArrayList<SpotDetailModel> alObjSportDetailModel = new ArrayList<SpotDetailModel>();
        SpotDetailModel objSportDetailModel;


        if (res.has(CONST_CLOUD_CALL.SPOTDETAILS) && res.getJSONObject(CONST_CLOUD_CALL.SPOTDETAILS) != null) {
            JSONArray alSportyDetail = res.getJSONArray(CONST_CLOUD_CALL.SPOTDETAILS);

            for (int iIncSportDt = 0; iIncSportDt < alSportyDetail.length(); iIncSportDt++) {
                objSportDetailModel = new SpotDetailModel();
                JSONObject objDetail = alSportyDetail.getJSONObject(iIncSportDt);
                if (objDetail.has(CONST_CLOUD_CALL.ADDRESSFIELD1) && objDetail.getString(CONST_CLOUD_CALL.ADDRESSFIELD1) != null) {
                    objSportDetailModel.setAddressField1(objDetail.getString(CONST_CLOUD_CALL.ADDRESSFIELD1));
                } else {
                    objSportDetailModel.setAddressField1("");
                }
                if (objDetail.has(CONST_CLOUD_CALL.ADDRESSFIELD2) && objDetail.getString(CONST_CLOUD_CALL.ADDRESSFIELD2) != null) {
                    objSportDetailModel.setAddressField2(objDetail.getString(CONST_CLOUD_CALL.ADDRESSFIELD2));
                } else {
                    objSportDetailModel.setAddressField2("");
                }
                if (objDetail.has(CONST_CLOUD_CALL.ZIP_PIN) && objDetail.getString(CONST_CLOUD_CALL.ZIP_PIN) != null) {
                    objSportDetailModel.setZipCode(objDetail.getString(CONST_CLOUD_CALL.ZIP_PIN));
                } else {
                    objSportDetailModel.setZipCode("");
                }
                if (objDetail.has(CONST_CLOUD_CALL.STATE) && objDetail.getString(CONST_CLOUD_CALL.STATE) != null) {
                    objSportDetailModel.setState(objDetail.getString(CONST_CLOUD_CALL.STATE));
                } else {
                    objSportDetailModel.setState("");
                }
                if (objDetail.has(CONST_CLOUD_CALL.SERVICEDETAILS) && objDetail.getString(CONST_CLOUD_CALL.SERVICEDETAILS) != null) {
                    objSportDetailModel.setServiceDetails(objDetail.getString(CONST_CLOUD_CALL.SERVICEDETAILS));
                } else {
                    objSportDetailModel.setServiceDetails("");
                }
                if (objDetail.has(CONST_CLOUD_CALL.SERVICEHOUR) && objDetail.getString(CONST_CLOUD_CALL.SERVICEHOUR) != null) {
                    objSportDetailModel.setServiceHour(objDetail.getString(CONST_CLOUD_CALL.SERVICEHOUR));
                } else {
                    objSportDetailModel.setServiceHour("");
                }
                if (objDetail.has(CONST_CLOUD_CALL.ISMERIDIANEXIST) && objDetail.getString(CONST_CLOUD_CALL.ISMERIDIANEXIST) != null) {
                    objSportDetailModel.setIsMeridianExist(objDetail.getString(CONST_CLOUD_CALL.ISMERIDIANEXIST));
                } else {
                    objSportDetailModel.setIsMeridianExist("");
                }
                if (objDetail.has(CONST_CLOUD_CALL.LATITUDE) && objDetail.getString(CONST_CLOUD_CALL.LATITUDE) != null) {
                    objSportDetailModel.setLatitude(objDetail.getString(CONST_CLOUD_CALL.LATITUDE));
                } else {
                    objSportDetailModel.setLatitude("");
                }
                if (objDetail.has(CONST_CLOUD_CALL.LONGITUDE) && objDetail.getString(CONST_CLOUD_CALL.LONGITUDE) != null) {
                    objSportDetailModel.setLongitude(objDetail.getString(CONST_CLOUD_CALL.LONGITUDE));
                } else {
                    objSportDetailModel.setLongitude("");
                }
                objSportDetailModel.setSupportSpotType(objDetail.getString(CONST_CLOUD_CALL.SUPPORTSPOTTYPE));
                objSportDetailModel.setCenterTitle(objDetail.getString(CONST_CLOUD_CALL.CENTERTITLE));
                objSportDetailModel.setCompanyId(objDetail.getString(CONST_CLOUD_CALL.COMPANY_ID));
                objSportDetailModel.setCountryCode(objDetail.getString(CONST_CLOUD_CALL.COUNTRYCODE));
                objSportDetailModel.setMessage(CONST_CLOUD_CALL.SUCCESS);

                alObjSportDetailModel.add(objSportDetailModel);
            }

            iSubAdminInterface.processSupportSpotDetailsFinish(alObjSportDetailModel);

        } else {
            iSubAdminInterface.processSupportSpotDetailsFinish(null);
        }

    }

    /**
     * -----------------------------------------------------------------------------
     * Method Name: getAdminMaintenanceMessageMessageDetails
     * Created By : Suganya
     * Created Date: 29/08/2017
     * Modified By:
     * Modified Date:
     * Purpose  : method will used to get the admin company maintenance message details
     * -----------------------------------------------------------------------------
     */
    public static void getAdminMaintenanceMessageMessageDetails(final Activity cntx, JSONObject res) {
        final ISubAdminInterface iSubAdminInterface = (ISubAdminInterface) cntx;
        final ArrayList<MaintenanceMessageModel> alMaintenanceMessageModel = new ArrayList<MaintenanceMessageModel>();
        MaintenanceMessageModel objMaintenanceMessageModel;


        if (res.has(CONST_CLOUD_CALL.MAINTENANCE_MESSAGE) && res.getJSONObject(CONST_CLOUD_CALL.MAINTENANCE_MESSAGE) != null) {
            JSONArray alMaintenanceMessageDetail = res.getJSONArray(CONST_CLOUD_CALL.MAINTENANCE_MESSAGE);

            for (int iIncMM = 0; iIncMM < alMaintenanceMessageDetail.length(); iIncMM++) {
                objMaintenanceMessageModel = new MaintenanceMessageModel();
                JSONObject objDetail = alMaintenanceMessageDetail.getJSONObject(iIncMM);
                objMaintenanceMessageModel.setCompanyId(objDetail.getString(CONST_CLOUD_CALL.COMPANY_ID));
                objMaintenanceMessageModel.setTitle(objDetail.getString(CONST_CLOUD_CALL.TITLE));
                objMaintenanceMessageModel.setDescription(objDetail.getString(CONST_CLOUD_CALL.DESCRIPTION));
                objMaintenanceMessageModel.setLang(objDetail.getString(CONST_CLOUD_CALL.LANG));
                objMaintenanceMessageModel.setLocation(objDetail.getString(CONST_CLOUD_CALL.LOCATION));
                objMaintenanceMessageModel.setMessageId(objDetail.getString(CONST_CLOUD_CALL.MESSAGEID));
                objMaintenanceMessageModel.setOutageType(objDetail.getString(CONST_CLOUD_CALL.OUTAGETYPE));
                objMaintenanceMessageModel.setRegion(objDetail.getString(CONST_CLOUD_CALL.REGION));
                objMaintenanceMessageModel.setStatus(objDetail.getString(CONST_CLOUD_CALL.STAUTS));
                objMaintenanceMessageModel.setCreatedDate(objDetail.getString((CONST_CLOUD_CALL.CREATED_DATE)));
                objMaintenanceMessageModel.setUniqueId(objDetail.getString((CONST_CLOUD_CALL.UNIQUE_ID)));
                objMaintenanceMessageModel.setExpiryType(objDetail.getString((CONST_CLOUD_CALL.EXPIRE_TYPE)));
                if (objDetail.has(CONST_CLOUD_CALL.EXPIRE_TIME) && objDetail.getString(CONST_CLOUD_CALL.EXPIRE_TIME) != null) {
                    objMaintenanceMessageModel.setExpireTime(objDetail.getString(CONST_CLOUD_CALL.EXPIRE_TIME));
                } else {
                    objMaintenanceMessageModel.setExpireTime("");
                }
                alMaintenanceMessageModel.add(objMaintenanceMessageModel);
            }

            iSubAdminInterface.processMaintenanceMessageFinish(alMaintenanceMessageModel);

        } else {
            iSubAdminInterface.processMaintenanceMessageFinish(null);
        }

    }
/*
    public static void getAdminUserData(final Activity cntx, final JSONObject res) {
        final ISubAdminInterface iSubAdminInterface = (ISubAdminInterface) cntx;
        final ArrayList<UserDataModel> alUserDataModel = new ArrayList<UserDataModel>();
        UserDataModel objUserDataModel = new UserDataModel();
        GsonBuilder gsonb = new GsonBuilder();
       Gson gson  = new Gson();
               //gsonb.create();
        ArrayList<UserDataModel> alUserDataList =  new ArrayList<>();
        UserDataModel userDataModel;
        UserData userData;
        Fields userFields ;
        ArrayList<String> arrField = new ArrayList<>();
        ArrayList<List> objarrList = null;
        CustomList listUser = new CustomList();
        List<CustomList> listModel = new ArrayList<>();

        AppLog.showLogE(TAG,"res =" +res.toString());
        userData = new UserData();
        Type listType = new TypeToken<UserDataModel>(){}.getType();
        userDataModel =  gson.fromJson(res.toString(), listType);
        AppLog.showLogE(TAG,"userDataModel ="+userDataModel);
       // userDataModel = new UserDataModel();
        userDataModel.setUserData(userDataModel.getUserData());
        *//*
        userData.setList(listModel);
        userDataModel.setUserData(userData);

        alUserDataList.add(objUserDataModel);*//*

        iSubAdminInterface.processUserDataFinish(userDataModel);

    }*/




    /**
     * -----------------------------------------------------------------------------
     * Method Name: getAdminSelfSupportDetails
     * Created By : Suganya
     * Created Date: 14/09/2017
     * Modified By:
     * Modified Date:
     * Purpose  : method will used to get the admin company self support details
     * -----------------------------------------------------------------------------
     */
    public static void getAdminSelfSupportDetails(final Activity cntx, JSONObject res) {
        final ISubAdminInterface iSubAdminInterface = (ISubAdminInterface) cntx;
        final ArrayList<SelfSupportModel> alSelfSupportModel = new ArrayList<SelfSupportModel>();
        ArrayList<SelfSupportOptionModel> alSelfSupportOptionModel;
        SelfSupportModel objSelfSupportModel;
        SelfSupportOptionModel objSelfSupportOptionModel;

        if (res.has(CONST_CLOUD_CALL.SELF_SUPPORT_DETAILS) && res.getJSONObject(CONST_CLOUD_CALL.SELF_SUPPORT_DETAILS) != null) {
            JSONArray alSelfSupportDetail = res.getJSONArray(CONST_CLOUD_CALL.SELF_SUPPORT_DETAILS);

            for (int iIncSS = 0; iIncSS < alSelfSupportDetail.length(); iIncSS++) {
                objSelfSupportModel = new SelfSupportModel();
                JSONObject objDetail = alSelfSupportDetail.getJSONObject(iIncSS);
                objSelfSupportModel.setCompanyId(objDetail.getString(CONST_CLOUD_CALL.COMPANY_ID));
                objSelfSupportModel.setLang(objDetail.getString(CONST_CLOUD_CALL.LANG));
                objSelfSupportModel.setNumberOfQueues(objDetail.getString(CONST_CLOUD_CALL.NO_OF_QUEUES));
                objSelfSupportModel.setCompanyDescription(objDetail.getString(CONST_CLOUD_CALL.COMPANY_DESCRIPTION));
                objSelfSupportModel.setUniqueId(objDetail.getString((CONST_CLOUD_CALL.UNIQUE_ID)));
                JSONArray alSelfSupportOptionsDetail = objDetail.getJSONArray(CONST_CLOUD_CALL.SELF_SUP_OPTIONS);
                alSelfSupportOptionModel = new ArrayList<SelfSupportOptionModel>();
                for (int iIncSSOption = 0; iIncSSOption < alSelfSupportOptionsDetail.length(); iIncSSOption++) {
                    objSelfSupportOptionModel = new SelfSupportOptionModel();
                    JSONObject objOptionDetail = alSelfSupportOptionsDetail.getJSONObject(iIncSSOption);
                    objSelfSupportOptionModel.setOptionName(objOptionDetail.getString(CONST_CLOUD_CALL.OPTION_NAME));
                    objSelfSupportOptionModel.setRequestId(objOptionDetail.getString(CONST_CLOUD_CALL.REQUEST_ID));
                    if (objOptionDetail.has(CONST_CLOUD_CALL.SERVICE_TYPE) && objOptionDetail.getString(CONST_CLOUD_CALL.SERVICE_TYPE) != null) {
                        objSelfSupportOptionModel.setServiceType(objOptionDetail.getString(CONST_CLOUD_CALL.SERVICE_TYPE));
                    } else {
                        objSelfSupportOptionModel.setServiceType("");
                    }
                    if (objOptionDetail.has(CONST_CLOUD_CALL.SERVICE_URL) && objOptionDetail.getString(CONST_CLOUD_CALL.SERVICE_URL) != null) {
                        objSelfSupportOptionModel.setServiceURL(objOptionDetail.getString(CONST_CLOUD_CALL.SERVICE_URL));
                    } else {
                        objSelfSupportOptionModel.setServiceURL("");
                    }
                    if (objOptionDetail.has(CONST_CLOUD_CALL.USER_NAME) && objOptionDetail.getString(CONST_CLOUD_CALL.USER_NAME) != null) {
                        objSelfSupportOptionModel.setUserName(objOptionDetail.getString(CONST_CLOUD_CALL.USER_NAME));
                    } else {
                        objSelfSupportOptionModel.setUserName("");
                    }
                    if (objOptionDetail.has(CONST_CLOUD_CALL.PASSWORD) && objOptionDetail.getString(CONST_CLOUD_CALL.PASSWORD) != null) {
                        objSelfSupportOptionModel.setPassword(objOptionDetail.getString(CONST_CLOUD_CALL.PASSWORD));
                    } else {
                        objSelfSupportOptionModel.setPassword("");
                    }
                    if (objOptionDetail.has(CONST_CLOUD_CALL.PATH) && objOptionDetail.getString(CONST_CLOUD_CALL.PATH) != null) {
                        objSelfSupportOptionModel.setPath(objOptionDetail.getString(CONST_CLOUD_CALL.PATH));
                    } else {
                        objSelfSupportOptionModel.setPath("");
                    }
                    if (objOptionDetail.has(CONST_CLOUD_CALL.COOKIE) && objOptionDetail.getString(CONST_CLOUD_CALL.COOKIE) != null) {
                        objSelfSupportOptionModel.setCookie(objOptionDetail.getString(CONST_CLOUD_CALL.COOKIE));
                    } else {
                        objSelfSupportOptionModel.setCookie("");
                    }
                    if (objOptionDetail.has(CONST_CLOUD_CALL.TENANT_ID) && objOptionDetail.getString(CONST_CLOUD_CALL.TENANT_ID) != null) {
                        objSelfSupportOptionModel.setTenantId(objOptionDetail.getString(CONST_CLOUD_CALL.TENANT_ID));
                    } else {
                        objSelfSupportOptionModel.setTenantId("");
                    }
                    alSelfSupportOptionModel.add(objSelfSupportOptionModel);
                }
                objSelfSupportModel.setSelfSupportOptions(alSelfSupportOptionModel);
                alSelfSupportModel.add(objSelfSupportModel);
            }

            iSubAdminInterface.processSelfSupportFinish(alSelfSupportModel);

        } else {
            iSubAdminInterface.processSelfSupportFinish(null);
        }

    }
    /**
     * -----------------------------------------------------------------------------
     * Method Name: getAllCompanyDetails
     * Created By : Suganya
     * Created Date: 09/10/2017
     * Modified By:
     * Modified Date:
     * Purpose    : method will used to get the all company details
     * -----------------------------------------------------------------------------
     */
    public static void getAllCompanyDetails(final Activity cntx, JSONObject res) {
        final IMainAdminHomeInterface iMainAdminHomeInterface = (IMainAdminHomeInterface) cntx;
        CompanyDetailsModel objCompanyDetailsModel = new CompanyDetailsModel();
        ArrayList<CompanyDetailsModel> alCompanyDetailsModel = new ArrayList<>();

        if (res.has(CONST_CLOUD_CALL.COMPANIES) && res.getJSONObject(CONST_CLOUD_CALL.COMPANIES) != null) {
            JSONArray alAllCompanyDatails = res.getJSONArray(CONST_CLOUD_CALL.COMPANIES);

            for (int iIncAllCompanyDetails = 0; iIncAllCompanyDetails < alAllCompanyDatails.length(); iIncAllCompanyDetails++) {
                objCompanyDetailsModel = new CompanyDetailsModel();
                JSONObject objCompanyDetail = alAllCompanyDatails.getJSONObject(iIncAllCompanyDetails);
                objCompanyDetailsModel.setCompanyId(objCompanyDetail.getString(CONST_CLOUD_CALL.COMPANY_ID));
                if (objCompanyDetail.has(CONST_CLOUD_CALL.COMPANY_LOGO) && objCompanyDetail.getString(CONST_CLOUD_CALL.COMPANY_LOGO) != null) {
                    objCompanyDetailsModel.setCompanyLogo(objCompanyDetail.getString(CONST_CLOUD_CALL.COMPANY_LOGO));
                }else{
                    objCompanyDetailsModel.setCompanyLogo("");
                }
                objCompanyDetailsModel.setCompanyName(objCompanyDetail.getString(CONST_CLOUD_CALL.COMPANY_NAME));
                objCompanyDetailsModel.setDomain(objCompanyDetail.getString(CONST_CLOUD_CALL.DOMAIN));
                if (objCompanyDetail.has(CONST_CLOUD_CALL.PRIVACY_URL) && objCompanyDetail.getString(CONST_CLOUD_CALL.PRIVACY_URL) != null) {
                    objCompanyDetailsModel.setPrivacyURL(objCompanyDetail.getString(CONST_CLOUD_CALL.PRIVACY_URL));
                }else{
                    objCompanyDetailsModel.setPrivacyURL("");
                }
                if (objCompanyDetail.has(CONST_CLOUD_CALL.RESET_PASSWORD_URL) && objCompanyDetail.getString(CONST_CLOUD_CALL.RESET_PASSWORD_URL) != null) {
                    objCompanyDetailsModel.setResetPasswordUrl(objCompanyDetail.getString(CONST_CLOUD_CALL.RESET_PASSWORD_URL));
                } else {
                    objCompanyDetailsModel.setResetPasswordUrl("");
                }
                objCompanyDetailsModel.setTechTableName(objCompanyDetail.getString(CONST_CLOUD_CALL.TECH_TABLE_NAME));
                objCompanyDetailsModel.setUserTableName(objCompanyDetail.getString(CONST_CLOUD_CALL.USER_TABLE_NAME));
                objCompanyDetailsModel.setUniqueId(objCompanyDetail.getString(CONST_CLOUD_CALL.UNIQUE_ID));
                objCompanyDetailsModel.setMessage(CONST_CLOUD_CALL.SUCCESS);
                if (objCompanyDetail.has(CONST_CLOUD_CALL.CONFIG_NOR)) {
                    JSONArray arrConfigNor = objCompanyDetail.getJSONArray(CONST_CLOUD_CALL.CONFIG_NOR);
                    ArrayList<String> sArrConfigNor = new ArrayList<String>();
                    for (int iIncrConfigNor = 0; iIncrConfigNor < arrConfigNor.length(); iIncrConfigNor++) {
                        sArrConfigNor.add(arrConfigNor.get(iIncrConfigNor).toString());
                    }
                    objCompanyDetailsModel.setConfig_U0(sArrConfigNor);
                }
                if (objCompanyDetail.has(CONST_CLOUD_CALL.CONFIG_VIP)) {
                    JSONArray arrConfigVIP = objCompanyDetail.getJSONArray(CONST_CLOUD_CALL.CONFIG_VIP);
                    ArrayList<String> sArrConfigVIP = new ArrayList<String>();
                    for (int iIncrConfigVIP = 0; iIncrConfigVIP < arrConfigVIP.length(); iIncrConfigVIP++) {
                        sArrConfigVIP.add(arrConfigVIP.get(iIncrConfigVIP).toString());
                    }
                    objCompanyDetailsModel.setConfig_U1(sArrConfigVIP);
                }
                if (objCompanyDetail.has(CONST_CLOUD_CALL.LANG_SUPPORT)) {
                    JSONArray arrLangSupport = objCompanyDetail.getJSONArray(CONST_CLOUD_CALL.LANG_SUPPORT);
                    ArrayList<String> sArrLangSupport = new ArrayList<String>();
                    for (int iIncrLangSupport = 0; iIncrLangSupport < arrLangSupport.length(); iIncrLangSupport++) {
                        sArrLangSupport.add(arrLangSupport.get(iIncrLangSupport).toString());
                    }
                    objCompanyDetailsModel.setLangSupported(sArrLangSupport);
                }
                alCompanyDetailsModel.add(objCompanyDetailsModel);
            }

            iMainAdminHomeInterface.processAllCompanyDetailsFinish(alCompanyDetailsModel);

        } else if (res.has(ERROR_RESPONSE) && res.getJSONObject(ERROR_RESPONSE) != null) {
            objCompanyDetailsModel.setMessage(res.getJSONObject(ERROR_RESPONSE).getString(ERRORCODE));
            alCompanyDetailsModel.add(objCompanyDetailsModel);
            iMainAdminHomeInterface.processAllCompanyDetailsFinish(alCompanyDetailsModel);
        } else {
            iMainAdminHomeInterface.processAllCompanyDetailsFinish(null);
        }


    }
}
