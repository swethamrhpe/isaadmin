package com.support.admin.instantsupport.Login;

/**-----------------------------------------------------------------------------
 Class Name: ILoginInterface
 Created By: Suganya
 Created Date: 31-07-2017
 Modified By:
 Modified Date:
 Purpose: ILoginInterface for storing Login datas
 -----------------------------------------------------------------------------*/
public interface ILoginInterface {
    void processFinish(LoginScreenModel objLoginScreenModel);
    void processUserDetailsFinish(UserDetailsModel objUserDetailsModel);
}
