package com.support.admin.instantsupport.UserScreen;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.support.admin.instantsupport.HomeScreen.CompanyUserDetailsModel;
import com.support.admin.instantsupport.R;
import com.support.admin.instantsupport.Utility.AppLog;
import com.support.admin.instantsupport.Utility.ScreenNavigator;

import java.util.ArrayList;

/**
 * -----------------------------------------------------------------------------
 * Class Name: UserAdapter
 * Created By: Swetha M.R
 * Created Date: 1-09-2018
 * Modified By:
 * Modified Date:
 * Purpose: UserAdapter class for list the data
 * -----------------------------------------------------------------------------
 */

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.USerDataHolder> implements AdapterView.OnItemClickListener {
    private final String TAG = "UserAdapter";
    private ArrayList<CompanyUserDetailsModel> alUserDataModel;
    private Context context;
    String selectType;
    public UserAdapter(Activity context, ArrayList<CompanyUserDetailsModel> companyUserDetailsModel,String selectType){
        this.context = context;
        this.alUserDataModel = companyUserDetailsModel;
        this.selectType = selectType;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    }


    public class USerDataHolder extends RecyclerView.ViewHolder {
        public TextView tvMMTitle;
        public LinearLayout llMMLayout;

        public USerDataHolder(View v) {
            super(v);
            tvMMTitle = (TextView) v.findViewById(R.id.txtudTitle);
            llMMLayout = (LinearLayout) v.findViewById(R.id.lludlayout);
        }
    }

    @Override
    public USerDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.ud_list, parent, false);
        USerDataHolder maintenanceMessageHolder = new USerDataHolder(v);
        return maintenanceMessageHolder;
    }

    @Override
    public void onBindViewHolder(USerDataHolder uSerDataHolder, final int position) {
        String userName =alUserDataModel.get(position).getUserName();
        uSerDataHolder.tvMMTitle.setText(userName);
        uSerDataHolder.llMMLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               ScreenNavigator.callAddUserActivity((Activity) context,alUserDataModel.get(position),selectType);
               ((Activity) context).finish();
            }
        });
        uSerDataHolder.llMMLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return true;// returning true instead of false, works for me
            }
        });
    }
    @Override
    public int getItemCount() {
        if(alUserDataModel!=null)

            return alUserDataModel.size();
        else
            return  0;
    }
}
