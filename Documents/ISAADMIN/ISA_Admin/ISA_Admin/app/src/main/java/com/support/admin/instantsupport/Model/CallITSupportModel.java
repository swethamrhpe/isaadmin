package com.support.admin.instantsupport.Model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * -----------------------------------------------------------------------------
 * Class Name: MaintenanceMessageModel
 * Created By: Suganya
 * Created Date: 29-08-2016
 * Modified By:
 * Modified Date:
 * Purpose: This will provide  all Maintenance Message details
 * -----------------------------------------------------------------------------
 */

public class CallITSupportModel implements Parcelable {

    private String region;

    private String Description;

    private String title;

    private String location;

    private String status;

    private String companyId;

    private String messageId;

    private String outageType;

    private String createdDate;

    private String lang;

    private String uniqueId;

    private String expireTime;

    private String expiryType;

    public CallITSupportModel() {
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getOutageType() {
        return outageType;
    }

    public void setOutageType(String outageType) {
        this.outageType = outageType;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getExpireTime() {
        return expireTime;
    }

    public void setExpiryType(String expiryType) {
        this.expiryType = expiryType;
    }

    public String getExpiryType() {
        return expiryType;
    }

    public void setExpireTime(String expireTime) {
        this.expireTime = expireTime;
    }


    @Override
    public String toString() {
        return "ClassPojo [region = " + region + ", Description = " + Description + ", title = " + title + ", location = " + location + ", status = " + status + ", companyId = " + companyId + ", messageId = " + messageId + ", outageType = " + outageType + ", createdDate = " + createdDate + ", lang = " + lang + ", uniqueId = " + uniqueId + ", expireTime = " + expireTime + ", expiryType = " + expiryType + "]";
    }

    protected CallITSupportModel(Parcel in) {
        region = in.readString();
        Description = in.readString();
        title = in.readString();
        location = in.readString();
        status = in.readString();
        companyId = in.readString();
        messageId = in.readString();
        outageType = in.readString();
        createdDate = in.readString();
        lang = in.readString();
        uniqueId = in.readString();
        expireTime = in.readString();
        expiryType= in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(region);
        parcel.writeString(Description);
        parcel.writeString(title);
        parcel.writeString(location);
        parcel.writeString(status);
        parcel.writeString(companyId);
        parcel.writeString(messageId);
        parcel.writeString(outageType);
        parcel.writeString(createdDate);
        parcel.writeString(lang);
        parcel.writeString(uniqueId);
        parcel.writeString(expireTime);
        parcel.writeString(expiryType);
    }

    public static final Creator<CallITSupportModel> CREATOR = new Creator<CallITSupportModel>() {
        @Override
        public CallITSupportModel createFromParcel(Parcel in) {
            return new CallITSupportModel(in);
        }

        @Override
        public CallITSupportModel[] newArray(int size) {
            return new CallITSupportModel[size];
        }
    };
}
