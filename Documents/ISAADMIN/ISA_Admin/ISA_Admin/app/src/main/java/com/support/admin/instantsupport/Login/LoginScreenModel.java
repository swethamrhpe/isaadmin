package com.support.admin.instantsupport.Login;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * -----------------------------------------------------------------------------
 * Class Name: LoginScreenModel
 * Created By: Suganya
 * Created Date: 31-07-2017
 * Modified By:
 * Modified Date:
 * Purpose:  This will provide stores the login  user details
 * -----------------------------------------------------------------------------
 */
public class LoginScreenModel implements Parcelable
{
    public String message;
    private String sessionToken;
    private String status;
    private String userId;

    public LoginScreenModel() {
    }
    public LoginScreenModel(Parcel in ) {
        readFromParcel( in );
    }

    public static final Creator<LoginScreenModel> CREATOR = new Creator<LoginScreenModel>() {
        @Override
        public LoginScreenModel createFromParcel(Parcel in) {
            return new LoginScreenModel(in);
        }

        @Override
        public LoginScreenModel[] newArray(int size) {
            return new LoginScreenModel[size];
        }
    };

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getSessionToken ()
    {
        return sessionToken;
    }

    public void setSessionToken (String sessionToken)
    {
        this.sessionToken = sessionToken;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getUserId ()
    {
        return userId;
    }

    public void setUserId (String userId)
    {
        this.userId = userId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", sessionToken = "+sessionToken+", status = "+status+", userId = "+userId+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeString(message);
        dest.writeString(sessionToken);
        dest.writeString(status);
        dest.writeString(userId);
    }

    private void readFromParcel(Parcel in ) {

        message = in .readString();
        sessionToken  = in .readString();
        status   = in .readString();
        userId       = in .readString();
    }

}
