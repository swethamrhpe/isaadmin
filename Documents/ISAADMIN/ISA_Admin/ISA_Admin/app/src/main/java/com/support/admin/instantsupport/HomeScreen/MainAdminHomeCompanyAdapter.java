package com.support.admin.instantsupport.HomeScreen;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.support.admin.instantsupport.Application.InstApplication;
import com.support.admin.instantsupport.Login.UserDetailsModel;
import com.support.admin.instantsupport.R;
import com.support.admin.instantsupport.Utility.ScreenNavigator;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * -----------------------------------------------------------------------------
 * Class Name: MainAdminHomeCompanyAdapter
 * Created By: Suganya
 * Created Date: 10-10-2017
 * Modified By:
 * Modified Date:
 * Purpose: MaintenanceMessageAdapter class for list the queue data in a maintenance message queue CustomList
 * -----------------------------------------------------------------------------
 */

public class MainAdminHomeCompanyAdapter extends RecyclerView.Adapter<MainAdminHomeCompanyAdapter.MainAdminHomeCompanyHolder> {

    private ArrayList<CompanyDetailsModel> alCompanyDetailsModel;
    private UserDetailsModel objUserDetailsModel;
    private InstApplication ApplicationObject;
    private Context context;

    public MainAdminHomeCompanyAdapter(Activity context, ArrayList<CompanyDetailsModel> alCompanyDetailsModel,UserDetailsModel objUserDetailsModel) {
        this.context = context;
        this.alCompanyDetailsModel = alCompanyDetailsModel;
        this.objUserDetailsModel = objUserDetailsModel;
        ApplicationObject = ((InstApplication) context.getApplicationContext());
    }

    @Override
    public MainAdminHomeCompanyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.main_admin_country_adapter, parent, false);
        MainAdminHomeCompanyHolder mainAdminHomeCompanyHolder = new MainAdminHomeCompanyHolder(v);
        return mainAdminHomeCompanyHolder;
    }

    @Override
    public void onBindViewHolder(MainAdminHomeCompanyHolder mainAdminHomeCompanyHolder, final int position) {
        mainAdminHomeCompanyHolder.tvCompanyName.setText(alCompanyDetailsModel.get(position).getCompanyName());
        mainAdminHomeCompanyHolder.tvCompanyName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                objUserDetailsModel.setDomain(alCompanyDetailsModel.get(position).getDomain());
                ApplicationObject.setCompanyDetailsModel(alCompanyDetailsModel.get(position));
                ScreenNavigator.callSubAdminHomeActivity((Activity) context, objUserDetailsModel);
                ((Activity) context).finish();
            }
        });

    }

    @Override
    public int getItemCount() {
        return alCompanyDetailsModel.size();
    }


    public class MainAdminHomeCompanyHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvCompanyName)
        TextView tvCompanyName;
        public MainAdminHomeCompanyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
