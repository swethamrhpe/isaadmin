package com.support.admin.instantsupport.HomeScreen;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.support.admin.instantsupport.Application.Constants;
import com.support.admin.instantsupport.Application.InstApplication;
import com.support.admin.instantsupport.IHttpConnectionInterface;
import com.support.admin.instantsupport.Login.UserDetailsModel;
import com.support.admin.instantsupport.R;

import com.support.admin.instantsupport.Utility.AppLog;
import com.support.admin.instantsupport.Utility.GetHttpConnectionResponseTask;
import com.support.admin.instantsupport.Utility.JSONFormater;
import com.support.admin.instantsupport.Utility.ScreenNavigator;
import com.support.admin.instantsupport.Utility.Utils;

import org.json.fh.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.support.admin.instantsupport.Application.Constants.CLOUD.REQ_GET_ALL_MAINTENANCE_MESSAGE;
import static com.support.admin.instantsupport.Application.Constants.CLOUD.REQ_GET_ALL_SELF_SUPPORT_DETAILS;
import static com.support.admin.instantsupport.Application.Constants.CLOUD.REQ_GET_CALL_IT_SUPPORT_FOR_COMPANY;
import static com.support.admin.instantsupport.Application.Constants.CLOUD.REQ_GET_CHAT_IT_SUPPORT_FOR_COMPANY;
import static com.support.admin.instantsupport.Application.Constants.CLOUD.REQ_GET_SPOT_DETAILS;
import static com.support.admin.instantsupport.Application.Constants.CLOUD.REQ_GET_USER_DETAILS_COMPANY;

/**
 * -----------------------------------------------------------------------------
 * Class Name: SubAdminHomeActivity
 * Created By: Suganya
 * Created Date: 14-07-2017
 * Modified By:
 * Modified Date:
 * Purpose: Display Sub Admin Details
 * -----------------------------------------------------------------------------
 */

public class SubAdminHomeActivity extends AppCompatActivity implements Constants, IHttpConnectionInterface, ISubAdminInterface, View.OnClickListener {

    private static final String TAG = "SubAdminHomeActivity";
    private TextView tvNormalUserCount, tvVIPUserCount, tvTechUserCount, tvCallITSupportCount, tvChatITSupportCount, tvSpotITSupportCount, tvSSCount, tvMMCount, tvUserCount, tvActionbarTitle;
    private InstApplication ApplicationObject;
    private Typeface fontArial;
    private UserDetailsModel objUserDetailsModel;
    private ProgressDialog mProgressDialog;
    private int iGetAllDetailsFlag, iNormalUserCount, iVIPUserCount, iTechUserCount;
    private ArrayList<MaintenanceMessageModel> alMaintenanceMessageModel;
    private UserDetailsModel alUserDetailsModel;
    private ArrayList<String> alUserStrings;
    //UserDataModel.CustomList userDataModel;

    private Spinner spinnerEnvironment;
    private CompanyDetailsModel objCompanyDetailsModel;

    private ArrayList<CompanyUserDetailsModel> alCompanyUserDetailsModel;
    private ArrayList<CallITSupportModel> alCallITSupportModel;
    private ArrayList<ChatITSupportModel> alChatITSupportModel;

    private ArrayList<SpotDetailModel> alSportDetailModel;
    private ArrayList<SelfSupportModel> alSelfSupportModel;
    private List<String> envTitleList;
    private ArrayList<Integer> envIconList;
    private Integer iDevFlag, iTestFlag, iLiveFlag, iEnvFlag;
    private LinearLayout llMMCount;
    private ISubAdminInterface iSubAdminInterface;

    LinearLayout llUserDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_admin_home);
        ApplicationObject = ((InstApplication) getApplicationContext());
        objUserDetailsModel = new UserDetailsModel();
        objUserDetailsModel = getIntent().getExtras().getParcelable(Constants.BundleKeyConstants.ADMIN_DETAIL);
        fontArial = Typeface.createFromAsset(this.getAssets(), Constants.FONT_ARIAL);
        tvNormalUserCount = (TextView) findViewById(R.id.tvNormalUserCount);
        tvVIPUserCount = (TextView) findViewById(R.id.tvVIPUserCount);
        tvTechUserCount = (TextView) findViewById(R.id.tvTechUserCount);
        tvCallITSupportCount = (TextView) findViewById(R.id.tvCallITSupportCount);
        tvChatITSupportCount = (TextView) findViewById(R.id.tvChatITSupportCount);
        tvSpotITSupportCount = (TextView) findViewById(R.id.tvSpotITSupportCount);
        tvSSCount = (TextView) findViewById(R.id.tvSSCount);
        tvMMCount = (TextView) findViewById(R.id.tvMMCount);
        tvUserCount = (TextView) findViewById(R.id.tvUserCount);
        tvActionbarTitle = (TextView) findViewById(R.id.tvActionbarTitle);
        spinnerEnvironment = (Spinner) findViewById(R.id.spinnerEnvironment);
        llMMCount = (LinearLayout) findViewById(R.id.llMMCount);
        llUserDetail = (LinearLayout)findViewById( R.id.llUserdetial);

        alCompanyUserDetailsModel = new ArrayList<CompanyUserDetailsModel>();
        alCallITSupportModel = new ArrayList<CallITSupportModel>();
        alChatITSupportModel = new ArrayList<ChatITSupportModel>();
        alSportDetailModel = new ArrayList<SpotDetailModel>();
        alSelfSupportModel = new ArrayList<SelfSupportModel>();
        alMaintenanceMessageModel = new ArrayList<MaintenanceMessageModel>();
        alUserDetailsModel = new UserDetailsModel();
       // aluserDataModel = new UserDataModel();
        envIconList = new ArrayList<>();
        envTitleList = new ArrayList<>();


        tvNormalUserCount.setTypeface(fontArial);
        tvNormalUserCount.setTypeface(Typeface.DEFAULT_BOLD);
        tvVIPUserCount.setTypeface(fontArial);
        tvVIPUserCount.setTypeface(Typeface.DEFAULT_BOLD);
        tvTechUserCount.setTypeface(fontArial);
        tvTechUserCount.setTypeface(Typeface.DEFAULT_BOLD);
        tvCallITSupportCount.setTypeface(fontArial);
        tvCallITSupportCount.setTypeface(Typeface.DEFAULT_BOLD);
        tvChatITSupportCount.setTypeface(fontArial);
        tvChatITSupportCount.setTypeface(Typeface.DEFAULT_BOLD);
        tvSpotITSupportCount.setTypeface(fontArial);
        tvSpotITSupportCount.setTypeface(Typeface.DEFAULT_BOLD);
        tvSSCount.setTypeface(fontArial);
        tvSSCount.setTypeface(Typeface.DEFAULT_BOLD);
        tvMMCount.setTypeface(fontArial);
        tvMMCount.setTypeface(Typeface.DEFAULT_BOLD);
        tvUserCount.setTypeface(fontArial);
        tvUserCount.setTypeface(Typeface.DEFAULT_BOLD);

        tvNormalUserCount.setOnClickListener(SubAdminHomeActivity.this);
        tvVIPUserCount.setOnClickListener(SubAdminHomeActivity.this);
        tvTechUserCount.setOnClickListener(SubAdminHomeActivity.this);
        tvCallITSupportCount.setOnClickListener(SubAdminHomeActivity.this);
        tvChatITSupportCount.setOnClickListener(SubAdminHomeActivity.this);
        tvSpotITSupportCount.setOnClickListener(SubAdminHomeActivity.this);
        tvSSCount.setOnClickListener(SubAdminHomeActivity.this);
        //tvMMCount.setOnClickListener(SubAdminHomeActivity.this);
        llMMCount.setOnClickListener(SubAdminHomeActivity.this);
        llUserDetail.setOnClickListener(SubAdminHomeActivity.this);

        iEnvFlag = 0;
        if (objUserDetailsModel.getEnvironment().contains(DEV_ENV)) {
            envTitleList.add(getString(R.string.development));
            envIconList.add(R.drawable.icon_dev);
            iDevFlag = iEnvFlag;
        }
        if (objUserDetailsModel.getEnvironment().contains(TEST_ENV)) {
            envTitleList.add(getString(R.string.test));
            envIconList.add(R.drawable.icon_test);
            iTestFlag = ++iEnvFlag;
        }
        if (objUserDetailsModel.getEnvironment().contains(LIVE_ENV)) {
            envTitleList.add(getString(R.string.live));
            envIconList.add(R.drawable.icon_live);
            iLiveFlag = ++iEnvFlag;
        }
        if (ApplicationObject.getCompanyDetailsModel() != null) {
            objCompanyDetailsModel = ApplicationObject.getCompanyDetailsModel();
            tvActionbarTitle.setText(getString(R.string.appbarTitle) + objCompanyDetailsModel.getCompanyName());

            if (ApplicationObject.getAlCompanyUserDetailsModel() != null &&  ApplicationObject.getAlCompanyUserDetailsModel()!=null) {
                alCompanyUserDetailsModel = ApplicationObject.getAlCompanyUserDetailsModel();
                iNormalUserCount = 0;
                iVIPUserCount = 0;
                iTechUserCount = 0;
                AppLog.showLogE(TAG,"userderil in subadmin = "+alCompanyUserDetailsModel.get(0).getUserName());

                for (int index = 0; index < alCompanyUserDetailsModel.size(); index++) {
                    if (alCompanyUserDetailsModel.get(index).getType().equals(Constants.NORMAL_USER_TYPE)) {
                        iNormalUserCount++;
                    } else if (alCompanyUserDetailsModel.get(index).getType().equals(Constants.VIP_USER_TYPE)) {
                        iVIPUserCount++;
                    } else {
                        iTechUserCount++;
                    }
                }
                tvNormalUserCount.setText(String.valueOf(iNormalUserCount));
                tvVIPUserCount.setText(String.valueOf(iVIPUserCount));
                tvTechUserCount.setText(String.valueOf(iTechUserCount));
            }
            if (ApplicationObject.getAlCallITSupportModel() != null) {
                alCallITSupportModel = ApplicationObject.getAlCallITSupportModel();
            }
            if (ApplicationObject.getAlChatITSupportModel() != null) {
                alChatITSupportModel = ApplicationObject.getAlChatITSupportModel();
            }
            if (ApplicationObject.getAlSportDetailModel() != null) {
                alSportDetailModel = ApplicationObject.getAlSportDetailModel();
            }
            if (ApplicationObject.getAlSelfSupportModel() != null) {
                alSelfSupportModel = ApplicationObject.getAlSelfSupportModel();
            }
            if (ApplicationObject.getAlMaintenanceMessageModel() != null) {
                alMaintenanceMessageModel = ApplicationObject.getAlMaintenanceMessageModel();

            }
            if(ApplicationObject.getUserDetailsModel() !=null){
                alUserDetailsModel =  ApplicationObject.getUserDetailsModel();
                //AppLog.showLogE(TAG,"userdetail= "+alUserDetailsModel);
            }


        }
        if (objUserDetailsModel.getAdminType().equals(ADMIN_TYPE.MAIN_ADMIN)) {
            iSubAdminInterface = (ISubAdminInterface) SubAdminHomeActivity.this;
            iSubAdminInterface.processCompanyDetailsFinish(ApplicationObject.getCompanyDetailsModel());
        } else {
            SpinnerAdapter adapter = new SpinnerAdapter(this, R.layout.env_spinner_text, envTitleList, envIconList);
            spinnerEnvironment.setAdapter(adapter);
            if (ApplicationObject.getBaseEnv().equals(ENVIRONMENT.DEV)) {
                spinnerEnvironment.setSelection(iDevFlag);
            } else if (ApplicationObject.getBaseEnv().equals(ENVIRONMENT.TEST)) {
                spinnerEnvironment.setSelection(iTestFlag);
            } else {
                spinnerEnvironment.setSelection(iLiveFlag);
            }
            spinnerEnvironment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int positionEnv, long l) {
                    if (positionEnv == 0) {
                        ApplicationObject.setBaseEnv(ENVIRONMENT.DEV);
                    } else if (positionEnv == 1) {
                        ApplicationObject.setBaseEnv(ENVIRONMENT.TEST);
                    } else {
                        ApplicationObject.setBaseEnv(ENVIRONMENT.LIVE);
                    }
                    if (objCompanyDetailsModel == null) {
                        getCompanyDetails();
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
    }

    private void getCompanyDetails() {
        if (Utils.isInternetOn(SubAdminHomeActivity.this)) {
            mProgressDialog = ProgressDialog.show(this, null, getResources().getString(R.string.pleasewait));
            mProgressDialog.setContentView(R.layout.progressdialog_custom);
            mProgressDialog.setCancelable(false);
            try {
                iGetAllDetailsFlag = 0;

                new GetHttpConnectionResponseTask(SubAdminHomeActivity.this, ApplicationObject.getBaseEnv() + CLOUD.REQ_METHOD_SUB_ADMIN_COMPANY_DETAIL + "?domain=" + objUserDetailsModel.getDomain(), null, Method.GET).execute();
            } catch (Exception e) {
                AppLog.showLogE(TAG, "Exception>>>" + e);
            }
        } else {

            Utils.ShowDialog(SubAdminHomeActivity.this, this.getString(R.string.app_name), getString(R.string.ErrorInternetConn));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llUserdetial:
                AppLog.showLogE(TAG,"alcom "+alCompanyUserDetailsModel);

                ScreenNavigator.callUserDataActivity(SubAdminHomeActivity.this, this.alCompanyUserDetailsModel);
                finish();
                objUserDetailsModel = null;
                break;
            case R.id.tvNormalUserCount:

                break;

            case R.id.tvVIPUserCount:

                break;
            case R.id.tvTechUserCount:

                break;
            case R.id.tvCallITSupportCount:
                ScreenNavigator.callITSupportActivity(SubAdminHomeActivity.this, alMaintenanceMessageModel);
                finish();
                break;
            case R.id.tvChatITSupportCount:

                break;
            case R.id.tvSpotITSupportCount:
                ScreenNavigator.callSupportSpotActivity(SubAdminHomeActivity.this, alSportDetailModel);
                finish();


                break;
            case R.id.tvSSCount:

                break;
            case R.id.llMMCount:
                ScreenNavigator.callMaintenanceMessageActivity(SubAdminHomeActivity.this, alMaintenanceMessageModel);
                finish();
                break;
//            case R.id.tvMMCount:
//
//                ScreenNavigator.callMaintenanceMessageActivity(SubAdminHomeActivity.this, alMaintenanceMessageModel);
//                finish();
//                break;

        }

    }

    @Override
    public void processConnectionFinish(String jsonObject) {
        try {
            AppLog.showLogE(TAG,"igetdetail= "+iGetAllDetailsFlag);
            JSONObject jsonResponse = new JSONObject(jsonObject);
           // JSONFormater.getAdminUserData(SubAdminHomeActivity.this, jsonResponse);
            if (iGetAllDetailsFlag == 0) {
                JSONFormater.getAdminCompanyDetails(SubAdminHomeActivity.this, jsonResponse);
            } else if (iGetAllDetailsFlag == 1) {
                JSONFormater.getAdminCompanyUserDetails(SubAdminHomeActivity.this, jsonResponse);
               // JSONFormater.getAdminUserData(SubAdminHomeActivity.this, jsonResponse);
            } else if (iGetAllDetailsFlag == 2) {
                JSONFormater.getAdminCallITSupportDetails(SubAdminHomeActivity.this, jsonResponse);
            } else if (iGetAllDetailsFlag == 3) {
                JSONFormater.getAdminChatITSupportDetails(SubAdminHomeActivity.this, jsonResponse);
            } else if (iGetAllDetailsFlag == 4) {
                JSONFormater.getAdminSupportSpotDetails(SubAdminHomeActivity.this, jsonResponse);
            } else if (iGetAllDetailsFlag == 5) {
                JSONFormater.getAdminSelfSupportDetails(SubAdminHomeActivity.this, jsonResponse);
            } else if (iGetAllDetailsFlag == 6) {
                JSONFormater.getAdminMaintenanceMessageMessageDetails(SubAdminHomeActivity.this, jsonResponse);
            }
        } catch (Exception e) {
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismiss();
            e.printStackTrace();
        }

    }

    @Override
    public void processCompanyDetailsFinish(CompanyDetailsModel objCompanyDetailsModel) {
        if (objCompanyDetailsModel != null && objCompanyDetailsModel.getMessage().contains(Constants.CONST_CLOUD_CALL.SUCCESS)) {
            ApplicationObject.setCompanyDetailsModel(objCompanyDetailsModel);
            this.objCompanyDetailsModel = objCompanyDetailsModel;
            if (Utils.isInternetOn(SubAdminHomeActivity.this)) {
                try {
                    iGetAllDetailsFlag++;
                    tvActionbarTitle.setText(getString(R.string.appbarTitle) + objCompanyDetailsModel.getCompanyName());
                    new GetHttpConnectionResponseTask(SubAdminHomeActivity.this, ApplicationObject.getBaseEnv() + REQ_GET_USER_DETAILS_COMPANY + "?companyId=" + objCompanyDetailsModel.getCompanyId(), null, Method.GET).execute();
                } catch (Exception e) {
                    AppLog.showLogE(TAG, "Exception>>>" + e);
                }
            } else {

                Utils.ShowDialog(SubAdminHomeActivity.this, this.getString(R.string.app_name), getString(R.string.ErrorInternetConn));
            }
        } else if (objCompanyDetailsModel != null && objCompanyDetailsModel.getMessage().contains(Constants.ERROR_CODE.NO_ADMIN_COMPANY)) {
            ApplicationObject.setCompanyDetailsModel(null);
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismiss();
            Utils.ShowDialogWithDismiss(SubAdminHomeActivity.this, getString(R.string.app_name), getString(R.string.Error_NoCompany), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    ScreenNavigator.callLoginActivity(SubAdminHomeActivity.this);
                    finish();
                }
            });
        } else {
            ApplicationObject.setCompanyDetailsModel(null);
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismiss();
            Utils.ShowDalogOKCancel(SubAdminHomeActivity.this, getString(R.string.app_name), "\n" + getString(R.string.ServerError));
        }

    }

    @Override
    public void processCompanyUserDetailsFinish(ArrayList<CompanyUserDetailsModel> alCompanyUserDetailsModel) {

        if (alCompanyUserDetailsModel != null && alCompanyUserDetailsModel.size() >0  && alCompanyUserDetailsModel.get(0).getMessage().contains(Constants.CONST_CLOUD_CALL.SUCCESS)) {
            if (Utils.isInternetOn(SubAdminHomeActivity.this)) {
                try {
                    iNormalUserCount = 0;
                    iVIPUserCount = 0;
                    iTechUserCount = 0;

                    for (int index = 0; index < alCompanyUserDetailsModel.size(); index++) {
                        if (alCompanyUserDetailsModel.get(index).getType().equals(Constants.NORMAL_USER_TYPE)) {
                            iNormalUserCount++;
                        } else if (alCompanyUserDetailsModel.get(index).getType().equals(Constants.VIP_USER_TYPE)) {
                            iVIPUserCount++;
                        } else {
                            iTechUserCount++;
                        }
                    }
                    tvNormalUserCount.setText(String.valueOf(iNormalUserCount));
                    tvVIPUserCount.setText(String.valueOf(iVIPUserCount));
                    tvTechUserCount.setText(String.valueOf(iTechUserCount));
                    AppLog.showLogE(TAG,"all user dara=" +alCompanyUserDetailsModel);
                 //   ApplicationObject.setObjUserDataModel(alCompanyUserDetailsModel);
                    ApplicationObject.setAlCompanyUserDetailsModel(alCompanyUserDetailsModel);
                    iGetAllDetailsFlag++;
                    new GetHttpConnectionResponseTask(SubAdminHomeActivity.this, ApplicationObject.getBaseEnv() + REQ_GET_CALL_IT_SUPPORT_FOR_COMPANY + "?companyId=" + objCompanyDetailsModel.getCompanyId(), null, Method.GET).execute();
                } catch (Exception e) {
                    if (mProgressDialog != null && mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    AppLog.showLogE(TAG, "Exception>>>" + e);
                }
            } else {
                if (mProgressDialog != null && mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Utils.ShowDialog(SubAdminHomeActivity.this, this.getString(R.string.app_name), getString(R.string.ErrorInternetConn));
            }
        } else if (alCompanyUserDetailsModel != null && alCompanyUserDetailsModel.get(0).getMessage().contains(Constants.ERROR_CODE.NO_ADMIN_COMPANY_USER)) {
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismiss();
            Utils.ShowDialogWithDismiss(SubAdminHomeActivity.this, getString(R.string.app_name), getString(R.string.Error_NoUsersCompany), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        } else {
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismiss();
            Utils.ShowDalogOKCancel(SubAdminHomeActivity.this, getString(R.string.app_name), "\n" + getString(R.string.ServerError));
        }
    }

    @Override
    public void processCallITSupportDetailsFinish(ArrayList<CallITSupportModel> alCallITSupportModel) {
        if (alCallITSupportModel != null) {
            if (Utils.isInternetOn(SubAdminHomeActivity.this)) {
                try {
                    ApplicationObject.setAlCallITSupportModel(alCallITSupportModel);
                    iGetAllDetailsFlag++;
                    new GetHttpConnectionResponseTask(SubAdminHomeActivity.this, ApplicationObject.getBaseEnv() + REQ_GET_CHAT_IT_SUPPORT_FOR_COMPANY + "?companyId=" + objCompanyDetailsModel.getCompanyId(), null, Method.GET).execute();
                } catch (Exception e) {
                    AppLog.showLogE(TAG, "Exception>>>" + e);
                }
            } else {
                if (mProgressDialog != null && mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Utils.ShowDialog(SubAdminHomeActivity.this, this.getString(R.string.app_name), getString(R.string.ErrorInternetConn));
            }
        } else {
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismiss();
        }

    }

    @Override
    public void processChatITSupportDetailsFinish(ArrayList<ChatITSupportModel> alChatITSupportModel) {
        if (alChatITSupportModel != null) {
            if (Utils.isInternetOn(SubAdminHomeActivity.this)) {
                try {
                    ApplicationObject.setAlChatITSupportModel(alChatITSupportModel);
                    iGetAllDetailsFlag++;
                    new GetHttpConnectionResponseTask(SubAdminHomeActivity.this, ApplicationObject.getBaseEnv() + REQ_GET_SPOT_DETAILS + "?companyId=" + objCompanyDetailsModel.getCompanyId(), null, Method.GET).execute();
                } catch (Exception e) {
                    if (mProgressDialog != null && mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    AppLog.showLogE(TAG, "Exception>>>" + e);
                }
            } else {
                if (mProgressDialog != null && mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Utils.ShowDialog(SubAdminHomeActivity.this, this.getString(R.string.app_name), getString(R.string.ErrorInternetConn));
            }
        } else {
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismiss();
        }
    }

    @Override
    public void processSupportSpotDetailsFinish(ArrayList<SpotDetailModel> alSportDetailModel) {
        if (alSportDetailModel != null) {
            if (Utils.isInternetOn(SubAdminHomeActivity.this)) {
                try {
                    ApplicationObject.setAlSportDetailModel(alSportDetailModel);
                    iGetAllDetailsFlag++;
                    new GetHttpConnectionResponseTask(SubAdminHomeActivity.this, ApplicationObject.getBaseEnv() + REQ_GET_ALL_SELF_SUPPORT_DETAILS + "?companyId=" + objCompanyDetailsModel.getCompanyId(), null, Method.GET).execute();
                } catch (Exception e) {
                    if (mProgressDialog != null && mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    AppLog.showLogE(TAG, "Exception>>>" + e);
                }
            } else {
                if (mProgressDialog != null && mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Utils.ShowDialog(SubAdminHomeActivity.this, this.getString(R.string.app_name), getString(R.string.ErrorInternetConn));
            }
        } else {
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismiss();
        }

    }

    @Override
    public void processSelfSupportFinish(ArrayList<SelfSupportModel> alSelfSupportModel) {
        if (alSelfSupportModel != null) {
            if (Utils.isInternetOn(SubAdminHomeActivity.this)) {
                try {
                    ApplicationObject.setAlSelfSupportModel(alSelfSupportModel);
                    iGetAllDetailsFlag++;
                    new GetHttpConnectionResponseTask(SubAdminHomeActivity.this, ApplicationObject.getBaseEnv() + REQ_GET_ALL_MAINTENANCE_MESSAGE + "?companyId=" + objCompanyDetailsModel.getCompanyId(), null, Method.GET).execute();
                } catch (Exception e) {
                    if (mProgressDialog != null && mProgressDialog.isShowing())
                        mProgressDialog.dismiss();
                    AppLog.showLogE(TAG, "Exception>>>" + e);
                }
            } else {
                if (mProgressDialog != null && mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Utils.ShowDialog(SubAdminHomeActivity.this, this.getString(R.string.app_name), getString(R.string.ErrorInternetConn));
            }
        } else {
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismiss();
        }

    }

    @Override
    public void processMaintenanceMessageFinish(ArrayList<MaintenanceMessageModel> alMaintenanceMessageModel) {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
        if (alMaintenanceMessageModel != null) {
            this.alMaintenanceMessageModel = alMaintenanceMessageModel;
            ApplicationObject.setAlMaintenanceMessageModel(alMaintenanceMessageModel);
        } else {

        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(objUserDetailsModel.getAdminType().equals(ADMIN_TYPE.MAIN_ADMIN)) {
            ScreenNavigator.callMainAdminHomeActivity(SubAdminHomeActivity.this, objUserDetailsModel);
            finish();
        }
    }
}
