package com.support.admin.instantsupport.SuportSpot;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.support.admin.instantsupport.Application.Constants;
import com.support.admin.instantsupport.Application.InstApplication;
import com.support.admin.instantsupport.HomeScreen.CompanyDetailsModel;
import com.support.admin.instantsupport.HomeScreen.SpotDetailModel;
import com.support.admin.instantsupport.IHttpConnectionInterface;
import com.support.admin.instantsupport.Login.UserDetailsModel;
import com.support.admin.instantsupport.R;
import com.support.admin.instantsupport.Utility.AppLog;
import com.support.admin.instantsupport.Utility.GetHttpConnectionResponseTask;
import com.support.admin.instantsupport.Utility.ScreenNavigator;
import com.support.admin.instantsupport.Utility.Utils;

import org.json.fh.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.support.admin.instantsupport.Application.Constants.CLOUD.REQ_CREATE_WALKIN_CENTER;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.ADDRESSFIELD1;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.ADDRESSFIELD2;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.CENTERTITLE;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.COMPANY_ID;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.COUNTRYCODE;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.ISMERIDIANEXIST;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.LATITUDE;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.LONGITUDE;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.SERVICEDETAILS;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.SERVICEHOUR;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.SERVICE_TYPE;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.STATE;
import static com.support.admin.instantsupport.Application.Constants.CONST_CLOUD_CALL.ZIP_PIN;

/* -----------------------------------------------------------------------------
 * Class Name: SupportSpotDetailActivity
 * Created By: Swetha MR
 * Created Date: 1-09-2018
 * Modified By:
 * Modified Date:
 * Purpose: SupportSpotDetailActivity class for adding / Editing the walkin center complete details
 * -----------------------------------------------------------------------------
 */
public class SupportSpotDetailActivity extends Activity implements IHttpConnectionInterface {
    private static final String TAG = "SupportSpotDetailActivity";
    @BindView(R.id.ivLoginIcon)
    ImageView ivLoginIcon;
    @BindView(R.id.eServiceTitle)
    EditText etServiceTitle;
    @BindView(R.id.eduServiceDetails)
    EditText etServiceDetails;
    @BindView(R.id.radioGroupServiceType)
    RadioGroup radioGroupServiceType;
    @BindView(R.id.eduServiceHour)
    EditText etServiceHour;
    @BindView(R.id.eduAdrlin1)
    EditText etAdrlin1;
    @BindView(R.id.eduAdreLine2)
    EditText etAdreLine2;
    @BindView(R.id.eduScity)
    EditText etScity;
    @BindView(R.id.eduSstate)
    EditText etSstate;
    @BindView(R.id.edusZip)
    EditText etSzip;
    @BindView(R.id.spinScountry)
    Spinner spinScountry;
    @BindView(R.id.edusLat)
    EditText etSlat;
    @BindView(R.id.eduServLang)
    EditText etServLang;
    @BindView(R.id.btnadduser)
    Button btAdduser;
    @BindView(R.id.btnSubmit)
    Button btSubmit;
    @BindView(R.id.radioWalkin)
    RadioButton radioWalkin;
    @BindView(R.id.radioKiosk)
    RadioButton radioKiosk;
    @BindView(R.id.radioTechSite)
    RadioButton radioTechSite;
    RadioButton radiobtnCommon;
    private ProgressDialog mProgressDialog;
    private InstApplication applicationObject;
    private UserDetailsModel objUserDetailsModel;
    private ArrayList<SpotDetailModel> arrSupportSpotsList;
    private SpotDetailModel objSpotDetailModel;
    private CompanyDetailsModel objCompanyDetailsModel;
    private String SelectedPos;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addsupportspot);
        ButterKnife.bind(this);
        applicationObject = ((InstApplication) getApplicationContext());
        objUserDetailsModel = applicationObject.getUserDetailsModel();
        arrSupportSpotsList = applicationObject.getAlSportDetailModel();
        objCompanyDetailsModel = applicationObject.getCompanyDetailsModel();
        Bundle bundle = getIntent().getExtras();
        radiobtnCommon = new RadioButton(this);
        if(bundle !=null){
            objSpotDetailModel = bundle.getParcelable(Constants.BundleKeyConstants.SUPPORT_SPOT);
        }
        String [] arr = getResources().getStringArray(R.array.arrCountrycodes);
        ArrayList<String>  arrNew = new ArrayList<>();
        for(String s:arr){
            String lastTwo = s.substring(s.length() - 2);
            arrNew.add(lastTwo);
        }
        ArrayAdapter<String> spinArray = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_dropdown_item, arrNew);
        spinScountry.setAdapter(spinArray);
        if(objSpotDetailModel!=null){
            etAdrlin1.setText(objSpotDetailModel.getAddressField1());
            etAdreLine2.setText(objSpotDetailModel.getAddressField2());
            String objCC = objSpotDetailModel.getCountryCode();
            for (int i=0;i<spinScountry.getCount();i++) {
                if (spinScountry.getItemAtPosition(i).equals(objCC)) {
                    spinScountry.setSelection(i);
                }
            }
            etServiceDetails.setText(objSpotDetailModel.getServiceDetails());
            etServiceTitle.setText(objSpotDetailModel.getCenterTitle());
            etServLang.setText(objSpotDetailModel.getLongitude());
            etSlat.setText(objSpotDetailModel.getLatitude());
            etServiceHour.setText(objSpotDetailModel.getServiceHour());
            etSzip.setText(objSpotDetailModel.getZipCode());
            etSstate.setText(objSpotDetailModel.getState());
            if(objSpotDetailModel.getSupportSpotType().equalsIgnoreCase("walkin")){
                radioWalkin.setChecked(true);
                radiobtnCommon.setText(radioWalkin.getText());
            }else if(objSpotDetailModel.getSupportSpotType().equalsIgnoreCase("kiosk")){
                radioKiosk.setChecked(true);
                radiobtnCommon.setText(radioKiosk.getText());
            }else{
                radioTechSite.setChecked(true);
                radiobtnCommon.setText(radioTechSite.getText());
            }
            btSubmit.setText("UPDATE");
            radiobtnCommon.getText();
        }else{
            radioWalkin.setChecked(true);
            btSubmit.setText("Submit");
            spinScountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    SelectedPos = String.valueOf(position);
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        //radioGroupServiceType.clearCheck();
        radioGroupServiceType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(radiobtnCommon!=null) {
                    radiobtnCommon = (RadioButton) group.findViewById(checkedId);
                    if (null != radiobtnCommon && checkedId > -1) {
                        AppLog.showLogE(TAG, "radiowalk=" + radiobtnCommon.getText());
                    }
                }else{
                    radioWalkin.setChecked(true);
                }
            }
        });

    }
    @OnClick({R.id.btnadduser, R.id.btnSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnadduser:
                break;
            case R.id.btnSubmit:
                try {
                    if (Utils.isInternetOn(SupportSpotDetailActivity.this)) {
                        mProgressDialog = ProgressDialog.show(this, null, getResources().getString(R.string.pleasewait));
                        mProgressDialog.setContentView(R.layout.progressdialog_custom);
                        mProgressDialog.setCancelable(false);
                        if (etSlat.getText().toString().trim().equalsIgnoreCase("")  ) {
                            etSlat.setError("Please enter proper values");

                        }
                        if( etServLang.getText().toString().trim().equalsIgnoreCase("")){
                            etServLang.setError("Please enter proper values");
                        }
                        if(!TextUtils.isEmpty(etSlat.getText()) ) {
                            if (!isValidLat(Double.valueOf(etSlat.getText().toString()))) {
                                etSlat.setError("Please enter proper values");
                            }
                        }
                        if(!TextUtils.isEmpty(etServLang.getText())){
                            if (!isValidLng(Double.valueOf(etServLang.getText().toString()))) {
                                etServLang.setError("Please enter proper values");
                            }
                        }
                        if (etServiceTitle.getText().toString().trim().equalsIgnoreCase("")  ) {
                            etServiceTitle.setError("Please enter proper values");
                        }
                        if (etServiceDetails.getText().toString().trim().equalsIgnoreCase("")  ) {
                            etServiceDetails.setError("Please enter proper values");
                        }
                        if (etSstate.getText().toString().trim().equalsIgnoreCase("")  ) {
                            etSstate.setError("Please enter proper values");
                        }
                        if (spinScountry.getSelectedItem().toString().trim().equalsIgnoreCase("")  ) {
                            //spinScountry.setError("Please enter proper values");
                        }
                        JSONObject jsonParam = new JSONObject();
                        if (!TextUtils.isEmpty(etServiceTitle.getText().toString()) &&
                                !TextUtils.isEmpty(etServiceDetails.getText().toString()) &&
                                !TextUtils.isEmpty(etAdrlin1.getText().toString()) &&
                                !TextUtils.isEmpty(etSstate.getText().toString()) &&
                                !TextUtils.isEmpty(spinScountry.getSelectedItem().toString()) &&
                                !TextUtils.isEmpty(etSlat.getText().toString()) &&
                                isValidLat(Double.valueOf(etSlat.getText().toString()))&&
                                isValidLng(Double.valueOf(etServLang.getText().toString())) &&
                                !TextUtils.isEmpty(etServLang.getText().toString())) {
                            jsonParam.put(COMPANY_ID, objCompanyDetailsModel.getCompanyId());
                            jsonParam.put(CENTERTITLE, etServiceTitle.getText().toString());
                            jsonParam.put(SERVICEDETAILS, etServiceDetails.getText().toString());
                            jsonParam.put(ADDRESSFIELD1, etAdrlin1.getText().toString());
                            jsonParam.put(ADDRESSFIELD2, etAdreLine2.getText().toString());
                            jsonParam.put(STATE, etSstate.getText().toString());
                            jsonParam.put(COUNTRYCODE, spinScountry.getSelectedItem().toString());
                            jsonParam.put(LONGITUDE, etServLang.getText().toString());
                            jsonParam.put(LATITUDE, etSlat.getText().toString());
                            jsonParam.put(SERVICEHOUR, etServiceHour.getText().toString());
                            jsonParam.put(ZIP_PIN, etSzip.getText().toString());
                            AppLog.showLogE(TAG, "radiobtnCommon.getText().toString() =" + radiobtnCommon.getText());
                            jsonParam.put(SERVICE_TYPE, radiobtnCommon.getText());
                            jsonParam.put(ISMERIDIANEXIST, false);
                            AppLog.showLogE(TAG, "jsonparma =" + jsonParam);
                            new GetHttpConnectionResponseTask(SupportSpotDetailActivity.this,
                                    applicationObject.getBaseEnv() + REQ_CREATE_WALKIN_CENTER, jsonParam,
                                    Constants.Method.POST).execute();
                        } else {
                            if (mProgressDialog != null && mProgressDialog.isShowing())
                                mProgressDialog.dismiss();
                            Utils.ShowDialog(SupportSpotDetailActivity.this, this.getString(R.string.app_name),
                                    getString(R.string.entermanddetails));
                        }
                    } else {
                        if (mProgressDialog != null && mProgressDialog.isShowing())
                            mProgressDialog.dismiss();
                        Utils.ShowDialog(SupportSpotDetailActivity.this, this.getString(R.string.app_name),
                                getString(R.string.checkData));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void processConnectionFinish(String jsonObject) {
        AppLog.showLogE(TAG, "jsonobject= " + jsonObject);
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
        /*ScreenNavigator.callSubAdminHomeActivity(SupportSpotDetailActivity.this,objUserDetailsModel);
        finish();*/
    }

    /*Validation for lat & long*/
    public boolean isValidLat(double lat) {
        if (lat < -90 || lat > 90) {
            return false;
        }
        return true;
    }
    public boolean isValidLng( double lng){
        if(lng < -180 || lng > 180)
        {
            return false;
        }
        return true;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ScreenNavigator.callSupportSpotActivity(SupportSpotDetailActivity.this,arrSupportSpotsList);
        finish();;
    }
}
