package com.support.admin.instantsupport.HomeScreen;

import java.util.ArrayList;

/**-----------------------------------------------------------------------------
 Class Name: IMainAdminHomeInterface
 Created By: Suganya
 Created Date: 10-10-2017
 Modified By:
 Modified Date:
 Purpose: Interface for getting Main Admin access details
 -----------------------------------------------------------------------------*/

public interface IMainAdminHomeInterface {
    void processAllCompanyDetailsFinish( ArrayList<CompanyDetailsModel> alCompanyDetailsModel);
}
