package com.support.admin.instantsupport.CallITSupport;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telecom.Call;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.support.admin.instantsupport.Application.Constants;
import com.support.admin.instantsupport.Application.InstApplication;
import com.support.admin.instantsupport.HomeScreen.MaintenanceMessageModel;
import com.support.admin.instantsupport.HomeScreen.SubAdminHomeActivity;
import com.support.admin.instantsupport.Login.UserDetailsModel;
import com.support.admin.instantsupport.R;
import com.support.admin.instantsupport.Utility.AppLog;
import com.support.admin.instantsupport.Utility.GetHttpConnectionResponseTask;
import com.support.admin.instantsupport.Utility.ScreenNavigator;
import com.support.admin.instantsupport.Utility.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CallITSupportActivity extends AppCompatActivity {

    private InstApplication ApplicationObject;
    private ArrayList<MaintenanceMessageModel> alMaintenanceMessageModel;
    private RecyclerView rvMaintenaceMesasage;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private UserDetailsModel objUserDetailsModel;
    private static final String TAG = "CallITSupportActivity";
    private ProgressDialog mProgressDialog;
    @BindView(R.id.spinSelectLanguage)
    Spinner spinLanguage ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_callitsupport);
        //spinLanguage = (Spinner)findViewById(R.id.spinSelectLanguage);
       // ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,bankNames);
      //  aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
       // spinLanguage.setAdapter(aa);


    }

    private void getCallITSupportDetails() {
        if (Utils.isInternetOn(CallITSupportActivity.this)) {
            mProgressDialog = ProgressDialog.show(this, null, getResources().getString(R.string.pleasewait));
            mProgressDialog.setContentView(R.layout.progressdialog_custom);
            mProgressDialog.setCancelable(false);
            try {
              //  iGetAllDetailsFlag = 0;

                new GetHttpConnectionResponseTask(CallITSupportActivity.this, ApplicationObject.getBaseEnv() + Constants.CLOUD.REQ_GET_CALL_IT_SUPPORT_FOR_COMPANY + "?domain=" + objUserDetailsModel.getDomain(), null, Constants.Method.GET).execute();
            } catch (Exception e) {
                AppLog.showLogE(TAG, "Exception>>>" + e);
            }
        } else {

            Utils.ShowDialog(CallITSupportActivity.this, this.getString(R.string.app_name), getString(R.string.ErrorInternetConn));
        }
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ScreenNavigator.callSubAdminHomeActivity(CallITSupportActivity.this,objUserDetailsModel);
        finish();
    }
}
