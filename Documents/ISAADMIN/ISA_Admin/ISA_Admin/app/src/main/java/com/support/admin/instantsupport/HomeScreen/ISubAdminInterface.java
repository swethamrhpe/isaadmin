package com.support.admin.instantsupport.HomeScreen;




import java.util.ArrayList;

/**-----------------------------------------------------------------------------
 Class Name: ISubAdminInterface
 Created By: Suganya
 Created Date: 09-08-2017
 Modified By:
 Modified Date:
 Purpose: Interface for getting SubAdmin access details
 -----------------------------------------------------------------------------*/

public interface ISubAdminInterface {
    void processCompanyDetailsFinish(CompanyDetailsModel objCompanyDetailsModel);
    void processCompanyUserDetailsFinish(ArrayList<CompanyUserDetailsModel> alCompanyUserDetailsModel);
    void processCallITSupportDetailsFinish(ArrayList<CallITSupportModel> alCallITSupportModel);
    void processChatITSupportDetailsFinish(ArrayList<ChatITSupportModel> alChatITSupportModel);
    void processSupportSpotDetailsFinish(ArrayList<SpotDetailModel> alSportDetailModel);
    void processSelfSupportFinish(ArrayList<SelfSupportModel> alSelfSupportModel);
    void processMaintenanceMessageFinish(ArrayList<MaintenanceMessageModel> alMaintenanceMessageModel);
  //  void processUserDataFinish(UserDataModel alUserDataModel);
   // void processUserDataStringFinish(ArrayList<String> alUserDataModel);

}
