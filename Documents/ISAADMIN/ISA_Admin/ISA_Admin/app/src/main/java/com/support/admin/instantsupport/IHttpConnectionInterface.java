package com.support.admin.instantsupport;

/**
 * Created by ksug on 8/16/2017.
 */

public interface IHttpConnectionInterface {
    void processConnectionFinish(String jsonObject);
}
