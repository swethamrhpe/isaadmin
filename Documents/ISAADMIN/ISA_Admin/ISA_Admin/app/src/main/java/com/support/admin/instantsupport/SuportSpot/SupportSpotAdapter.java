package com.support.admin.instantsupport.SuportSpot;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.support.admin.instantsupport.HomeScreen.SpotDetailModel;
import com.support.admin.instantsupport.R;

import com.support.admin.instantsupport.Utility.AppLog;
import com.support.admin.instantsupport.Utility.ScreenNavigator;

import java.util.ArrayList;

/**
 * -----------------------------------------------------------------------------
 * Class Name: SupportSpotAdapter
 * Created By: Swetha M.R
 * Created Date: 1-09-2018
 * Modified By:
 * Modified Date:
 * Purpose: SupportSpotAdapter class for list the walkin data
 * -----------------------------------------------------------------------------
 */

public class SupportSpotAdapter extends RecyclerView.Adapter<SupportSpotAdapter.SupportSpotHolder>   {
    private final String TAG = "SupportSpotAdapter";
    private ArrayList<SpotDetailModel> alSpotDetailModel;
    private Context context;
    public SupportSpotAdapter(Activity context, ArrayList<SpotDetailModel> companyUserDetailsModel)
    {
        this.context = context;
        this.alSpotDetailModel = companyUserDetailsModel;
    }
    public class SupportSpotHolder extends RecyclerView.ViewHolder {
        public TextView tvSSName;
        public LinearLayout llMMLayout;
        public SupportSpotHolder(View v) {
            super(v);
            tvSSName = (TextView) v.findViewById(R.id.tvsupportspotName);
            llMMLayout = (LinearLayout) v.findViewById(R.id.llSslayout);
        }
    }
    @Override
    public SupportSpotHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.adapter_supportspot_list, parent, false);
        SupportSpotHolder supportspotHolder = new SupportSpotHolder(v);
        return supportspotHolder;
    }
    @Override
    public void onBindViewHolder(SupportSpotHolder supportSpotHolder, final int position) {
        String centerTitle = alSpotDetailModel.get(position).getCenterTitle();
        supportSpotHolder.tvSSName.setText(centerTitle);
        supportSpotHolder.llMMLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ScreenNavigator.callSupportSpotDetailActivity((Activity) context, alSpotDetailModel.get(position));
                ((Activity) context).finish();
            }
        });
    }
    @Override
    public int getItemCount() {
        if(alSpotDetailModel !=null)
            return alSpotDetailModel.size();
        else
            return  0;
    }
}
