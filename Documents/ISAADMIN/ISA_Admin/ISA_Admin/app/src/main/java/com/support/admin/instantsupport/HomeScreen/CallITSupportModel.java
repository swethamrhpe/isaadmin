package com.support.admin.instantsupport.HomeScreen;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * -----------------------------------------------------------------------------
 * Class Name: CallITSupportModel
 * Created By: Suganya
 * Created Date: 21-08-2017
 * Modified By:
 * Modified Date:
 * Purpose: CallITSupportModel for Storing admin Call IT Support details
 * -----------------------------------------------------------------------------
 */
public class CallITSupportModel implements Parcelable {

    private String startTimeInUTC;

    private String endTimeInUTC;

    private String cbaAddress;

    private String config;

    private ArrayList<CallITPhoneNumberModel> phoneNumbers;

    private String companyId;

    private String lang;

    private String numberOfQueues;

    private String uniqueId;

    private String serviceDaysPerWeek;

    public CallITSupportModel() {

    }


    public String getStartTimeInUTC() {
        return startTimeInUTC;
    }

    public void setStartTimeInUTC(String startTimeInUTC) {
        this.startTimeInUTC = startTimeInUTC;
    }

    public String getEndTimeInUTC() {
        return endTimeInUTC;
    }

    public void setEndTimeInUTC(String endTimeInUTC) {
        this.endTimeInUTC = endTimeInUTC;
    }

    public String getCbaAddress() {
        return cbaAddress;
    }

    public void setCbaAddress(String cbaAddress) {
        this.cbaAddress = cbaAddress;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public ArrayList<CallITPhoneNumberModel> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(ArrayList<CallITPhoneNumberModel> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getNumberOfQueues() {
        return numberOfQueues;
    }

    public void setNumberOfQueues(String numberOfQueues) {
        this.numberOfQueues = numberOfQueues;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getServiceDaysPerWeek() {
        return serviceDaysPerWeek;
    }

    public void setServiceDaysPerWeek(String serviceDaysPerWeek) {
        this.serviceDaysPerWeek = serviceDaysPerWeek;
    }

    @Override
    public String toString() {
        return "ClassPojo [startTimeInUTC = " + startTimeInUTC + ", endTimeInUTC = " + endTimeInUTC + ", cbaAddress = " + cbaAddress + ", config = " + config + ", phoneNumbers = " + phoneNumbers + ", companyId = " + companyId + ", lang = " + lang + ", numberOfQueues = " + numberOfQueues + ", uniqueId = " + uniqueId + ", serviceDaysPerWeek = " + serviceDaysPerWeek + "]";
    }

    protected CallITSupportModel(Parcel in) {
        startTimeInUTC = in.readString();
        endTimeInUTC = in.readString();
        cbaAddress = in.readString();
        config = in.readString();
        companyId = in.readString();
        lang = in.readString();
        numberOfQueues = in.readString();
        uniqueId = in.readString();
        serviceDaysPerWeek = in.readString();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(startTimeInUTC);
        dest.writeString(endTimeInUTC);
        dest.writeString(cbaAddress);
        dest.writeString(config);
        dest.writeString(companyId);
        dest.writeString(lang);
        dest.writeString(numberOfQueues);
        dest.writeString(uniqueId);
        dest.writeString(serviceDaysPerWeek);
    }

    public static final Creator<CallITSupportModel> CREATOR = new Creator<CallITSupportModel>() {
        @Override
        public CallITSupportModel createFromParcel(Parcel in) {
            return new CallITSupportModel(in);
        }

        @Override
        public CallITSupportModel[] newArray(int size) {
            return new CallITSupportModel[size];
        }
    };
}


