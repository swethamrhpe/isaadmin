package com.support.admin.instantsupport.Utility;

import android.util.Log;

/**-----------------------------------------------------------------------------
Class Name: AppLog
Created By: Suganya
Created Date: 06-1-2016
Modified By: 
Modified Date: 
Purpose: Application Log define here
-----------------------------------------------------------------------------*/
public class AppLog {

	private static boolean sFlagDebug= true;
	private static boolean sFlagInfo= true;
	private static boolean sFlagErr= true;
	
	
	/**-----------------------------------------------------------------------------
	Class Name: showLogD
	Created By: Suganya
	Created Date: 06-1-2016
	Modified By: 
	Modified Date: 
	Purpose: Method is Define for Debug scenario log
	-----------------------------------------------------------------------------*/
	public  static void showLogD(String tag, String message){
		if(sFlagDebug)
		{
			Log.d(tag,message);
		}
	}
	
	
	/**-----------------------------------------------------------------------------
	Class Name: showLogI
	Created By: Suganya
	Created Date: 06-1-2016
	Modified By: 
	Modified Date: 
	Purpose: Method is Define for any log
	-----------------------------------------------------------------------------*/
	public  static void showLogI(String tag, String message){
		if(sFlagInfo)
		{
			Log.i(tag,message);
		}
	}
	
	/**-----------------------------------------------------------------------------
	Class Name: showLogE
	Created By: Suganya
	Created Date: 06-1-2016
	Modified By: 
	Modified Date: 
	Purpose: Method is Define for Error log
	-----------------------------------------------------------------------------*/
	public  static void showLogE(String tag, String message){
		if(sFlagErr)
		{
			Log.e(tag,message);
		}
	}
}
