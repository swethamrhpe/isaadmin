package com.support.admin.instantsupport.MaintenanceMessage;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.support.admin.instantsupport.HomeScreen.MaintenanceMessageModel;
import com.support.admin.instantsupport.R;
import com.support.admin.instantsupport.Utility.ScreenNavigator;

import java.util.ArrayList;

/**
 * -----------------------------------------------------------------------------
 * Class Name: MaintenanceMessageAdapter
 * Created By: Suganya
 * Created Date: 31-08-2017
 * Modified By:
 * Modified Date:
 * Purpose: MaintenanceMessageAdapter class for list the queue data in a maintenance message queue CustomList
 * -----------------------------------------------------------------------------
 */

public class MaintenanceMessageAdapter extends RecyclerView.Adapter<MaintenanceMessageAdapter.MaintenanceMessageHolder> {
    private final String TAG = "MaintenanceMessageAdapter";
    private ArrayList<MaintenanceMessageModel> alMaintenanceMessageModel;
    private Context context;

    public MaintenanceMessageAdapter(Activity context,ArrayList<MaintenanceMessageModel> alMaintenanceMessageModel) {
        this.context = context;
        this.alMaintenanceMessageModel = alMaintenanceMessageModel;
    }


    public class MaintenanceMessageHolder extends RecyclerView.ViewHolder {
        public TextView tvMMTitle;
        public TextView tvMMLanguage,tvMMStatus,tvMMRegion,tvMMLocation;
        public LinearLayout llMMLayout;

        public MaintenanceMessageHolder(View v) {
            super(v);
            tvMMTitle = (TextView) v.findViewById(R.id.txtuName);
            tvMMLanguage = (TextView) v.findViewById(R.id.tvMMLanguage);
            tvMMStatus = (TextView) v.findViewById(R.id.tvMMStatus);
            tvMMRegion = (TextView) v.findViewById(R.id.tvMMRegion);
            tvMMLocation = (TextView) v.findViewById(R.id.tvMMLocation);
            llMMLayout = (LinearLayout) v.findViewById(R.id.llMMLayout);
        }
    }

    @Override
    public MaintenanceMessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.mm_list, parent, false);
        MaintenanceMessageHolder maintenanceMessageHolder = new MaintenanceMessageHolder(v);
        return maintenanceMessageHolder;
    }

    @Override
    public void onBindViewHolder(MaintenanceMessageHolder maintenanceMessageHolder, final int position) {
        maintenanceMessageHolder.tvMMTitle.setText(alMaintenanceMessageModel.get(position).getTitle());
        maintenanceMessageHolder.tvMMStatus.setText(alMaintenanceMessageModel.get(position).getStatus());
        maintenanceMessageHolder.tvMMLanguage.setText(alMaintenanceMessageModel.get(position).getLang());
        maintenanceMessageHolder.tvMMRegion.setText(alMaintenanceMessageModel.get(position).getRegion());
        maintenanceMessageHolder.tvMMLocation.setText(alMaintenanceMessageModel.get(position).getLocation());
        maintenanceMessageHolder.llMMLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ScreenNavigator.callMaintenanceMessageDetailsActivity((Activity) context, alMaintenanceMessageModel.get(position));
                ((Activity) context).finish();
            }
        });
        maintenanceMessageHolder.llMMLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                System.out.println("LongClick: ");
                return true;// returning true instead of false, works for me
            }
        });
    }
    @Override
    public int getItemCount() {
        return alMaintenanceMessageModel.size();
    }


}
