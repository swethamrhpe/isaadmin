package com.support.admin.instantsupport.HomeScreen;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.support.admin.instantsupport.R;

import java.util.ArrayList;
import java.util.List;

/**
 * -----------------------------------------------------------------------------
 * Class Name: SpinnerAdapter
 * Created By: Suganya
 * Created Date: 30-08-2017
 * Modified By:
 * Modified Date:
 * Purpose: Spinner custom view environments
 * -----------------------------------------------------------------------------
 */

public class SpinnerAdapter extends ArrayAdapter<String> {
    private Context ctx;
    private List<String> envTitleList;
    private ArrayList<Integer> envIconList;

    public SpinnerAdapter(Context context, int resource,  List<String> envTitleList,
                          ArrayList<Integer> envIconList) {
        super(context,  R.layout.env_spinner_text, R.id.spinnerEnvironment, envTitleList);
        this.ctx = context;
        this.envTitleList = envTitleList;
        this.envIconList = envIconList;
    }

    public SpinnerAdapter(Context context, int resource,  List<String> envTitleList
                         ) {
        super(context,  R.layout.env_spinner_text, R.id.spinnerEnvironment, envTitleList);
        this.ctx = context;
        this.envTitleList = envTitleList;
       // this.envIconList = envIconList;
    }
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.spinner_list, parent, false);

        TextView textView = (TextView) row.findViewById(R.id.spinnerTextView);
        textView.setText(envTitleList.get(position));

        ImageView imageView = (ImageView)row.findViewById(R.id.spinnerImages);
        if(envIconList !=null)
        imageView.setImageResource(envIconList.get(position));

        return row;
    }
}