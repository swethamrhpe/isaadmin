package com.support.admin.instantsupport.HomeScreen;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * -----------------------------------------------------------------------------
 * Class Name: ChatITSupportModel
 * Created By: Suganya
 * Created Date: 22-08-2017
 * Modified By:
 * Modified Date:
 * Purpose: ChatITSupportModel for Storing admin Chat IT Support details
 * -----------------------------------------------------------------------------
 */
public class ChatITSupportModel implements Parcelable {

    private String chatType;

    private String startTimeInUTC;

    private String endTimeInUTC;

    private String lang;

    private String uniqueId;

    private String serviceDaysPerWeek;

    private String chatConfig;

    private String companyId;

    private String chatBaseUrl;

    private String numberOfQueues;

    public ChatITSupportModel(){

    }


    private ArrayList<ChatITOptionModel> chatOptions;

    private String AIC;

    public String getChatType ()
    {
        return chatType;
    }

    public void setChatType (String chatType)
    {
        this.chatType = chatType;
    }

    public String getStartTimeInUTC ()
    {
        return startTimeInUTC;
    }

    public void setStartTimeInUTC (String startTimeInUTC)
    {
        this.startTimeInUTC = startTimeInUTC;
    }

    public String getEndTimeInUTC ()
    {
        return endTimeInUTC;
    }

    public void setEndTimeInUTC (String endTimeInUTC)
    {
        this.endTimeInUTC = endTimeInUTC;
    }

    public String getLang ()
    {
        return lang;
    }

    public void setLang (String lang)
    {
        this.lang = lang;
    }

    public String getUniqueId ()
    {
        return uniqueId;
    }

    public void setUniqueId (String uniqueId)
    {
        this.uniqueId = uniqueId;
    }

    public String getServiceDaysPerWeek ()
    {
        return serviceDaysPerWeek;
    }

    public void setServiceDaysPerWeek (String serviceDaysPerWeek)
    {
        this.serviceDaysPerWeek = serviceDaysPerWeek;
    }

    public String getChatConfig ()
    {
        return chatConfig;
    }

    public void setChatConfig (String chatConfig)
    {
        this.chatConfig = chatConfig;
    }

    public String getCompanyId ()
    {
        return companyId;
    }

    public void setCompanyId (String companyId)
    {
        this.companyId = companyId;
    }

    public String getChatBaseUrl ()
    {
        return chatBaseUrl;
    }

    public void setChatBaseUrl (String chatBaseUrl)
    {
        this.chatBaseUrl = chatBaseUrl;
    }

    public String getNumberOfQueues ()
    {
        return numberOfQueues;
    }

    public void setNumberOfQueues (String numberOfQueues)
    {
        this.numberOfQueues = numberOfQueues;
    }

    public ArrayList<ChatITOptionModel> getChatOptions ()
    {
        return chatOptions;
    }

    public void setChatOptions (ArrayList<ChatITOptionModel> chatOptions)
    {
        this.chatOptions = chatOptions;
    }

    public String getAIC ()
    {
        return AIC;
    }

    public void setAIC (String AIC)
    {
        this.AIC = AIC;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [chatType = "+chatType+", startTimeInUTC = "+startTimeInUTC+", endTimeInUTC = "+endTimeInUTC+", lang = "+lang+", uniqueId = "+uniqueId+", serviceDaysPerWeek = "+serviceDaysPerWeek+", chatConfig = "+chatConfig+", companyId = "+companyId+", chatBaseUrl = "+chatBaseUrl+", numberOfQueues = "+numberOfQueues+", chatOptions = "+chatOptions+", AIC = "+AIC+"]";
    }

    protected ChatITSupportModel(Parcel in) {
        chatType = in.readString();
        startTimeInUTC = in.readString();
        endTimeInUTC = in.readString();
        lang = in.readString();
        uniqueId = in.readString();
        serviceDaysPerWeek = in.readString();
        chatConfig = in.readString();
        companyId = in.readString();
        chatBaseUrl = in.readString();
        numberOfQueues = in.readString();
        AIC = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(chatType);
        dest.writeString(startTimeInUTC);
        dest.writeString(endTimeInUTC);
        dest.writeString(lang);
        dest.writeString(uniqueId);
        dest.writeString(serviceDaysPerWeek);
        dest.writeString(chatConfig);
        dest.writeString(companyId);
        dest.writeString(chatBaseUrl);
        dest.writeString(numberOfQueues);
        dest.writeString(AIC);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ChatITSupportModel> CREATOR = new Creator<ChatITSupportModel>() {
        @Override
        public ChatITSupportModel createFromParcel(Parcel in) {
            return new ChatITSupportModel(in);
        }

        @Override
        public ChatITSupportModel[] newArray(int size) {
            return new ChatITSupportModel[size];
        }
    };
}
