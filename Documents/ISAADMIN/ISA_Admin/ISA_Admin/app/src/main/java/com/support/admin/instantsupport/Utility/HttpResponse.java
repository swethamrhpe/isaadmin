package com.support.admin.instantsupport.Utility;

import com.google.gson.annotations.SerializedName;

/**
 * Created by mrswe on 12/19/2017.
 */

public class HttpResponse {

    @SerializedName("successCode")
    private String successcode;
    @SerializedName("successMessage")
    private String successmessage;

    public String getSuccesscode() {
        return successcode;
    }

    public void setSuccesscode(String successcode) {
        this.successcode = successcode;
    }

    public String getSuccessmessage() {
        return successmessage;
    }

    public void setSuccessmessage(String successmessage) {
        this.successmessage = successmessage;
    }
}

