package com.support.admin.instantsupport.Login;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.support.admin.instantsupport.Application.Constants;
import com.support.admin.instantsupport.Application.InstApplication;
import com.support.admin.instantsupport.IHttpConnectionInterface;
import com.support.admin.instantsupport.R;
import com.support.admin.instantsupport.Utility.AppLog;
import com.support.admin.instantsupport.Utility.GetHttpConnectionResponseTask;
import com.support.admin.instantsupport.Utility.JSONFormater;
import com.support.admin.instantsupport.Utility.ScreenNavigator;
import com.support.admin.instantsupport.Utility.Utils;

import org.json.fh.JSONObject;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * -----------------------------------------------------------------------------
 * Class Name: LoginActivity
 * Created By: Suganya
 * Created Date: 14-07-2017
 * Modified By:
 * Modified Date:
 * Purpose: This will provide login screen
 * -----------------------------------------------------------------------------
 */
public class LoginActivity extends AppCompatActivity implements ILoginInterface, IHttpConnectionInterface, Constants, View.OnClickListener {
    protected static final String TAG = "LoginActivity";
    private Button btnLoginSubmit;
    public Typeface fontArial;
    private TextView tvLoginSignin, tvLoginEmailAddress, tvLoginPassword, tvLoginForgotPassword;
    private EditText etLoginEmailAddress, etLoginPassword;
    private AlertDialog.Builder alertDialog;
    private InstApplication ApplicationObject;
    private String sEmailid, sPassword;
    private Locale locale;
    private ProgressDialog mProgressDialog;
    private boolean isEnablePasswrd = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        ApplicationObject = ((InstApplication) getApplicationContext());
        alertDialog = new AlertDialog.Builder(LoginActivity.this);
        fontArial = Typeface.createFromAsset(this.getAssets(), Constants.FONT_ARIAL);
        tvLoginSignin = (TextView) findViewById(R.id.tvLoginSignin);
        tvLoginEmailAddress = (TextView) findViewById(R.id.tvLoginEmailAddress);
        tvLoginPassword = (TextView) findViewById(R.id.tvLoginPassword);
        etLoginEmailAddress = (EditText) findViewById(R.id.etLoginEmailAddress);
        etLoginPassword = (EditText) findViewById(R.id.etLoginPassword);
        btnLoginSubmit = (Button) findViewById(R.id.btLoginSubmit);
        tvLoginForgotPassword = (TextView) findViewById(R.id.tvLoginForgotPassword);

        btnLoginSubmit.setTypeface(fontArial);
        btnLoginSubmit.setTypeface(Typeface.DEFAULT_BOLD);
        tvLoginSignin.setTypeface(fontArial);
        tvLoginSignin.setTypeface(Typeface.DEFAULT_BOLD);
        tvLoginEmailAddress.setTypeface(fontArial);
        tvLoginPassword.setTypeface(fontArial);
        etLoginEmailAddress.setTypeface(fontArial);
        etLoginPassword.setTypeface(fontArial);
        tvLoginForgotPassword.setTypeface(fontArial);
        btnLoginSubmit.setOnClickListener(LoginActivity.this);
        tvLoginForgotPassword.setOnClickListener(LoginActivity.this);
    }

    /**
     * -----------------------------------------------------------------------------
     * Method Name: isValidEmail
     * Created By: Suganya
     * Created Date: 14-1-2016
     * Modified By:
     * Modified Date:
     * Purpose: For validating Email id
     * -----------------------------------------------------------------------------
     */
    private boolean isValidEmail(String email) {
        Pattern pattern = Pattern.compile(Constants.EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /**
     * -----------------------------------------------------------------------------
     * Method Name: isValidPassword
     * Created By: Suganya
     * Created Date: 14-1-2016
     * Modified By:
     * Modified Date:
     * Purpose: For validating Password
     * -----------------------------------------------------------------------------
     */

    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() > 2) {
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btLoginSubmit:
                sEmailid = etLoginEmailAddress.getText().toString().trim().toLowerCase();
                if (!isValidEmail(sEmailid)) {
                    etLoginEmailAddress.setError(getString(R.string.ErrorEmailid));
                } else {
                    sPassword = etLoginPassword.getText().toString().trim();
                    if (Utils.isInternetOn(LoginActivity.this)) {
                        mProgressDialog = ProgressDialog.show(this, null, getResources().getString(R.string.pleasewait));
                        mProgressDialog.setContentView(R.layout.progressdialog_custom);
                        mProgressDialog.setCancelable(false);

                        if (!isValidPassword(sPassword)) {
                            if (mProgressDialog != null && mProgressDialog.isShowing())
                                mProgressDialog.dismiss();
                            etLoginPassword.setError(getString(R.string.ErrorPassword));
                        } else {
                            AppLog.showLogE(TAG, "Test1");
                            Utils.doAuthLogin(LoginActivity.this, sEmailid, sPassword);
                        }

                    } else {
                        Utils.ShowDalogOKCancel(LoginActivity.this, this.getString(R.string.app_name), getString(R.string.ErrorInternetConn));
                    }

                }
                break;

            case R.id.tvLoginForgotPassword:

                break;

        }
    }

    @Override
    public void processFinish(LoginScreenModel objLoginScreenModel) {
        AppLog.showLogE(TAG, "objLoginScreenModel>>" + objLoginScreenModel);
        if (objLoginScreenModel != null && !TextUtils.isEmpty(objLoginScreenModel.getStatus()) && objLoginScreenModel.getStatus().equalsIgnoreCase(getString(R.string.ok))) {
            if (Utils.isInternetOn(LoginActivity.this)) {
                try {
                    new GetHttpConnectionResponseTask(LoginActivity.this, ENVIRONMENT.DEV + CLOUD.REQ_METHOD_USER_DETAIL + "?emailId=" + objLoginScreenModel.getUserId(), null, Method.GET).execute();
                } catch (Exception e) {
                    AppLog.showLogE(TAG, "Exception>>>" + e);
                }
            } else {
                if (mProgressDialog != null && mProgressDialog.isShowing())
                    mProgressDialog.dismiss();
                Utils.ShowDialog(LoginActivity.this, this.getString(R.string.app_name), getString(R.string.ErrorInternetConn));
            }
        } else {
            if (mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismiss();
            if (objLoginScreenModel != null && !TextUtils.isEmpty(objLoginScreenModel.getMessage()) && objLoginScreenModel.getMessage().contains(Constants.LOGIN_FAILED))
                Utils.ShowDalogOKCancel(LoginActivity.this, getString(R.string.app_name), "\n " + getString(R.string.InvalidLoginCredential));
            else {
                Utils.ShowDalogOKCancel(LoginActivity.this, getString(R.string.app_name), "\n" + getString(R.string.ServerError));
            }
        }

    }

    @Override
    public void processUserDetailsFinish(UserDetailsModel objUserDetailsModel) {
        AppLog.showLogE(TAG, "objUserDetailsModel>>>>" + objUserDetailsModel);
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
        if (objUserDetailsModel != null && objUserDetailsModel.getMessage().contains(Constants.CONST_CLOUD_CALL.SUCCESS)) {
            ApplicationObject.setUserDetailsModel(objUserDetailsModel);
            if(objUserDetailsModel.getEnvironment().contains(DEV_ENV)){
                ApplicationObject.setBaseEnv(ENVIRONMENT.DEV);
            }else if(objUserDetailsModel.getEnvironment().contains(TEST_ENV)){
                ApplicationObject.setBaseEnv(ENVIRONMENT.TEST);
            }else{
                ApplicationObject.setBaseEnv(ENVIRONMENT.LIVE);
            }
//            ScreenNavigator.callSubAdminHomeActivity(LoginActivity.this, objUserDetailsModel);
//            finish();
            AppLog.showLogE(TAG,"gettype=" +objUserDetailsModel.getAdminType() + "userdetails=" +objUserDetailsModel.describeContents());
            if (objUserDetailsModel.getAdminType() == ADMIN_TYPE.SUB_ADMIN) {
                ScreenNavigator.callSubAdminHomeActivity(LoginActivity.this, objUserDetailsModel);
                finish();
            } else {
                ScreenNavigator.callMainAdminHomeActivity(LoginActivity.this, objUserDetailsModel);
                finish();
            }

        } else if (objUserDetailsModel != null && objUserDetailsModel.getMessage().contains(Constants.ERROR_CODE.NO_ADMIN_USER)) {
            Utils.ShowDialogWithDismiss(LoginActivity.this, getString(R.string.app_name), getString(R.string.Error_NoUer), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    etLoginEmailAddress.setText("");
                    etLoginPassword.setText("");
                }
            });
        } else {
            Utils.ShowDalogOKCancel(LoginActivity.this, getString(R.string.app_name), "\n" + getString(R.string.ServerError));
        }

    }

    @Override
    public void processConnectionFinish(String jsonObject) {
        try {
            if(jsonObject.startsWith("{")) {
                JSONObject jsonResponse = new JSONObject(jsonObject);
                JSONFormater.getJSONUserInfoClude(LoginActivity.this, jsonResponse);
            }else{
                AppLog.showLogE(TAG,"json error");
                if (mProgressDialog != null && mProgressDialog.isShowing())
                    mProgressDialog.dismiss();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
