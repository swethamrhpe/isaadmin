package com.support.admin.instantsupport.Login;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * -----------------------------------------------------------------------------
 * Class Name: UserDetailsModel
 * Created By: Suganya
 * Created Date: 07-08-2017
 * Modified By:
 * Modified Date:
 * Purpose: UserDetailsModel for Storing admin details
 * -----------------------------------------------------------------------------
 */

public class UserDetailsModel  implements Parcelable {

    private String emailId;

    private ArrayList<String> environment ;

    private String domain;

    private String adminType;

    private String message;

    private AdminRolesModel role;

    public UserDetailsModel() {

    }

    public String getEmailId ()
    {
        return emailId;
    }

    public void setEmailId (String emailId)
    {
        this.emailId = emailId;
    }

    public ArrayList<String> getEnvironment  ()
    {
        return environment ;
    }

    public void setEnvironment  (ArrayList<String> environment )
    {
        this.environment  = environment ;
    }

    public String getDomain ()
    {
        return domain;
    }

    public void setDomain (String domain)
    {
        this.domain = domain;
    }

    public String getAdminType ()
    {
        return adminType;
    }

    public void setAdminType (String adminType)
    {
        this.adminType = adminType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AdminRolesModel getRole ()
    {
        return role;
    }

    public void setRole (AdminRolesModel role)
    {
        this.role = role;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [emailId = "+emailId+", environment  = "+environment +", domain = "+domain+", adminType = "+adminType+", role = "+role+" message = "+message+"]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    protected UserDetailsModel(Parcel in) {

        emailId = in.readString();
        if (in.readByte() == 0x01) {
            environment = new ArrayList<String>();
            in.readList(environment, String.class.getClassLoader());
        } else {
            environment = null;
        }
        domain = in.readString();
        adminType = in.readString();
        message = in.readString();
        role = (AdminRolesModel) in.readValue(AdminRolesModel.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(emailId);
        if (environment == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(environment);
        }
        dest.writeString(domain);
        dest.writeString(adminType);
        dest.writeString(message);
        dest.writeValue(role);
    }

    public static final Creator<UserDetailsModel> CREATOR = new Creator<UserDetailsModel>() {
        @Override
        public UserDetailsModel createFromParcel(Parcel in) {
            return new UserDetailsModel(in);
        }

        @Override
        public UserDetailsModel[] newArray(int size) {
            return new UserDetailsModel[size];
        }
    };
}

