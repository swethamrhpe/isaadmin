package com.support.admin.instantsupport.SuportSpot;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.support.admin.instantsupport.Application.Constants;
import com.support.admin.instantsupport.Application.InstApplication;
import com.support.admin.instantsupport.HomeScreen.SpotDetailModel;
import com.support.admin.instantsupport.Login.UserDetailsModel;
import com.support.admin.instantsupport.R;
import com.support.admin.instantsupport.Utility.ScreenNavigator;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
/* -----------------------------------------------------------------------------
 * Class Name: SupportSpotActivity
 * Created By: Swetha MR
 * Created Date: 1-09-2018
 * Modified By:
 * Modified Date:
 * Purpose: SupportSpotActivity class for displaying walkin center complete details
 * -----------------------------------------------------------------------------
 */

public class SupportSpotActivity extends Activity {
    @BindView(R.id.ivLoginIcon)
    ImageView ivLoginIcon;
    @BindView(R.id.rvSpot)
    RecyclerView rvUserDetail;
    @BindView(R.id.fabSupportSPotAddNew)
    FloatingActionButton fabUserDetailAddNew;
    @BindView(R.id.spinnerSupportType)
    Spinner spinnerSupportType;
    private ArrayList<SpotDetailModel> alSupportSpotsList;
    private RecyclerView.Adapter mAdapter;
    private InstApplication ApplicationObject;
    private RecyclerView.LayoutManager mLayoutManager;
    private UserDetailsModel objUserDetailsModel;
    private ArrayList<String> alSupportType;
    private ArrayList<SpotDetailModel> filteredSpotList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_supportsport);
        ButterKnife.bind(this);
        ApplicationObject = ((InstApplication) getApplicationContext());
        objUserDetailsModel = ApplicationObject.getUserDetailsModel();
        Bundle bunlde = getIntent().getExtras();
        if(bunlde!=null){
            alSupportSpotsList = bunlde.getParcelableArrayList(Constants.BundleKeyConstants.SUPPORT_SPOT);
        }
        Resources res = getResources();
        String[] mySpinnerItem = res.getStringArray(R.array.arrSupportType);
        alSupportType = new ArrayList<>();
        filteredSpotList = new ArrayList<>();
        for(String s:mySpinnerItem){
            alSupportType.add(s);
        }
        ArrayAdapter<String> spinSupportTypeAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item, alSupportType);
        spinnerSupportType.setAdapter(spinSupportTypeAdapter);
        spinnerSupportType.setSelection(0);
        spinnerSupportType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(alSupportSpotsList!=null && alSupportSpotsList.size()>0) {
                    filter(alSupportType.get(position), alSupportSpotsList);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        rvUserDetail.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        rvUserDetail.setLayoutManager(mLayoutManager);
    }

    @OnClick(R.id.fabSupportSPotAddNew)
    public void onViewClicked() {
        ScreenNavigator.callSupportDetailActivity(SupportSpotActivity.this,null);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ScreenNavigator.callSubAdminHomeActivity(SupportSpotActivity.this,objUserDetailsModel);
        finish();
    }
    /*Filter method used to filter based on supportspottype*/
    private void filter(String selectedItem,ArrayList<SpotDetailModel> arrSupportList){
        if(filteredSpotList.size()>0){
            filteredSpotList.clear();
        }
        for(int i=0;i<arrSupportList.size();i++){
            if(arrSupportList.get(i).getSupportSpotType().equals(selectedItem)){
                filteredSpotList.add(arrSupportList.get(i));
            }
        }
        mAdapter = new SupportSpotAdapter(SupportSpotActivity.this, filteredSpotList);
        rvUserDetail.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }
}
