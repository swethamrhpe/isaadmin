package com.support.admin.instantsupport.HomeScreen;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * -----------------------------------------------------------------------------
 * Class Name: SelfSupportModel
 * Created By: Suganya
 * Created Date: 14-09-2017
 * Modified By:
 * Modified Date:
 * Purpose: SelfSupportModel for Storing admin self Support queue details
 * -----------------------------------------------------------------------------
 */

public class SelfSupportModel implements Parcelable {

    private String companyDescription;

    private String companyId;

    private ArrayList<SelfSupportOptionModel> selfSupportOptions;

    private String numberOfQueues;

    private String lang;

    private String uniqueId;

    public SelfSupportModel(){

    }

    public String getCompanyDescription() {
        return companyDescription;
    }

    public void setCompanyDescription(String companyDescription) {
        this.companyDescription = companyDescription;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public ArrayList<SelfSupportOptionModel> getSelfSupportOptions() {
        return selfSupportOptions;
    }

    public void setSelfSupportOptions(ArrayList<SelfSupportOptionModel> selfSupportOptions) {
        this.selfSupportOptions = selfSupportOptions;
    }

    public String getNumberOfQueues() {
        return numberOfQueues;
    }

    public void setNumberOfQueues(String numberOfQueues) {
        this.numberOfQueues = numberOfQueues;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    protected SelfSupportModel(Parcel in) {
        companyDescription = in.readString();
        companyId = in.readString();
        selfSupportOptions = in.createTypedArrayList(SelfSupportOptionModel.CREATOR);
        numberOfQueues = in.readString();
        lang = in.readString();
        uniqueId = in.readString();
    }

    @Override
    public String toString() {
        return "ClassPojo [companyDescription = " + companyDescription + ", companyId = " + companyId + ", selfSupportOptions = " + selfSupportOptions + ", numberOfQueues = " + numberOfQueues + ", lang = " + lang + ", uniqueId = " + uniqueId + "]";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(companyDescription);
        parcel.writeString(companyId);
        parcel.writeTypedList(selfSupportOptions);
        parcel.writeString(numberOfQueues);
        parcel.writeString(lang);
        parcel.writeString(uniqueId);
    }

    public static final Creator<SelfSupportModel> CREATOR = new Creator<SelfSupportModel>() {
        @Override
        public SelfSupportModel createFromParcel(Parcel in) {
            return new SelfSupportModel(in);
        }

        @Override
        public SelfSupportModel[] newArray(int size) {
            return new SelfSupportModel[size];
        }
    };
}
