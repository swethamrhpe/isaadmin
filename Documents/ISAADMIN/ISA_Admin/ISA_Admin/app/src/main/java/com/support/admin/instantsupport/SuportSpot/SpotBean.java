package com.support.admin.instantsupport.SuportSpot;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mrswe on 1/5/2018.
 */

public class SpotBean implements Parcelable {

    @SerializedName("spotDetails")
    private List<Spotdetails> spotdetails;

    public List<Spotdetails> getSpotdetails() {
        return spotdetails;
    }

    public void setSpotdetails(List<Spotdetails> spotdetails) {
        this.spotdetails = spotdetails;
    }

    public static class Spotdetails implements Parcelable {

        @SerializedName("companyId")
        private int companyid;
        @SerializedName("supportSpotType")
        private String supportspottype;
        @SerializedName("latitude")
        private double latitude;
        @SerializedName("longitude")
        private double longitude;
        @SerializedName("addressField1")
        private String addressfield1;
        @SerializedName("addressField2")
        private String addressfield2;
        @SerializedName("state")
        private String state;
        @SerializedName("zipCode")
        private int zipcode;
        @SerializedName("countryCode")
        private String countrycode;
        @SerializedName("isMeridianExist")
        private boolean ismeridianexist;
        @SerializedName("serviceHour")
        private String servicehour;
        @SerializedName("serviceDetails")
        private String servicedetails;
        @SerializedName("centerTitle")
        private String centertitle;
        @SerializedName("uniqueId")
        private String uniqueid;

        public int getCompanyid() {
            return companyid;
        }

        public void setCompanyid(int companyid) {
            this.companyid = companyid;
        }

        public String getSupportspottype() {
            return supportspottype;
        }

        public void setSupportspottype(String supportspottype) {
            this.supportspottype = supportspottype;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public String getAddressfield1() {
            return addressfield1;
        }

        public void setAddressfield1(String addressfield1) {
            this.addressfield1 = addressfield1;
        }

        public String getAddressfield2() {
            return addressfield2;
        }

        public void setAddressfield2(String addressfield2) {
            this.addressfield2 = addressfield2;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public int getZipcode() {
            return zipcode;
        }

        public void setZipcode(int zipcode) {
            this.zipcode = zipcode;
        }

        public String getCountrycode() {
            return countrycode;
        }

        public void setCountrycode(String countrycode) {
            this.countrycode = countrycode;
        }

        public boolean getIsmeridianexist() {
            return ismeridianexist;
        }

        public void setIsmeridianexist(boolean ismeridianexist) {
            this.ismeridianexist = ismeridianexist;
        }

        public String getServicehour() {
            return servicehour;
        }

        public void setServicehour(String servicehour) {
            this.servicehour = servicehour;
        }

        public String getServicedetails() {
            return servicedetails;
        }

        public void setServicedetails(String servicedetails) {
            this.servicedetails = servicedetails;
        }

        public String getCentertitle() {
            return centertitle;
        }

        public void setCentertitle(String centertitle) {
            this.centertitle = centertitle;
        }

        public String getUniqueid() {
            return uniqueid;
        }

        public void setUniqueid(String uniqueid) {
            this.uniqueid = uniqueid;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.companyid);
            dest.writeString(this.supportspottype);
            dest.writeDouble(this.latitude);
            dest.writeDouble(this.longitude);
            dest.writeString(this.addressfield1);
            dest.writeString(this.addressfield2);
            dest.writeString(this.state);
            dest.writeInt(this.zipcode);
            dest.writeString(this.countrycode);
            dest.writeByte(this.ismeridianexist ? (byte) 1 : (byte) 0);
            dest.writeString(this.servicehour);
            dest.writeString(this.servicedetails);
            dest.writeString(this.centertitle);
            dest.writeString(this.uniqueid);
        }

        public Spotdetails() {
        }

        protected Spotdetails(Parcel in) {
            this.companyid = in.readInt();
            this.supportspottype = in.readString();
            this.latitude = in.readDouble();
            this.longitude = in.readDouble();
            this.addressfield1 = in.readString();
            this.addressfield2 = in.readString();
            this.state = in.readString();
            this.zipcode = in.readInt();
            this.countrycode = in.readString();
            this.ismeridianexist = in.readByte() != 0;
            this.servicehour = in.readString();
            this.servicedetails = in.readString();
            this.centertitle = in.readString();
            this.uniqueid = in.readString();
        }

        public static final Creator<Spotdetails> CREATOR = new Creator<Spotdetails>() {
            @Override
            public Spotdetails createFromParcel(Parcel source) {
                return new Spotdetails(source);
            }

            @Override
            public Spotdetails[] newArray(int size) {
                return new Spotdetails[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.spotdetails);
    }

    public SpotBean() {
    }

    protected SpotBean(Parcel in) {
        this.spotdetails = new ArrayList<Spotdetails>();
        in.readList(this.spotdetails, Spotdetails.class.getClassLoader());
    }

    public static final Parcelable.Creator<SpotBean> CREATOR = new Parcelable.Creator<SpotBean>() {
        @Override
        public SpotBean createFromParcel(Parcel source) {
            return new SpotBean(source);
        }

        @Override
        public SpotBean[] newArray(int size) {
            return new SpotBean[size];
        }
    };
}
