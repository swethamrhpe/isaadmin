package com.support.admin.instantsupport.HomeScreen;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * -----------------------------------------------------------------------------
 * Class Name: CallITPhoneNumberModel
 * Created By: Suganya
 * Created Date: 21-08-2017
 * Modified By:
 * Modified Date:
 * Purpose: CallITPhoneNumberModel for Storing admin Call IT Support queue details
 * -----------------------------------------------------------------------------
 */

public class CallITPhoneNumberModel implements Parcelable {

    private String questionId;

    private String requestId;

    private String phoneType;

    private String secondaryAddress;

    private String queueId;

    private String vdnNum;

    private String queueName;

    private ArrayList<String> number;

    private String prompt;

    private String primaryAddress;

    public CallITPhoneNumberModel() {

    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getPhoneType() {
        return phoneType;
    }

    public void setPhoneType(String phoneType) {
        this.phoneType = phoneType;
    }

    public String getSecondaryAddress() {
        return secondaryAddress;
    }

    public void setSecondaryAddress(String secondaryAddress) {
        this.secondaryAddress = secondaryAddress;
    }

    public String getQueueId() {
        return queueId;
    }

    public void setQueueId(String queueId) {
        this.queueId = queueId;
    }

    public String getVdnNum() {
        return vdnNum;
    }

    public void setVdnNum(String vdnNum) {
        this.vdnNum = vdnNum;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public ArrayList<String> getNumber() {
        return number;
    }

    public void setNumber(ArrayList<String> number) {
        this.number = number;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    public String getPrimaryAddress() {
        return primaryAddress;
    }

    public void setPrimaryAddress(String primaryAddress) {
        this.primaryAddress = primaryAddress;
    }

    @Override
    public String toString() {
        return "ClassPojo [questionId = " + questionId + ", requestId = " + requestId + ", phoneType = " + phoneType + ", secondaryAddress = " + secondaryAddress + ", queueId = " + queueId + ", vdnNum = " + vdnNum + ", queueName = " + queueName + ", number = " + number + ", prompt = " + prompt + ", primaryAddress = " + primaryAddress + "]";
    }

    protected CallITPhoneNumberModel(Parcel in) {
        questionId = in.readString();
        requestId = in.readString();
        phoneType = in.readString();
        secondaryAddress = in.readString();
        queueId = in.readString();
        vdnNum = in.readString();
        queueName = in.readString();
        number = in.createStringArrayList();
        prompt = in.readString();
        primaryAddress = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(questionId);
        dest.writeString(requestId);
        dest.writeString(phoneType);
        dest.writeString(secondaryAddress);
        dest.writeString(queueId);
        dest.writeString(vdnNum);
        dest.writeString(queueName);
        dest.writeStringList(number);
        dest.writeString(prompt);
        dest.writeString(primaryAddress);
    }

    public static final Creator<CallITPhoneNumberModel> CREATOR = new Creator<CallITPhoneNumberModel>() {
        @Override
        public CallITPhoneNumberModel createFromParcel(Parcel in) {
            return new CallITPhoneNumberModel(in);
        }

        @Override
        public CallITPhoneNumberModel[] newArray(int size) {
            return new CallITPhoneNumberModel[size];
        }
    };

}


