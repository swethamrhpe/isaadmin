package com.support.admin.instantsupport.HomeScreen;

/**
 * -----------------------------------------------------------------------------
 * Class Name: ChatITOptionModel
 * Created By: Suganya
 * Created Date: 22-08-2017
 * Modified By:
 * Modified Date:
 * Purpose: ChatITOptionModel for Storing admin Chat IT Support queue details
 * -----------------------------------------------------------------------------
 */

public class ChatITOptionModel {

    private String escQuestion;

    private String requestId;

    private String chatCategory;

    public String getEscQuestion ()
    {
        return escQuestion;
    }

    public void setEscQuestion (String escQuestion)
    {
        this.escQuestion = escQuestion;
    }

    public String getRequestId ()
    {
        return requestId;
    }

    public void setRequestId (String requestId)
    {
        this.requestId = requestId;
    }

    public String getChatCategory ()
    {
        return chatCategory;
    }

    public void setChatCategory (String chatCategory)
    {
        this.chatCategory = chatCategory;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [escQuestion = "+escQuestion+", requestId = "+requestId+", chatCategory = "+chatCategory+"]";
    }
}


