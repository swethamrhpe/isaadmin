package com.hp.qrcode.myapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Created by mrswe on 1/11/2018.
 */


public class MainActivity extends Activity {
    private static final String TAG = "MainActivity";
    private TextView etPhone;
    private ListAdapter lAdapter;
    // private Map<Integer, Map<String, String>> arrayEdit = new HashMap<>();
    private ArrayList<Map<String,String>> arrayEdit = new ArrayList<>();
    Map<String, String> newMap ;
    private Window dialogWindow;
    RecyclerView rvList;
    Map<Integer,Map<String,String>> arrOfStr;
    int mainPosition =0;
    boolean isDel= false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout);
        etPhone = (TextView) findViewById(R.id.etPhone);
        // String[] aa = {"[ss-df]","[dd-23]","[ss-3e3]"};
        String ss  = "[aa - 232]";
        JSONArray json = new JSONArray();
        String[] arrayValues = ss.trim().split("-");

        String etGetItem = arrayValues[0].replace("[", "");
        String spinGetItem = arrayValues[1].replace("]", "");
        Log.e(TAG,"sep =" +etGetItem +"s,"+spinGetItem);
        newMap = new HashMap<>();

/*
for(String s: aa) {
    json.put(s);
}*/

        json.put(ss.replace("[","").replace("]","").replace("\"",""));
        Log.e(TAG,"json= "+json.toString().replace("\\\\","").replace("\"",""));
        etPhone.setText("[XX - 1234567565]");
        etPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (mainPosition > 0 && arrayEdit.entrySet().size() > 0) {
                    mainPosition = arrayEdit.entrySet().size();
                    // arrOfStr = etPhone.getText().toString().split(",");
                }*/
                buttonClicked(etPhone.getText().toString());
            }
        });
    }

    public void buttonClicked(final String editFields) {
        LayoutInflater inflater = getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.lisview, null);
        rvList = alertLayout.findViewById(R.id.rvList);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvList.setLayoutManager(mLayoutManager);
        rvList.setHasFixedSize(true);
        lAdapter = new ListAdapter(MainActivity.this,arrayEdit);
        //MainActivity.this, arrayEdit.size());
        rvList.setAdapter(lAdapter);
        final Button btAdd = alertLayout.findViewById(R.id.btAdd);
        if(!TextUtils.isEmpty(editFields)){

            if(!editFields.contains(",")) {
                String[] arrayValues = editFields.trim().split("-");
                String etGetItem = arrayValues[0].replace("[", "");
                String spinGetItem = arrayValues[1].replace("]", "");
                newMap.put(etGetItem, spinGetItem);
                arrayEdit.add(newMap);
                //Add(0);
            }
        }
        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mainPosition++;
                newMap = new HashMap<>();
                newMap.put("", "");
                arrayEdit.add(newMap);
                //Add(mainPosition);
                lAdapter.notifyItemInserted(mainPosition);
                lAdapter.notifyItemRangeChanged(mainPosition, arrayEdit.size());
                lAdapter = new ListAdapter(MainActivity.this, arrayEdit);
                rvList.setAdapter(lAdapter);
            }
        });
        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
        alert.setTitle("Phone Number");
        alert.setView(alertLayout);
        alert.setCancelable(false);
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getBaseContext(), "Cancel clicked", Toast.LENGTH_SHORT).show();
            }
        });
        alert.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                StringBuilder builder = new StringBuilder();
                String prefix = "";
                int size = arrayEdit.size();
                for(int i=0;i<size;i++) {
                    Set set = arrayEdit.get(i).entrySet();
                    Iterator iterator = set.iterator();
                    while(iterator.hasNext()) {
                        Map.Entry mentry = (Map.Entry)iterator.next();
                        builder.append(prefix);
                        prefix = ",";
                        builder.append(mentry.getKey() + " - " + mentry.getValue());
                    }
                }
                /*
                int set = arrayEdit.size();
                for(int i=0;i<set;i++){
                    builder.append(prefix);
                    prefix = ",";
                    builder.append(arrayEdit.get(i).toString().replace("=", "-"));
                }*/
                String str = builder.toString().replace("{","").replace("}","");
                etPhone.setText(Arrays.asList(str).toString());
               /* String str = builder.toString();
                etPhone.setText(str);*/
            }
        });
        AlertDialog dialog = alert.create();
        dialogWindow = dialog.getWindow();
        dialog.show();
    }

    public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ListHolder> {
        private final String TAG = "ListAdapter";
        private Context context;
        ArrayList<Map<String,String>> editFields;
        public ListAdapter(Context context, ArrayList<Map<String,String>> editFields) {
            this.context = context;
            this.editFields = editFields;
        }
        public class ListHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            final EditText etPhoneNumber;
            final Spinner spinCountyCode;
            final ImageView imgEdit, imgCheck;
            final ImageView imgDelete;
            final LinearLayout llLayout;
            String etItem,spItem;
            public ListHolder(View v) {
                super(v);
                etPhoneNumber = v.findViewById(R.id.etPhoneNumber);
                spinCountyCode = v.findViewById(R.id.spCountyCode);
                imgEdit = v.findViewById(R.id.imgEdit);
                imgCheck = v.findViewById(R.id.imgCheck);
                imgDelete = v.findViewById(R.id.imgDelete);
                llLayout = v.findViewById(R.id.llAllData);
                imgDelete.setOnClickListener(this);
                imgCheck.setOnClickListener(this);

            }
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.imgCheck:

                         etItem = etPhoneNumber.getText().toString();
                         spItem = spinCountyCode.getSelectedItem().toString();
                        newMap.put(etItem, spItem);
                        if(etItem.equalsIgnoreCase("") || etItem.length()<12){
                            etPhoneNumber.setError("Please enter valid phonenumber");
                        }
                        if(arrayEdit.size() == getAdapterPosition()) {
                            arrayEdit.add(newMap);
                        }
                        //Add(getAdapterPosition()-1);
                        imgEdit.setVisibility(View.VISIBLE);
                        imgCheck.setVisibility(View.GONE);
                        etPhoneNumber.setEnabled(false);
                        spinCountyCode.setEnabled(false);
                        break;
                    case R.id.imgDelete:
                        delete(getAdapterPosition());
                        break;
                }
            }
        }
        @Override
        public ListAdapter.ListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(
                    parent.getContext());
            View v = inflater.inflate(R.layout.call_it_support_add_phonenumber_row, parent, false);
            ListAdapter.ListHolder listHolder = new ListAdapter.ListHolder(v);
            return listHolder;
        }

        @Override
        public void onBindViewHolder(final ListAdapter.ListHolder listHolder, final int position) {
            final EditText etPhoneNumber = listHolder.etPhoneNumber;
            final ImageView imageEdit = listHolder.imgEdit;
            final ImageView imageCheck = listHolder.imgCheck;
            final Spinner spinCC = listHolder.spinCountyCode;

            //newMap = new HashMap<>();
            Resources res = context.getResources();
            String[] mySpinnerItem = res.getStringArray(R.array.arrCutntrycodes);
            final ArrayAdapter<String> spinArray = new ArrayAdapter<String>(context,
                    android.R.layout.simple_spinner_dropdown_item, mySpinnerItem);
            spinCC.setAdapter(spinArray);

            String[] arrayValues;
            String etGetItem;
            String spinGetItem;

            if (editFields.size() ==1) {
                Map<String, String> values = arrayEdit.get(position);
                arrayValues = values.toString().split("=");
                etGetItem = arrayValues[1].replace("}", "");
                spinGetItem = arrayValues[0].replace("{", "");
                etPhoneNumber.setText(etGetItem);
                for (int i = 0; i < spinCC.getCount(); i++) {
                    if (spinCC.getItemAtPosition(i).equals(spinGetItem)) {
                        spinCC.setSelection(i);
                    }
                }
            }
            if (editFields.size() >1) {
                Map<String, String> values = arrayEdit.get(position);
                arrayValues = values.toString().split("=");
                etGetItem = arrayValues[0].replace("{", "");
                spinGetItem = arrayValues[1].replace("}", "");
                etPhoneNumber.setText(etGetItem);
                for (int i = 0; i < spinCC.getCount(); i++) {
                    if (spinCC.getItemAtPosition(i).equals(spinGetItem)) {
                        spinCC.setSelection(i);
                    }
                }
            }
            etPhoneNumber.setEnabled(false);
            etPhoneNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogWindow.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
                    dialogWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

                }
            });
            spinCC.setEnabled(false);
            imageEdit.setVisibility(View.VISIBLE);
            imageEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    etPhoneNumber.setEnabled(true);
                    spinCC.setEnabled(true);
                    imageCheck.setVisibility(View.VISIBLE);
                    imageEdit.setVisibility(View.GONE);
                }
            });
        }

        @Override
        public int getItemCount() {
            Log.d("getItemCount()","getItemCount() "+arrayEdit.size());
            return (arrayEdit.size() > 1 ? arrayEdit.size() : 1);
        }
    }

    /*public void Add(int position){
        arrayEdit.add(position,newMap);
    }*/
    public void delete(int position) {
        arrayEdit.remove(position);
        lAdapter.notifyItemRemoved(position);
        lAdapter.notifyItemRangeChanged(position, arrayEdit.size());
    }
}
